#include "control.h"

SCM g_make_vector(int len, SCM *sp)
{
  SCM vect = scm_make_vector (scm_from_uint (len), SCM_BOOL_F);
  int i,j;
  SCM *pt  = SCM_I_VECTOR_WELTS(vect);
  for(i=0,j=len-1;i<len;i++,j--)
    {
      pt[i]=sp[j];
    }
  return vect;
}

void g_wrong_argument(long rax)
{
  scm_misc_error("vm-native-error","wrong number of arguments! got ~a",scm_list_1(SCM_PACK(rax/2+2)));
}

void g_car_error()
{
  scm_misc_error("vm-native-error","taking car of non pair!",SCM_EOL);
}

void g_cdr_error()
{
  scm_misc_error("vm-native-error","taking cdr of non pair!",SCM_EOL);
}

void g_setcar_error()
{
  scm_misc_error("vm-native-error","taking set-car! of non pair!",SCM_EOL);
}

void g_setcdr_error()
{
  scm_misc_error("vm-native-error","taking set-cdr! of non pair!",SCM_EOL);
}

void g_not_a_var(SCM scm)
{
  scm_misc_error("vm-native-error","(variable-ref) ~a is not a variable",scm_list_1(scm));
}

void g_not_a_var_b(SCM scm)
{
  scm_misc_error("vm-native-error","(variable-bound?) ~a is not a variable",scm_list_1(scm));
}

void g_not_a_var_s(SCM scm)
{
  scm_misc_error("vm-native-error","(variable-set!) ~a is not a variable",scm_list_1(scm));
}

void g_var_not_bound(SCM scm)
{
  scm_misc_error("vm-native-error","variable ~a is not bounded",scm_list_1(scm));
}

//Copied from vm-i-system.c
#define VARIABLE_BOUNDP(v)      (!scm_is_eq (VARIABLE_REF (v), SCM_UNDEFINED))
#define VARIABLE_REF(v)		SCM_VARIABLE_REF (v)

//Copied from vm.c
static SCM
resolve_variable (SCM what, SCM program_module)
{
  if (SCM_LIKELY (scm_is_symbol (what)))
    {
      if (SCM_LIKELY (scm_module_system_booted_p
                      && scm_is_true (program_module)))
        /* might longjmp */
        return scm_module_lookup (program_module, what);
      else
        {
          SCM v = scm_sym2var (what, SCM_BOOL_F, SCM_BOOL_F);
          if (scm_is_false (v))
            scm_misc_error (NULL, "unbound variable: ~S", scm_list_1 (what));
          else
            return v;
        }
    }
  else
    {
      SCM mod;
      /* compilation of @ or @@
         `what' is a three-element list: (MODNAME SYM INTERFACE?)
         INTERFACE? is #t if we compiled @ or #f if we compiled @@
      */
      mod = scm_resolve_module (SCM_CAR (what));
      if (scm_is_true (SCM_CADDR (what)))
        mod = scm_module_public_interface (mod);
      if (scm_is_false (mod))
        scm_misc_error (NULL, "no such module: ~S",
                        scm_list_1 (SCM_CAR (what)));
      /* might longjmp */
      return scm_module_lookup (mod, SCM_CADR (what));
    }
}


SCM g_toplevel_lookup(SCM what, SCM program, int i)
{
  SCM resolved;

  resolved = resolve_variable (what, scm_program_module (program));
  if (!VARIABLE_BOUNDP (resolved))
    {
      scm_misc_error("vm-native-error","(toplevel-ref) toplevel refed variable ~a is not bounded",scm_list_1(what));     
    }

  SCM o = SCM_PROGRAM_OBJTABLE(program);
  SCM_I_VECTOR_WELTS(o)[i] = resolved;
  return resolved;
}


SCM g_prompt(SCM k, scm_t_uint8 escape_only_p)
{
  SCM prompt;

  /* Push the prompt onto the dynamic stack. */
  prompt = scm_c_make_prompt (k, 
                              (SCM*)0, 
                              (SCM*)0, 
                              (scm_t_uint8 *) 0, 
                              escape_only_p, 
                              SCM_BOOL_F,
                              scm_i_dynwinds ());
  
  scm_i_set_dynwinds (scm_cons (prompt, SCM_PROMPT_DYNWINDS (prompt)));

  return prompt;
}


SCM g_get_program(SCM program)
{
   if (SCM_STRUCTP (program) && SCM_STRUCT_APPLICABLE_P (program))
     {
       return SCM_STRUCT_PROCEDURE (program);
     }
   else if (SCM_NIMP (program) && SCM_TYP7 (program) == scm_tc7_smob
            && SCM_SMOB_APPLICABLE_P (program))
     {
       scm_misc_error("vm-native-error","~a smob applicable is not natively supported",scm_list_1(program));
     
       /*
         PUSH (program);
         prepare_smob_call (sp, ++nargs, program);
       */
     }
   
   scm_misc_error("vm-native-error","~a is not a program", scm_list_1(program));
   
   return SCM_BOOL_F;
}

void g_no_jitted_code(SCM p)
{
  scm_misc_error("vm-native-error","~a is a non native program excuted from native VM", scm_list_1(p));
}

void g_truncate_err()
{
  scm_misc_error("vm-native-error","wrong number of values to truncate errors", SCM_EOL);
}

SCM scm_call_back_to_vm(SCM *program_p, SCM *sp)
{
  int n       = program_p - sp;
  SCM args    = SCM_EOL;
  SCM program = *program_p;

  if(!SCM_PROGRAM_P(program))
    program = g_get_program(program);

  while(sp != program_p)
    {
      args = scm_cons(*sp,args);
      sp++;
    }
  
  return scm_apply_0(program,args);
}

SCM scm_mvcall_back_to_vm(SCM *program_p, SCM *sp, SCM wrapper)
{
  int n       = program_p - sp;
  SCM args    = SCM_EOL;
  SCM program = *program_p;

  if(!SCM_PROGRAM_P(program))
    program = g_get_program(program);

  while(sp != program_p)
    {
      args = scm_cons(*sp,args);
      sp++;
    }

  return scm_apply_1(wrapper,program,args);
}

void g_zero_in_values()
{
  scm_misc_error("vm-native-error","return/values return zero values", SCM_EOL);
}
