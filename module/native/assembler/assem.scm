(define-module (native assembler assem)
  #:use-module  (native assembler disassem)
  #:use-module  (native assembler sbcl)
  #:use-module  (native assembler sset)
  #:use-module  (native assembler early-assem)
  #:use-module  (native assembler params)
  #:use-module  (rnrs records syntactic)
  #:use-module  (rnrs bytevectors)
  #:use-module  (ice-9 pretty-print)
  #:use-module  (srfi srfi-11)
  #:use-module  (srfi srfi-1)
  #:use-module  (system syntax)

  #:export (define-bitfield-emitter define-instruction make-segment inst
	     finalize-segment
             assemble make-segment
	     segment-buffer
             emit-back-patch %%current-segment%%
             label label? label-position gen-label
	     *assem-scheduler?* *threading*
             emit-byte emit-back-patch emit-chooser
             emit-alignment emit-label))
;;;; scheduling assembler

;;;; This software is part of the SBCL system. See the README file for
;;;; more information.
;;;;
;;;; This software is derived from the CMU CL system, which was
;;;; written at Carnegie Mellon University and released into the
;;;; public domain. The software is in the public domain and is
;;;; provided with absolutely no warranty. See the COPYING and CREDITS
;;;; files for more information.

;;;; assembly control parameters
(define *threading*           #f)
(define *assem-scheduler?*    #f)
(define *assem-instructions*  (make-hash-table))
(define *assem-max-locations* 0)
(define *dyncount*           #f)
;;;; the SEGMENT structure

;;; This structure holds the state of the assembler.
(define-record-type segment
  (protocol
   (mk-cl-protocol
    (type             #:regular)
    (buffer           (make-bytevector 1))
    (run-scheduler    #f)
    (inst-hook        #f)
    (current-posn     0)
    (%current-index   0)
    (annotations     '())
    (last-annotation '())
    (alignment        max-alignment)
    (sync-posn        0)
    (final-posn       0)
    (final-index      0)
    (postits         '())
    (inst-number      0)
    (readers          (make-vector *assem-max-locations* #f))
    (writers          (make-vector *assem-max-locations* #f))
    (branch-countdown #f)
    (emittable-insts-sset (make-sset))
    (queued-branches '())
    (delayed         '())
    (emittable-insts-queue '())
    (collect-dynamic-statistics #f)))

  (fields
   ;; the type of this segment (for debugging output and stuff)
   (mutable type)

   ;; Ordinarily this is a vector where instructions are written. If
   ;; the segment is made invalid (e.g. by APPEND-SEGMENT) then the
   ;; vector can be replaced by NIL. This used to be an adjustable
   ;; array, but we now do the array size management manually for
   ;; performance reasons (as of 2006-05-13 hairy array operations
   ;; are rather slow compared to simple ones).
   (mutable buffer)
   
   ;; whether or not to run the scheduler. Note: if the instruction
   ;; definitions were not compiled with the scheduler turned on, this
   ;; has no effect.
   (mutable run-scheduler)

   ;; If a function, then this is funcalled for each inst emitted with
   ;; the segment, the VOP, the name of the inst (as a string), and the
   ;; inst arguments.
   (mutable inst-hook)

   ;; what position does this correspond to? Initially, positions and
   ;; indexes are the same, but after we start collapsing choosers,
   ;; positions can change while indexes stay the same.
   (mutable current-posn)
   (mutable %current-index)

   ;; a list of all the annotations that have been output to this segment
   (mutable annotations )

   ;; a pointer to the last cons cell in the annotations list. This is
   ;; so we can quickly add things to the end of the annotations list.
   (mutable last-annotation)
   ;; the number of bits of alignment at the last time we synchronized

   (mutable alignment)

   ;; the position the last time we synchronized
   (mutable sync-posn)

  ;; The posn and index everything ends at. This is not maintained
  ;; while the data is being generated, but is filled in after.
   ;; Basically, we copy CURRENT-POSN and CURRENT-INDEX so that we can
   ;; trash them while processing choosers and back-patches.
   (mutable final-posn )
   (mutable final-index)

   ;; *** State used by the scheduler during instruction queueing.
   ;;
   ;; a list of postits. These are accumulated between instructions.
   (mutable postits)

   ;; ``Number'' for last instruction queued. Used only to supply insts
   ;; with unique sset-element-number's.
   (mutable inst-number)

   ;; SIMPLE-VECTORs mapping locations to the instruction that reads them and
   ;; instructions that write them
   (mutable readers)
   (mutable writers )
   
   ;; The number of additional cycles before the next control transfer,
   ;; or NIL if a control transfer hasn't been queued. When a delayed
   ;; branch is queued, this slot is set to the delay count.
   (mutable branch-countdown)
   
   ;; *** These two slots are used both by the queuing noise and the
   ;; scheduling noise.
   ;;
   ;; All the instructions that are pending and don't have any
   ;; unresolved dependents. We don't list branches here even if they
   ;; would otherwise qualify. They are listed above.
   (mutable emittable-insts-sset)
   
   ;; list of queued branches. We handle these specially, because they
   ;; have to be emitted at a specific place (e.g. one slot before the
   ;; end of the block).
   (mutable queued-branches )

   ;; *** state used by the scheduler during instruction scheduling
   ;;
   ;; the instructions who would have had a read dependent removed if
   ;; it were not for a delay slot. This is a list of lists. Each
   ;; element in the top level list corresponds to yet another cycle of
   ;; delay. Each element in the second level lists is a dotted pair,
   ;; holding the dependency instruction and the dependent to remove.
   (mutable delayed )

   ;; The emittable insts again, except this time as a list sorted by depth.
   (mutable emittable-insts-queue )

  ;; Whether or not to collect dynamic statistics. This is just the same as
  ;; *COLLECT-DYNAMIC-STATISTICS* but is faster to reference.))
   (mutable collect-dynamic-statistics)))

(defprinter (segment) type)
  
(define (segment-current-index segment)
  (segment-%current-index segment))

(define (segment-current-index-set! segment new-value)
  ;; FIXME: It would be lovely to enforce this, but first FILL-IN will
  ;; need to be convinced to stop rolling SEGMENT-CURRENT-INDEX
  ;; backwards.
  ;;
  ;; Enforce an observed regularity which makes it easier to think
  ;; about what's going on in the (legacy) code: The segment never
  ;; shrinks. -- WHN the reverse engineer
  (define-syntax-rule (replace new old seg)
    (begin
      (segment-buffer-set! segment new)
      (let ((n (bytevector-length old)))
	(bytevector-copy! old 0 new 0 n))))
		
  (let* ((buffer (segment-buffer segment))
         (new-buffer-size (bytevector-length buffer)))
    ;; Make sure the array is big enough.
    (when (<= new-buffer-size new-value)
	  (while (> new-buffer-size new-value))
	  ;; When we have to increase the size of the array, we want to
	  ;; roughly double the vector length: that way growing the array
	  ;; to size N conses only O(N) bytes in total. But just doubling
	  ;; the length would leave a zero-length vector unchanged. Hence,
	  ;; take the MAX with 1..
	  (set! new-buffer-size (max 1 (* 2 new-buffer-size)))

	  (let ((new-buffer (make-bytevector new-buffer-size)))
	    (replace new-buffer buffer segment)))

    ;; Now that the array has the intended next free byte, we can point to it.
    (segment-%current-index-set! segment new-value)))

;;; Various functions (like BACK-PATCH-FUN or CHOOSER-WORST-CASE-FUN)
;;; aren't cleanly parameterized, but instead use
;;; SEGMENT-CURRENT-INDEX and/or SEGMENT-CURRENT-POSN as global
;;; variables. So code which calls such functions needs to modify
;;; SEGMENT-CURRENT-INDEX and SEGMENT-CURRENT-POSN. This is left over
;;; from the old new-assem.lisp C-style code, and so all the
;;; destruction happens to be done after other uses of these slots are
;;; done and things basically work. However, (1) it's fundamentally
;;; nasty, and (2) at least one thing doesn't work right: OpenMCL
;;; properly points out that SUBSEQ's indices aren't supposed to
;;; exceed its logical LENGTH, i.e. its FILL-POINTER, i.e.
;;; SEGMENT-CURRENT-INDEX.
;;;
;;; As a quick fix involving minimal modification of legacy code,
;;; we do such sets of SEGMENT-CURRENT-INDEX and SEGMENT-CURRENT-POSN
;;; using this macro, which restores 'em afterwards.
;;;
;;; FIXME: It'd probably be better to cleanly parameterize things like
;;; BACK-PATCH-FUN so we can avoid this nastiness altogether.
(define-syntax-rule (with-modified-segment-index-and-posn
		     (segment index posn) . body)

  (let* ((n-segment segment)
	 (old-index (segment-current-index n-segment))
	 (old-posn (segment-current-posn n-segment)))
    (dynamic-wind
	(lambda x #f)
	(lambda ()
	  (segment-current-index-set! n-segment index)
	  (segment-current-posn-set!  n-segment posn)
	  . body)
	(lambda x
	  (segment-current-index-set! n-segment old-index)
	  (segment-current-posn-set!  n-segment old-posn)))))

;;;; structures/types used by the scheduler
(!def-boolean-attribute instruction
  ;; This attribute is set if the scheduler can freely flush this
  ;; instruction if it thinks it is not needed. Examples are NOP and
  ;; instructions that have no side effect not described by the
  ;; writes.
  flushable
  ;; This attribute is set when an instruction can cause a control
  ;; transfer. For test instructions, the delay is used to determine
  ;; how many instructions follow the branch.
  branch
  ;; This attribute indicates that this ``instruction'' can be
  ;; variable length, and therefore had better never be used in a
  ;; branch delay slot.
  variable-length)

(define-record-type (inst make-instruction instruction?)
  (parent sset-element)

  (protocol
   (lambda (n)
     (lambda (number emitter attributes delay)
       (let ((p (n number)))
	 (p emitter attributes delay #f 
	    (make-sset) (make-sset) (make-sset) (make-sset))))))
	    
  (fields
   ;; The function to envoke to actually emit this instruction. Gets called
   ;; with the segment as its one argument.
  (mutable emitter)

  ;; The attributes of this instruction.
  (mutable attributes)

  ;; Number of instructions or cycles of delay before additional
  ;; instructions can read our writes.
  (mutable delay)

  ;; the maximum number of instructions in the longest dependency
  ;; chain from this instruction to one of the independent
  ;; instructions. This is used as a heuristic at to which
  ;; instructions should be scheduled first.
  (mutable depth)

  ;; Note: When trying remember which of the next four is which, note
  ;; that the ``read'' or ``write'' always refers to the dependent
  ;; (second) instruction.
  ;;
  ;; instructions whose writes this instruction tries to read
  (mutable read-dependencies)

  ;; instructions whose writes or reads are overwritten by this instruction
  (mutable write-dependencies)

  ;; instructions which write what we read or write
  (mutable write-dependents)

  ;; instructions which read what we write
  (mutable read-dependents)))

(define instruction inst)
  

(add-printer instruction
  (lambda (inst stream)    
    (format stream
            "emitter=~S"
            (let ((emitter (inst-emitter inst)))
              (if emitter
                  (procedure-name emitter)
                  '<flushed>)))
    (when (inst-depth inst)
	  (format stream ", depth=~W" (inst-depth inst)))))


;;;; the scheduler itself

(define-syntax without-scheduling   
  (syntax-rules ()
    ((_ ()        . body)
     (without-scheduling (%%current-segment%%)  . body))
    ((_ (segment) body ...)
;;     "Execute BODY (as a PROGN) without scheduling any of the instructions
;;   generated inside it. This is not protected by UNWIND-PROTECT, so
;;   DO NOT use THROW or RETURN-FROM to escape from it."
  
;; FIXME: Why not just use UNWIND-PROTECT? Or is there some other
;; reason why we shouldn't use THROW or RETURN-FROM?
     (let* ((seg segment)
            (var (segment-run-scheduler seg)))
       (when var
         (schedule-pending-instructions seg)
         (setf (segment-run-scheduler seg) #f))
       body ...
       (setf (segment-run-scheduler seg) var)))))

;;TODO, define the syntax parameters!
(define-syntax-rule (note-dependencies (segment inst) body ...)
  (let ((-segment segment) (-inst inst))
    (syntax-parameterize
     ((reads 
       (syntax-rules () 
	 ((_ loc)
	  (note-read-dependency) -segment -inst loc)))

      (writes
       (syntax-rules () 
	 ((_ loc . keys)
	  (note-write-dependency -segment -inst loc . keys)))))
     body ...)))



(define (note-read-dependency segment inst read)
  (let-values (((loc-num size) (location-number read)))
    (when loc-num
      ;; Iterate over all the locations for this TN.
      (do ((index loc-num (+ 1 index))
           (end-loc (+ loc-num (or size 1))))
          ((>= index end-loc))
        (let ((writers (vector-ref (segment-writers segment) index)))
          (when writers
            ;; The inst that wrote the value we want to read must have
            ;; completed.
            (let ((writer (car writers)))
              (sset-adjoin writer (inst-read-dependencies inst))
              (sset-adjoin inst (inst-read-dependents writer))
              (sset-delete writer (segment-emittable-insts-sset segment))
              ;; And it must have been completed *after* all other
              ;; writes to that location. Actually, that isn't quite
              ;; true. Each of the earlier writes could be done
              ;; either before this last write, or after the read, but
              ;; we have no way of representing that.
              (dolist (other-writer (cdr writers))
                (sset-adjoin other-writer (inst-write-dependencies writer))
                (sset-adjoin writer (inst-write-dependents other-writer))
                (sset-delete other-writer
                             (segment-emittable-insts-sset segment))))
            ;; And we don't need to remember about earlier writes any
            ;; more. Shortening the writers list means that we won't
            ;; bother generating as many explicit arcs in the graph.
            (set-cdr! writers '())))
        (set! inst (cons (vector-ref (segment-readers segment) index) inst)))))
  (values))



(define* (note-write-dependency segment inst write #:key (partially #f))
  (let-values  (((loc-num size)
		 (location-number write)))
    (when loc-num
      ;; Iterate over all the locations for this TN.
      (do ((index loc-num (1+ index))
           (end-loc (+ loc-num (or size 1))))
          ((>= index end-loc))
        ;; All previous reads of this location must have completed.
        (dolist (prev-inst (vector-ref (segment-readers segment) index))
	  (unless (eq? prev-inst inst)
            (sset-adjoin prev-inst (inst-write-dependencies inst))
            (sset-adjoin inst (inst-write-dependents prev-inst))
            (sset-delete prev-inst (segment-emittable-insts-sset segment))))
        (when partially
          ;; All previous writes to the location must have completed.
          (dolist (prev-inst (vector-ref (segment-writers segment) index))
	    (sset-adjoin prev-inst (inst-write-dependencies inst))
            (sset-adjoin inst (inst-write-dependents prev-inst))
            (sset-delete prev-inst (segment-emittable-insts-sset segment)))
          ;; And we can forget about remembering them, because
          ;; depending on us is as good as depending on them.
          (vector-set! (segment-writers segment) index #f))
        (set! inst (cons (vector-ref (segment-writers segment) index)
			 inst)))))
  (values))

;;; This routine is called by due to uses of the INST macro when the
;;; scheduler is turned on. The change to the dependency graph has
;;; already been computed, so we just have to check to see whether the
;;; basic block is terminated.
(define (queue-inst segment inst)
  (aver (segment-run-scheduler segment))
  (let ((countdown (segment-branch-countdown segment)))
    (when (> countdown 0)
	  (set! countdown (- countdown 1))
	  (aver (not (instruction-attribute? (inst-attributes inst)
					     variable-length))))
    (cond ((instruction-attribute? (inst-attributes inst) branch)
           (unless (> countdown 0)
		   (set! countdown (inst-delay inst)))
	   (segment-queued-branches-set! 
	    segment
	    (cons (cons countdown inst)
		  (segment-queued-branches segment))))

          (else
           (sset-adjoin inst (segment-emittable-insts-sset segment))))

    (when (> countdown 0)
	  (segment-branch-countdown-set! segment countdown)
	  (when (= 0 countdown)
		(schedule-pending-instructions segment)))

    (values)))


;;; a utility for maintaining the segment-delayed list. We cdr down
;;; list n times (extending it if necessary) and then push thing on
;;; into the car of that cons cell.
(define (add-to-nth-list li thing n)
  (let ((l (if (pair? li) li (list '()))))
    (let loop ((l l) (i n))
      (if (= i 0)
	  (set-car! li (cons thing (car li)))
	  (aif (it) (cdr li)
	       (loop it (- i 1))
	       (let ((it (cons '() '())))
		 (set-cdr! l it)
		 (loop it (- i 1))))))
    l))

;;; Emit all the pending instructions, and reset any state. This is
;;; called whenever we hit a label (i.e. an entry point of some kind)
;;; and when the user turns the scheduler off (otherwise, the queued
;;; instructions would sit there until the scheduler was turned back
;;; on, and emitted in the wrong place).
(define (schedule-pending-instructions segment)
  (aver (segment-run-scheduler segment))

  ;; Quick blow-out if nothing to do.
  (cond 
   ((and (sset-empty? (segment-emittable-insts-sset segment))
	 (null? (segment-queued-branches segment)))
    (values))

  ;; Note that any values live at the end of the block have to be
  ;; computed last.
   (else
    (let ((emittable-insts (segment-emittable-insts-sset segment))
	  (writers         (segment-writers segment)))
     (dotimes (index (length writers))
	(let* ((writer      (vector-ref writers index))
	       (inst        (car writer))
	       (overwritten (cdr writer)))
           (when writer
              (when overwritten
		    (let ((write-dependencies (inst-write-dependencies inst)))
                    (dolist (other-inst overwritten)
                (sset-adjoin inst (inst-write-dependents other-inst))
                (sset-adjoin other-inst write-dependencies)
                (sset-delete other-inst emittable-insts))))
	      ;; If the value is live at the end of the block, 
	      ;; we can't flush it.
	      (attribute-set!
	       (instruction-attribute? (inst-attributes inst) flushable)
	       #f)))))

	   ;; Grovel through the entire graph in the forward direction finding
  ;; all the leaf instructions.
  
  (letrec ((grovel-inst 
	    (lambda (inst)
	      (let ((max 0))
		(do-sset-elements (dep (inst-write-dependencies inst))
		   (let ((dep-depth (or (inst-depth dep) (grovel-inst dep))))
		     (when (> dep-depth max)
			   (set! max dep-depth))))
		(do-sset-elements (dep (inst-read-dependencies inst))
		   (let ((dep-depth
			  (+ (or (inst-depth dep) (grovel-inst dep))
			     (inst-delay dep))))
		     (when (> dep-depth max)
		       (set! max dep-depth))))

		(cond ((and (sset-empty? (inst-read-dependents inst))
			    (instruction-attribute? (inst-attributes inst)
						    flushable))
		       (inst-emitter-set! inst #f)
		       (inst-depth-set!   inst max)
		       max)
                     (else
                      (inst-depth-set! inst max)
		      max))))))
    (let ((emittable-insts '())
          (delayed         '()))
      (do-sset-elements (inst (segment-emittable-insts-sset segment))
	(grovel-inst inst)
        (if (zero? (inst-delay inst))
            (set! emittable-insts (cons inst emittable-insts))
            (set! delayed
                  (add-to-nth-list delayed inst (1- (inst-delay inst))))))
      (segment-emittable-insts-queue-set! 
       segment
       (sort emittable-insts (lambda (x y) (> (inst-depth x) (inst-depth y)))))
      (segment-delayed-set! segment delayed))

    (dolist (branch (segment-queued-branches segment))
	    (grovel-inst (cdr branch))))

  ;; Accumulate the results in reverse order. Well, actually, this
  ;; list will be in forward order, because we are generating the
  ;; reverse order in reverse.
  (let ((results '()))
    ;; Schedule all the branches in their exact locations.
    (let ((insts-from-end (segment-branch-countdown segment)))
      (dolist (branch (segment-queued-branches segment))
	(let ((inst (cdr branch)))
          (dotimes (i (- (car branch) insts-from-end))
            ;; Each time through this loop we need to emit another
            ;; instruction. First, we check to see whether there is
            ;; any instruction that must be emitted before (i.e. must
            ;; come after) the branch inst. If so, emit it. Otherwise,
            ;; just pick one of the emittable insts. If there is
            ;; nothing to do, then emit a nop. ### Note: despite the
            ;; fact that this is a loop, it really won't work for
            ;; repetitions other then zero and one. For example, if
            ;; the branch has two dependents and one of them dpends on
            ;; the other, then the stuff that grabs a dependent could
            ;; easily grab the wrong one. But I don't feel like fixing
            ;; this because it doesn't matter for any of the
            ;; architectures we are using or plan on using.
	 (letrec ((maybe-schedule-dependent 
		   (lambda (dependents)
                     (do-sset-elements (inst dependents)
                       ;; If do-sset-elements enters the body, then there is a
                       ;; dependent. Emit it.
		       (note-resolved-dependencies segment inst)
                       ;; Remove it from the emittable insts.
                       (segment-emittable-insts-queue-set!
			segment
			(remove (lambda (x) (eq? inst x))
				(segment-emittable-insts-queue segment)))
				
                       ;; And if it was delayed, removed it from the delayed
                       ;; list. This can happen if there is a load in a
                       ;; branch delay slot.
		       (let loop ((delayed (segment-delayed segment)))
			 (if (pair? delayed)
			     (let loop2 ((prev '()) (c (car delayed)))
			       (if (pair? c)
				   (begin
				     (when (eq? (car c) inst)
					   (if (pair? prev)
					       (set-cdr! prev    (cdr c))
					       (set-car! delayed (cdr c)))
					   (loop '()))
				     (loop2 c (cdr c)))
				   (loop (cdr delayed))))
			     #f))
                       ;; And return it.
                       inst))))

	   (let ((fill (or (maybe-schedule-dependent
			    (inst-read-dependents inst))
			   (maybe-schedule-dependent
			    (inst-write-dependents inst))
			   (schedule-one-inst segment #t)
			   #:nop)))
	     
	     (set! results (cons fill results))
	     (advance-one-inst segment)
	     (set! insts-from-end (+ insts-from-end 1))
	     (note-resolved-dependencies segment inst)
	     (set! results (cons inst results))
	     (advance-one-inst segment)))))))

    ;; Keep scheduling stuff until we run out.
    (awhile (inst (schedule-one-inst segment #f))
       (set! results (cons inst results))
       (advance-one-inst segment))

    ;; Now call the emitters, but turn the scheduler off for the duration.
    (segment-run-scheduler-set! segment #f)
    (dolist (inst results)
      (if (eq? inst #:nop)
          (emit-nop segment)
          ((inst-emitter inst) segment)))
    (segment-run-scheduler-set! segment #t))

  ;; Clear out any residue left over.
  (segment-inst-number-set!          segment   0)
  (segment-queued-branches-set!      segment  '())
  (segment-branch-countdown-set!     segment  '())
  (segment-emittable-insts-sset-set! segment  (make-sset))
  (fill (segment-readers segment) #f)
  (fill (segment-writers segment) #f)

  ;; That's all, folks.
  (values))))



;;; Find the next instruction to schedule and return it after updating
;;; any dependency information. If we can't do anything useful right
;;; now, but there is more work to be done, return :NOP to indicate
;;; that a nop must be emitted. If we are all done, return NIL.
(define (schedule-one-inst segment delay-slot?)
  (let loop ((prev      '())  
             (remaining (segment-emittable-insts-queue segment)))

      (if (pair? remaining)
	(let ((inst (car remaining)))
	  (unless (and delay-slot?
		       (instruction-attribute? (inst-attributes inst)
					       variable-length))
            ;; We've got us a live one here. Go for it.

	    ;; Delete it from the list of insts.
	    (if (pair? prev)
		(set-cdr! prev (cdr remaining))
		(segment-emittable-insts-queue-set! 
		   segment
		  (cdr remaining)))

            ;; Note that this inst has been emitted.
            (note-resolved-dependencies segment inst)

	    ;; And return.
	    (if (inst-emitter inst)
		;; Nope, it's still a go. So return it.
		inst
		;; Yes, so pick a new one. We have to start
		;; over, because note-resolved-dependencies
		;; might have changed the emittable-insts-queue.
		(schedule-one-inst segment delay-slot?))))

	;; Nothing to do, so make something up.
	(cond ((segment-delayed segment)
	       ;; No emittable instructions, but we have more work to do. Emit
	       ;; a NOP to fill in a delay slot.
	       #:nop)
	      (else
	       ;; All done.
	       #f)))))



;;; This function is called whenever an instruction has been
;;; scheduled, and we want to know what possibilities that opens up.
;;; So look at all the instructions that this one depends on, and
;;; remove this instruction from their dependents list. If we were the
;;; last dependent, then that dependency can be emitted now.
(define (note-resolved-dependencies segment inst)
  (aver (sset-empty? (inst-read-dependents inst)))
  (aver (sset-empty? (inst-write-dependents inst)))

  (do-sset-elements (dep (inst-write-dependencies inst))
    ;; These are the instructions who have to be completed before our
    ;; write fires. Doesn't matter how far before, just before.
    (let ((dependents (inst-write-dependents dep)))
      (sset-delete inst dependents)
      (when (and (sset-empty? dependents)
                 (sset-empty? (inst-read-dependents dep)))
	(insert-emittable-inst segment dep))))

  (do-sset-elements (dep (inst-read-dependencies inst))
    ;; These are the instructions who write values we read. If there
    ;; is no delay, then just remove us from the dependent list.
    ;; Otherwise, record the fact that in n cycles, we should be
    ;; removed.
    (if (zero? (inst-delay dep))
        (let ((dependents (inst-read-dependents dep)))
          (sset-delete inst dependents)
          (when (and (sset-empty? dependents)
                     (sset-empty? (inst-write-dependents dep)))
            (insert-emittable-inst segment dep)))
        (segment-delayed-set! segment
              (add-to-nth-list (segment-delayed segment)
                               (cons dep inst)
                               (inst-delay dep)))))
  (values))



;;; Process the next entry in segment-delayed. This is called whenever
;;; anyone emits an instruction.
(define (advance-one-inst segment)
  (let ((delayed-stuff (let ((r (segment-delayed segment)))
			 (if (pair? r)
			     (begin
			       (segment-delayed-set! segment (cdr r))
			       (car r))
			     '()))))

    (dolist (stuff delayed-stuff)
      (if (pair? stuff)
          (let* ((dependency (car stuff))
                 (dependent  (cdr stuff))
                 (dependents (inst-read-dependents dependency)))
            (sset-delete dependent dependents)
            (when (and (sset-empty? dependents)
                       (sset-empty? (inst-write-dependents dependency)))
              (insert-emittable-inst segment dependency)))
          (insert-emittable-inst segment stuff)))))



;;; Note that inst is emittable by sticking it in the
;;; SEGMENT-EMITTABLE-INSTS-QUEUE list. We keep the emittable-insts
;;; sorted with the largest ``depths'' first. Except that if INST is a
;;; branch, don't bother. It will be handled correctly by the branch
;;; emitting code in SCHEDULE-PENDING-INSTRUCTIONS.
(define (insert-emittable-inst segment inst)
  (unless (instruction-attribute? (inst-attributes inst) branch)
    (do ((my-depth (inst-depth inst))
         (remaining (segment-emittable-insts-queue segment) (cdr remaining))
         (prev '() remaining))
      ((or (null? remaining) (> my-depth (inst-depth (car remaining))))
         (if (pair? prev)
             (set-cdr! prev (cons inst remaining))
             (segment-emittable-insts-queue-set! 
	      segment
	      (cons inst remaining))))))
  (values))

;;;; structure used during output emission

;;; common supertype for all the different kinds of annotations
(define-record-type annotation
  (protocol
   (lambda (n)
     (lambda () (n 0 #f))))

  (fields
   ;; Where in the raw output stream was this annotation emitted?
   (mutable index)
   ;; What position does that correspond to?
   (mutable posn)))

(define-record-type (label gen-label label?)
  (parent annotation)
  (protocol (lambda (n) (lambda () ((n)))))
  ;; (doesn't need any additional information beyond what is in the
  ;; annotation structure)
  )

(add-printer label
  (lambda (label stream)
    (format stream "L~A" (annotation-posn label))))

;;; a constraint on how the output stream must be aligned

(define-record-type alignment
  (parent annotation)
  (protocol
   (lambda (n)
     (lambda (bit size fill-byte)
       (let ((p (n)))
	 (p bit size fill-byte)))))
  (fields
   (mutable bits)
   (mutable size)
   (mutable fill-byte)))

;;; a reference to someplace that needs to be back-patched when
;;; we actually know what label positions, etc. are
(define-record-type back-patch
  (parent annotation)
  (protocol
   (lambda (n)
     (lambda (size fun)
       (let ((p (n)))
	 (p size fun)))))
  (fields
   (mutable size)
   (mutable fun)))

;;; This is similar to a BACK-PATCH, but also an indication that the
;;; amount of stuff output depends on label positions, etc.
;;; BACK-PATCHes can't change their mind about how much stuff to emit,
;;; but CHOOSERs can.
(define-record-type chooser
  (parent annotation)
  (protocol
   (lambda (n)
     (lambda (size align shrink worst)
       (let ((p (n)))
	 (p size align shrink worst)))))
  (fields
   ;; the worst case size for this chooser. There is this much space
   ;; allocated in the output buffer.
   (mutable size)

   ;; the worst case alignment this chooser is guaranteed to preserve
   (mutable alignment)

   ;; the function to call to determine if we can use a shorter
   ;; sequence. It returns NIL if nothing shorter can be used, or emits
   ;; that sequence and returns #t.
   (mutable maybe-shrink)

   ;; the function to call to generate the worst case sequence. This is
   ;; used when nothing else can be condensed.   
   (mutable worst-case-fun)))


;;; This is used internally when we figure out a chooser or alignment
;;; doesn't really need as much space as we initially gave it.

(define-record-type filler
  (parent annotation)
  (protocol
   (lambda (n)
     (lambda (bytes)
       (let ((p (n)))
	 (p bytes)))))
  (fields (mutable bytes)))

;;;; output functions

;;; interface: Emit the supplied BYTE to SEGMENT, growing SEGMENT if
;;; necessary.
(define (emit-byte segment byte)
  (let ((old-index (segment-current-index segment)))
    (segment-current-index-set! segment 
				(+ (segment-current-index segment) 1))

    (u8vector-set! (segment-buffer segment) old-index
		     (logand byte assembly-unit-mask))

    (segment-current-posn-set! segment (+ 1 (segment-current-posn segment)))
    
    (values)))

;;; interface: Output AMOUNT copies of FILL-BYTE to SEGMENT.
(define* (emit-skip segment amount #:optional (fill-byte 0))
  (dotimes (i amount)
	   (emit-byte segment fill-byte))
  (values))

;;; This is used to handle the common parts of annotation emission. We
;;; just assign the POSN and INDEX of NOTE and tack it on to the end
;;; of SEGMENT's annotations list.
(define (emit-annotation segment note)
  (when (annotation-posn note)
    (error "attempt to emit ~S a second time" note))
  (annotation-posn-set!  note (segment-current-posn segment))
  (annotation-index-set! note (segment-current-index segment))
  (let ((last (segment-last-annotation segment))
        (new (list note)))
    (segment-last-annotation-set! 
     segment
     (begin
       (if (pair? last)
	   (set-cdr! last new)
	   (segment-annotations-set! segment new))
       new)))
  (values))

;;; Note that the instruction stream has to be back-patched when label
;;; positions are finally known. SIZE bytes are reserved in SEGMENT,
;;; and function will be called with two arguments: the segment and
;;; the position. The function should look at the position and the
;;; position of any labels it wants to and emit the correct sequence.
;;; (And it better be the same size as SIZE). SIZE can be zero, which
;;; is useful if you just want to find out where things ended up.
(define (emit-back-patch segment size function)
  (emit-annotation segment (make-back-patch size function))
  (emit-skip segment size))

;;; Note that the instruction stream here depends on the actual
;;; positions of various labels, so can't be output until label
;;; positions are known. Space is made in SEGMENT for at least SIZE
;;; bytes. When all output has been generated, the MAYBE-SHRINK
;;; functions for all choosers are called with three arguments: the
;;; segment, the position, and a magic value. The MAYBE-SHRINK
;;; decides if it can use a shorter sequence, and if so, emits that
;;; sequence to the segment and returns T. If it can't do better than
;;; the worst case, it should return NIL (without emitting anything).
;;; When calling LABEL-POSITION, it should pass it the position and
;;; the magic-value it was passed so that LABEL-POSITION can return
;;; the correct result. If the chooser never decides to use a shorter
;;; sequence, the WORST-CASE-FUN will be called, just like a
;;; BACK-PATCH. (See EMIT-BACK-PATCH.)
(define (emit-chooser segment size alignment maybe-shrink worst-case-fun)
  (let ((chooser (make-chooser size alignment maybe-shrink worst-case-fun)))
    (emit-annotation segment chooser)
    (emit-skip segment size)
    (adjust-alignment-after-chooser segment chooser)))

;;; This is called in EMIT-CHOOSER and COMPRESS-SEGMENT in order to
;;; recompute the current alignment information in light of this
;;; chooser. If the alignment guaranteed by the chooser is less than
;;; the segment's current alignment, we have to adjust the segment's
;;; notion of the current alignment.
;;;
;;; The hard part is recomputing the sync posn, because it's not just
;;; the chooser's posn. Consider a chooser that emits either one or
;;; three words. It preserves 8-byte (3 bit) alignments, because the
;;; difference between the two choices is 8 bytes.
(define (adjust-alignment-after-chooser segment chooser)
  (let ((alignment     (chooser-alignment chooser))
        (seg-alignment (segment-alignment segment)))
    (when (< alignment seg-alignment)
      ;; The chooser might change the alignment of the output. So we
      ;; have to figure out what the worst case alignment could be.
      (segment-alignment-set! segment alignment)
      (let* ((posn      (annotation-posn chooser))
             (sync-posn (segment-sync-posn segment))
             (offset    (- posn sync-posn))
             (delta     (logand offset (1- (ash 1 alignment)))))
        (segment-sync-posn-set! segment (- posn delta)))))
  #f)

;;; This is used internally whenever a chooser or alignment decides it
;;; doesn't need as much space as it originally thought.
(define (emit-filler segment n-bytes)
  (let ((last (segment-last-annotation segment)))
    (cond ((and (pair? last) (filler? (car last)))
           (filler-bytes-set! (car last)
			      (+ (filler-bytes (car last)) n-bytes)))
          (else
           (emit-annotation segment (make-filler n-bytes))))
    (segment-current-index-set! 
     segment
     (+ (segment-current-index segment) n-bytes)))
  (values))

;;; EMIT-LABEL (the interface) basically just expands into this,
;;; supplying the SEGMENT and VOP.
(define (%emit-label segment vop label)
  (when (segment-run-scheduler segment)
	(schedule-pending-instructions segment))
  (let ((postits (segment-postits segment)))
    (segment-postits-set! segment '())
    (dolist (postit postits)
	    (emit-back-patch segment 0 postit)))
  (let ((hook (segment-inst-hook segment)))
    (when hook
	  (hook segment vop #:label label)))
  (emit-annotation segment label))


;;; Called by the EMIT-ALIGNMENT macro to emit an alignment note. We check to
;;; see if we can guarantee the alignment restriction by just outputting a
;;; fixed number of bytes. If so, we do so. Otherwise, we create and emit an
;;; alignment note.
(define* (%emit-alignment segment vop bits #:optional (fill-byte 0))
  (when (segment-run-scheduler segment)
	(schedule-pending-instructions segment))
  (let ((hook (segment-inst-hook segment)))
    (when hook
	  (hook segment vop #:align bits)))
  (let ((alignment (segment-alignment segment))
        (offset (- (segment-current-posn segment)
                   (segment-sync-posn segment))))
    (cond ((> bits alignment)
           ;; We need more bits of alignment. First emit enough noise
           ;; to get back in sync with alignment, and then emit an
           ;; alignment note to cover the rest.
           (let ((slop (logand offset (1- (ash 1 alignment)))))
             (unless (zero? slop)
		     (emit-skip segment (- (ash 1 alignment) slop) fill-byte)))
           (let ((size (logand (1- (ash 1 bits))
                               (lognot (1- (ash 1 alignment))))))
             (aver (> size 0))
             (emit-annotation segment (make-alignment bits size fill-byte))
             (emit-skip segment size fill-byte))
           (segment-alignment-set! segment bits)
           (segment-sync-posn-set! segment (segment-current-posn segment)))
          (else
           ;; The last alignment was more restrictive than this one.
           ;; So we can just figure out how much noise to emit
           ;; assuming the last alignment was met.
           (let* ((mask (1- (ash 1 bits)))
                  (new-offset (logand (+ offset mask) (lognot mask))))
             (emit-skip segment (- new-offset offset) fill-byte))
           ;; But we emit an alignment with size=0 so we can verify
           ;; that everything works.
           (emit-annotation segment (make-alignment bits 0 fill-byte)))))
  (values))

;;; This is used to find how ``aligned'' different offsets are.
;;; Returns the number of low-order 0 bits, up to MAX-ALIGNMENT.
(define (find-alignment offset)
  (let loop ((i 0))
    (if (= i max-alignment)
	max-alignment
	(if (logbit? i offset)
	    i
	    (loop (+ i 1))))))

;;; Emit a postit. The function will be called as a back-patch with
;;; the position the following instruction is finally emitted. Postits
;;; do not interfere at all with scheduling.
(define (%emit-postit segment function)
  (segment-postits-set! segment 
			(cons function 
			      (segment-postits segment)))
  (values))
;; TODO: There is a bug hidden in the following code, fix it!
;;;; output compression/position assignment stuff
;;; Grovel though all the annotations looking for choosers. When we
;;; find a chooser, invoke the maybe-shrink function. If it returns T,
;;; it output some other byte sequence.
(define (compress-output segment)
  (let times ((i 5)) ; it better not take more than one or two passes.
    (if (> i 0)
	(let ((delta 0))
	  (segment-alignment-set! segment max-alignment)
	  (segment-sync-posn-set! segment 0)
	  (let loop ((prev '())
		     (remaining (segment-annotations segment))
		     (next      (if (pair?  (segment-annotations segment))
                                    (cdr (segment-annotations segment))
                                    #f)))
	    (if (pair? remaining) 
		(let* ((note (car remaining))
		       (posn (annotation-posn note)))
		  (unless (zero? delta)
			  (set! posn (- posn delta))
			  (annotation-posn-set! note posn))
		  
		  (cond
		   ((chooser? note)
		    (with-modified-segment-index-and-posn 
		     (segment (annotation-index note)
			      posn)
		     (segment-last-annotation-set! segment prev)
		     (cond
		      (((chooser-maybe-shrink note) segment posn delta)
		       ;; It emitted some replacement.
		       (let ((new-size (- (segment-current-index segment)
					  (annotation-index note)))
			     (old-size (chooser-size note)))
			 (when (> new-size old-size)
                    (merror "~S emitted ~W bytes, but claimed its max was ~W."
			    note new-size old-size))
			 (let ((additional-delta (- old-size new-size)))
			   (when (< (find-alignment additional-delta)
				    (chooser-alignment note))
			  (merror "~S shrunk by ~W bytes, but claimed that it ~
                              preserves ~W bits of alignment."
				  note additional-delta 
				  (chooser-alignment note)))
			   (set! delta (+ delta additional-delta))
			   (emit-filler segment additional-delta))
			 (set! prev (segment-last-annotation segment))
			 (if (pair? prev)
			     (set-cdr! prev (cdr remaining))
			     (segment-annotations-set! segment
						       (cdr remaining)))))
		      (else
		       ;; The chooser passed on shrinking. Make sure it didn't
		       ;; emit anything.
		       (unless (= (segment-current-index segment)
				  (annotation-index note))
	    (merror "Chooser ~S passed, but not before emitting ~W bytes."
		    note
		    (- (segment-current-index segment)
		       (annotation-index note))))
		       ;; Act like we just emitted this chooser.
		       (let ((size (chooser-size note)))
			 (segment-current-index-set! 
			  segment
			  (+ size (segment-current-index segment)))
			 (segment-current-posn-set! 
			  segment
			  (+ size (segment-current-posn segment))))
		       ;; Adjust the alignment accordingly.
		       (adjust-alignment-after-chooser segment note)
		       ;; And keep this chooser for next time around.
		       (set! prev remaining)))))
		   ((alignment? note)
		    (unless (zero? (alignment-size note))
		     ;; Re-emit the alignment, letting it collapse if we know
		     ;; anything more about the alignment guarantees of the
		     ;; segment.
			    (let ((index (annotation-index note)))
			      (with-modified-segment-index-and-posn 
			       (segment index posn)
			       (segment-last-annotation-set! segment prev)
			       (%emit-alignment segment #f 
						(alignment-bits note)
						(alignment-fill-byte note))
			       (let* ((new-index        
				       (segment-current-index segment))
				      (size    
				       (- new-index index))
				      (old-size         
				       (alignment-size note))
				      (additional-delta 
				       (- old-size size)))
				 (when (< additional-delta 0)
                                   (merror "Alignment ~S needs more space now?  It was ~S, and is ~S now."
					   note old-size size))
				 (when (> additional-delta 0)
				       (emit-filler segment additional-delta)
				       (set! delta (+ delta additional-delta))))
			       (set! prev (segment-last-annotation segment))
			       (if (pair? prev)
				   (set-cdr! prev (cdr remaining))
				   (segment-annotations-set! 
				    segment
				    (cdr remaining)))))))
		   (else
		    (set! prev remaining)))   	  
		  (loop prev next (xcdr next)))))
      
	  (when (not (zero? delta))
		(segment-final-posn-set! segment 
					 (- (segment-final-posn segment) delta))
		(times (- i 1))))))
  (values))



;;; We have run all the choosers we can, so now we have to figure out
;;; exactly how much space each alignment note needs.
(define (finalize-positions segment)
  (let ((delta 0))
    (let loop ((prev     '())
	       (remaining (segment-annotations segment))
	       (next      (if (pair? (segment-annotations segment))
                              (cdr (segment-annotations segment))
                              #f)))
  (if (pair? remaining)
      (let* ((note (car remaining))
             (posn (- (annotation-posn note) delta)))
        (cond
         ((alignment? note)
          (let* ((bits     (alignment-bits note))
                 (mask     (1- (ash 1 bits)))
                 (new-posn (logand (+ posn mask) (lognot mask)))
                 (size     (- new-posn posn))
                 (old-size (alignment-size note))
                 (additional-delta (- old-size size)))
            (aver (<= 0 size old-size))
            (unless (zero? additional-delta)
              (segment-last-annotation-set! segment prev)
              (set! delta (+ delta additional-delta))
              (with-modified-segment-index-and-posn (segment
                                                     (annotation-index note)
                                                     posn)
                (emit-filler segment additional-delta)
                (set! prev (segment-last-annotation segment))
                (if prev
                    (set-cdr! prev next)
                    (segment-annotations-set! segment next))))))
         (else
          (annotation-posn-set! note posn)
          (set! prev remaining)
          (set! next (cdr remaining))))

	(loop prev next (xcdr next)))))

    (unless (zero? delta)
      (segment-final-posn-set!
       segment
       (- (segment-final-posn segment)
          delta))))
  (values))


;;; Grovel over segment, filling in any backpatches. If any choosers
;;; are left over, we need to emit their worst case variant.
(define (process-back-patches segment)
  (let loop ((prev      '())
	     (remaining (segment-annotations segment))
	     (next      (if (pair? (segment-annotations segment))
                            (cdr (segment-annotations segment))
                            #f)))
(if (pair? remaining)
    (let ((note (car remaining)))
      (letrec 
	  ((fill-in 
	    (lambda (function old-size)
	      (let ((index (annotation-index note))
		    (posn  (annotation-posn note)))
                 (with-modified-segment-index-and-posn (segment index posn)
                   (segment-last-annotation-set! segment prev)
                   (function segment posn)
                   (let ((new-size (- (segment-current-index segment) index)))
                     (unless (= new-size old-size)
                       (merror "~S emitted ~W bytes, but claimed it was ~W."
                              note new-size old-size)))
                   (let ((tail (segment-last-annotation segment)))
                     (if (pair? tail)
                         (set-cdr! tail next)
                         (segment-annotations-set! segment next)))
                   (set! next (if (pair? prev) (cdr prev) 
                                  next)))))))
        (cond ((back-patch? note)
               (fill-in (back-patch-fun  note)
                        (back-patch-size note)))
              ((chooser? note)
               (fill-in (chooser-worst-case-fun note)
                        (chooser-size note)))
              (else
               (set! prev remaining))))
      (loop prev next (xcdr next))))))

(define (xcdr x) (if (pair? x) (cdr x) #f))

;;;; interface to the rest of the compiler

;;; This holds the current segment while assembling. Use ASSEMBLE to
;;; change it.
;;;
;;; The double asterisks in the name are intended to suggest that this
;;; isn't just any old special variable, it's an extra-special
;;; variable, because sometimes MACROLET is used to bind it. So be
;;; careful out there..
;;;
;;; (This used to be called **CURRENT-SEGMENT** in SBCL until 0.7.3,
;;; and just *CURRENT-SEGMENT* in CMU CL. In both cases, the rebinding
;;; now done with MACROLET was done with SYMBOL-MACROLET instead. The
;;; rename-with-double-asterisks was because the SYMBOL-MACROLET made
;;; it an extra-special variable. The change over to
;;; %%CURRENT-SEGMENT%% was because ANSI forbids the use of label
;;; SYMBOL-MACROLET on special variable names, and CLISP correctly
;;; complains about this when being used as a bootstrap host.)
(define-syntax-parameter %%current-segment%% 
  (lambda (x) 
    #'**current-segment**))

(define **current-segment** (make-fluid #f))

;;; Just like %%CURRENT-SEGMENT%%, except this holds the current vop.
;;; This is used only to keep track of which vops emit which insts.
;;;
;;; The double asterisks in the name are intended to suggest that this
;;; isn't just any old special variable, it's an extra-special
;;; variable, because sometimes MACROLET is used to bind it. So be
;;; careful out there..
(define-syntax-parameter %%current-vop%% 
  (lambda (x) #'**current-vop**))

(define **current-vop** (make-fluid #f))


;;; We also MACROLET %%CURRENT-SEGMENT%% to a local holding the
;;; segment so uses of %%CURRENT-SEGMENT%% inside the body don't have
;;; to keep dereferencing the symbol. Given that ASSEMBLE is the only
;;; interface to **CURRENT-SEGMENT**, we don't have to worry about the
;;; special value becomming out of sync with the lexical value. Unless
;;; some bozo closes over it, but nobody does anything like that...
;;;
;;; FIXME: The way this macro uses MACROEXPAND internally breaks my
;;; old assumptions about macros which are needed both in the host and
;;; the target. (This is more or less the same way that PUSH-IN,
;;; DELETEF-IN, and !DEF-BOOLEAN-ATTRIBUTE break my old assumptions,
;;; except that they used GET-SETF-EXPANSION instead of MACROEXPAND to
;;; do the dirty deed.) The quick and dirty "solution" here is the
;;; same as there: use cut and paste to duplicate the defmacro in a
;;; (SB!INT:DEF!MACRO FOO (..) .. CL:MACROEXPAND ..) #+SB-XC-HOST
;;; (DEFMACRO FOO (..) .. SB!XC:MACROEXPAND ..) idiom. This is
;;; disgusting and unmaintainable, and there are obviously better
;;; solutions and maybe even good solutions, but I'm disinclined to
;;; hunt for good solutions until the system works and I can test them
;;; in isolation.
;;;
;;; The above comment remains true, except that instead of a cut-and-paste
;;; copy we now have a macrolet. This is charitably called progress.
;;; -- NS 2008-09-19

(define (label-union x y)
  (define (in x l) 
    (let ((x (syntax->datum x)))
      (or-map (lambda (y) 
		(eq? x (syntax->datum y))) 
	      l)))
 
  (let loop ((x (if (pair? x) (label-union '() x) '()))
	     (y y))
    (if (pair? y) 
	(if (in (car y) x)
	    (loop x                (cdr y))
	    (loop (cons (car y) x) (cdr y)))
	x)))
	

(define (fluid-ref1 x)
  (if (fluid? x)
      (fluid-ref x) 
      x))

(define-syntax assemble 
  (lambda (x)
    (define (defined-label? x)
      (let-values (((m x) (syntax-local-binding x)))
	(eq? m 'lexical)))

    (syntax-case x ()
      ;;Cludge to simulate ((#:optional segment vop #:key labels) body ...)
      ((_ () . body)            #'(assemble (#f      #f  ()) . body))
      ((_ (segment    ) . body) #'(assemble (segment #f  ()) . body))
      ((_ (segment vop) . body) #'(assemble (segment vop ()) . body))
      ((_ (segment vop #:labels labels) . body) 
       #'(assemble (segment vop labels) . body))
      ((_ (segment #:labels labels) . body) 
       #'(assemble (segment #f labels) . body))
      ((_ (#:labels labels) . body) 
       #'(assemble (#f #f labels) . body))


      ((_ (segment vop (labels ...)) body ...)
        (let ()          
	  (define (not-label-name? thing)
	    (let ((thing (syntax->datum thing)))
	      (not (and thing (symbol? thing)))))

	  (let* ((seg-var        (datum->syntax x  (gensym "segment-")))
		 (vop-var        (datum->syntax x  (gensym "vop-")))
		 (visible-labels (remove not-label-name? #'(body ...)))
		 (new-labels 
		  (remove defined-label?
			  (label-union #'(labels ...)
				       visible-labels))))
       (with-syntax ((seg-var seg-var) (vop-var vop-var))
	 #`(let* ((seg-var #,(if (syntax->datum #'segment)
                                 #'segment
				 #'(fluid-ref1 %%current-segment%%)))
                  (vop-var #,(if (syntax->datum #'vop)
                                 #'vop
				 #'(fluid-ref1 %%current-vop%%))))
	     (with-fluids(
			  #,@(if (syntax->datum #'segment)
                                 #`((**current-segment** 
                                     seg-var))
                                 '())
			  #,@(if (syntax->datum #'vop)
                                 #`((**current-vop** 
                                     vop-var))
                                 '()))
	        (let* (
		       #,@(map (lambda (name)
                                 #`(#,name (gen-label)))
                               new-labels))
		  
		  (syntax-parameterize
		   ((%%current-segment%% (lambda (x) #'seg-var))
		    (%%current-vop%%     (lambda (x) #'vop-var)))
		   #,@(map (lambda (form)
			    (if (not (not-label-name? form))
				#`(emit-label #,form)
                                form))
			  #'(body ...)))))))))))))

(define-syntax inst 
  (lambda (x)
    (syntax-case x ()
      ((_  instruction  args ...)
       (let* ((ins  (syntax->datum #'instruction))
	      (inst (hash-ref *assem-instructions* ins)))
	 (cond ((not inst)
		(merror "unknown instruction: ~S" 
			(syntax->datum #'instruction)))
	       ((procedure? inst)
		(inst (stx-cdr x)))
	       (else
		#`(#,inst %%current-segment%% %%current-vop%% args ...))))))))


;;; Note: The need to capture MACROLET bindings of %%CURRENT-SEGMENT%%
;;; and %%CURRENT-VOP%% prevents this from being an ordinary function.
(define-syntax-rule (emit-label label)
  (%emit-label %%current-segment%% %%current-vop%% label))

;;; Note: The need to capture MACROLET bindings of
;;; %%CURRENT-SEGMENT%% prevents this from being an ordinary function.
(define-syntax-rule (emit-postit function)
  (%emit-postit %%current-segment%% function))

;;; Note: The need to capture SYMBOL-MACROLET bindings of
;;; **CURRENT-SEGMENT* and (%%CURRENT-VOP%%) prevents this from being an
;;; ordinary function.
(define-syntax emit-alignment 
  (syntax-rules ()
    ((_ bits)
     (emit-alignment bits 0))
    ((_ bits fill-byte)
     (%emit-alignment %%current-segment%%
		      %%current-vop%%
		      bits fill-byte))))



(define* (label-position label #:optional (if-after #f) (delta 0))
  "Return the current position for LABEL. Chooser maybe-shrink functions
   should supply IF-AFTER and DELTA in order to ensure correct results."
  (let ((posn (annotation-posn label)))
    (if (and if-after (> posn if-after))
        (- posn delta)
        posn)))


(define (append-segment segment other-segment)
  "Append OTHER-SEGMENT to the end of SEGMENT. Don't use OTHER-SEGMENT
   for anything after this."
  (when (segment-run-scheduler segment)
	(schedule-pending-instructions segment))
  (let ((postits (segment-postits segment)))
    (segment-postits-set! segment (segment-postits other-segment))
    (dolist (postit postits)
	    (emit-back-patch segment 0 postit)))

  (when (not (or x86 x86-64))
	(%emit-alignment segment #f max-alignment))

  (when (or x86 x86-64)
	(unless (eq? #:elsewhere (segment-type other-segment))
		(%emit-alignment segment #f max-alignment)))

  (let ((segment-current-index-0 (segment-current-index segment))
        (segment-current-posn-0  (segment-current-posn  segment)))
    (segment-current-index-set! 
     segment
     (+ (segment-current-index segment) 
	(segment-current-index other-segment)))

    (bytevector-copy! (segment-buffer segment)       
		      segment-current-index-0

		      (segment-buffer other-segment) 0

		      (segment-current-index other-segment))

    (segment-buffer-set! other-segment #f) ; to prevent accidental reuse

    (segment-current-posn-set!
     segment
     (+ (segment-current-posn segment)
	(segment-current-posn other-segment)))

    (let ((other-annotations (segment-annotations other-segment)))
      (when other-annotations
        (dolist (note other-annotations)
          (annotation-index-set! 
	   note
	   (+ (annotation-index note) segment-current-index-0))

          (annotation-posn-set!
	   note
	   (+ (annotation-posn note) segment-current-posn-0))

	  ;; This SEGMENT-LAST-ANNOTATION code is confusing. Is it really
	  ;; worth enough in efficiency to justify it? -- WHN 19990322
	  (let ((last (segment-last-annotation segment)))
	    (if (pair? last)
		(set-cdr! last other-annotations)
		(segment-annotations-set! segment other-annotations)))

	  (segment-last-annotation-set! 
	   segment
	   (segment-last-annotation other-segment))))))
  (values))



(define (finalize-segment segment)
  "Do any final processing of SEGMENT and return the total number of bytes
   covered by this segment."
  (when (segment-run-scheduler segment)
	(schedule-pending-instructions segment))
  (segment-run-scheduler-set! segment #f)
  (let ((postits (segment-postits segment)))
    (segment-postits-set! segment '())
    (dolist (postit postits)
	    (emit-back-patch segment 0 postit)))

  (segment-final-index-set! segment (segment-current-index segment))
  (segment-final-posn-set!  segment (segment-current-posn segment))
  (segment-inst-hook-set!   segment #f)
  #;(compress-output segment)
  (finalize-positions segment)
  (process-back-patches segment)
  (segment-final-posn segment))

;;; Call FUNCTION on all the stuff accumulated in SEGMENT. FUNCTION
;;; should accept a single vector argument. It will be called zero or
;;; more times on vectors of the appropriate byte type. The
;;; concatenation of the vector arguments from all the calls is the
;;; contents of SEGMENT.
;;;
;;; KLUDGE: This implementation is sort of slow and gross, calling
;;; FUNCTION repeatedly and consing a fresh vector for its argument
;;; each time. It might be possible to make a more efficient version
;;; by making FINALIZE-SEGMENT do all the compacting currently done by
;;; this function: then this function could become trivial and fast,
;;; calling FUNCTION once on the entire compacted segment buffer. --
;;; WHN 19990322
(define (on-segment-contents-vectorly segment function)
  (let ((buffer (segment-buffer segment))
        (i0 0))
    (define (frob i0 i1)
      (when (< i0 i1)
	    (function (subseq buffer i0 i1))))
    (dolist (note (segment-annotations segment))
	    (when (filler? note)
		  (let ((i1 (annotation-index note)))
		    (frob i0 i1)
		    (set! i0 (+ i1 (filler-bytes note))))))
    (frob i0 (segment-final-index segment)))
  (values))

;;; Write the code accumulated in SEGMENT to STREAM, and return the
;;; number of bytes written.
(define (write-segment-contents segment stream)
  (let ((result 0))
    (on-segment-contents-vectorly 
     segment
     (lambda (v)
       (set! result (+ result (length v)))
       (write-sequence v stream)))
    result))

;;;; interface to the instruction set definition

;;; Define a function named NAME that merges its arguments into a
;;; single integer and then emits the bytes of that integer in the
;;; correct order based on the endianness of the target-backend.
(define (p x) (pk (number->string x 2)) x)

(define (mk-contents total-bits ar exprs specs is)
  (let* ((overall-mask (ash -1 total-bits))
	 (num-bytes    (let-values (((quo rem)
				     (euclidean/ total-bits
						 assembly-unit-bits)))

			 (unless (zero? rem)
				 (merror "~W isn't an even multiple of ~W."
					 total-bits assembly-unit-bits))
			 quo))
	 (bytes        (make-vector num-bytes '())))

    (define (push x i)
      (vector-set! 
       bytes i 
       (cons x (vector-ref bytes i))))
		   
    (define (add-content expr spec i)
      (let* (	    
	     (spec expr)
	     (size (byte-size     spec))
	     (posn (byte-position spec)))

    (when (ldb-test (byte size posn) overall-mask)
	  (merror "The byte spec ~S either overlaps another byte spec, or ~%extends past the end."
		  spec))

    (ldb-set! spec overall-mask -1)
    (let-values  (((start-byte offset)
		   (floor/ posn assembly-unit-bits)))
      (let ((end-byte (floor/ (1- (+ posn size))
			     assembly-unit-bits))
	    (maybe-ash (lambda (expr offset)
			 (if (zero? offset)
			     expr
			     (lambda () 
			       (ash (expr) offset))))))
	(cond ((zero? size))
	      ((= start-byte end-byte)
	       (push (maybe-ash 
		      (lambda () (ldb (byte size 0) (vector-ref ar i)))
                      offset)
                     start-byte))
	      (else
	       (push (maybe-ash
		      (lambda ()
			(ldb (byte (- assembly-unit-bits offset) 0)
			     (vector-ref ar i)))
		      offset)
		     start-byte)

	       (let loop ((index (+ 1 start-byte)))
		 (if (< index end-byte)
		     (begin
		       (push
			(lambda ()
			  (ldb (byte assembly-unit-bits
				     (- (* assembly-unit-bits
					   (- index start-byte))
					offset))
			       (vector-ref ar i)))
			index)
		       (loop (+ 1 index)))))

	       (let ((len (remainder (+ size offset)
				     assembly-unit-bits)))		   
		   (push
		    (lambda ()
		      (ldb (byte (if (zero? len)
				     assembly-unit-bits
				     len)
				 (- (* assembly-unit-bits
				       (- end-byte start-byte))
				    offset))
			   (vector-ref ar i)))
		    end-byte))))))))

    (for-each add-content exprs specs is)
    
    (unless (= overall-mask -1)
	    (error "There are holes."))
    
    (let ((forms '()))
      (let loop ((i 0) (r '()))
	(if (< i num-bytes)
	    (let ((pieces (vector-ref bytes i)))
	      (aver (pair? pieces))
	      (loop (+ i 1)
		    (cons
		     (if (pair? pieces)
			 (lambda (segment)
			   (emit-byte segment
				      (let loop ((p (cdr pieces))
						 (r ((car pieces))))
					(if (pair? p)
					    (loop (cdr p) (logior r ((car p))))
					    r))))
			 (lambda (segment)
			   (emit-byte segment ((car pieces)))))
		     r)))
	    (case *backend-byte-order*
	      ((#:little-endian) r)
	      ((#:big-endian)    (reverse r))
	      ((#f) 
	       (warn "*backend-byte-order* has not ben setted in backend code")
	       r)))))))
	      


(define-syntax define-bitfield-emitter 
  (lambda (x)
   (syntax-case x ()
     ((_ name total-bits  byte-specs ...)
      (with-syntax (((arg-names ...) (map (lambda (x) 
					    (datum->syntax #'name 
							   (gensym "arg")))
					  #'(byte-specs ...)))
		    
		    ((i         ...) (let loop ((i 0) (bc #'(byte-specs ...)))
				       (if (pair? bc)
					   (cons i (loop (+ i 1) (cdr bc)))
					   '())))		    
		    (n               (length #'(byte-specs ...)))
		    ((specs     ...) (map (lambda (x)
					    #`(quote #,x))
					  #'(byte-specs ...)))
		    (ar              (datum->syntax #'name (gensym "ar")))
		    (segment-arg  (datum->syntax #'name (gensym "SEGMENT-"))))
				       
	#'(define name 
	    (let* ((tot  total-bits)
		   (ar   (make-vector n))
		   (fs   (mk-contents tot ar 
				      (list byte-specs ...) 
				      (list specs     ...) 
				      (list i         ...))))
	      (lambda (segment-arg arg-names ...)
		(vector-set! ar i arg-names) ...
		(for-each (lambda (x) (x segment-arg)) fs)
		'name))))))))

(define (nu x) (if (null? x) #''() x))
(define (grovel-lambda-list stx lambda-list vop-var)
  (define (gensm x) (datum->syntax stx (gensym x)))
  (let* ((lambda-list  (stx->list lambda-list))
         (segment-name (stx-car lambda-list))
         (vop-var      (or vop-var (gensm "vop"))))
    (let ((new-lambda-list '()))
      (define (collect x) (set! new-lambda-list (cons x new-lambda-list)))
      (collect segment-name)
      (collect vop-var)
      (letrec
          ((grovel 
	    (lambda (state lambda-list)
	      (if (stx-pair? lambda-list)
		(let ((param (stx->list (stx-car lambda-list))))
		  (cond
		   ((keyword? (syntax->datum param))
		    (collect param)
		    (grovel (syntax->datum param) (stx-cdr lambda-list)))
		   (else
                   (case state
                     ((#f)
		      (collect param)
                      #`(cons #,param #,(let ((a 
                                               (grovel state 
                                                       (stx-cdr lambda-list))))
                                          (if (null? a)
                                              #''()
                                              a))))

                     ((#:optional)
                      (let-values (((name default supplied?)
				    (if (stx-pair? param)
					(values (car  param)
						(cadr param)
						(if (pair? (cddr param))
						    (caddr param)
						    (gensm "supplied?-")))
					(values param 
						'()
						(gensm "supplied?-")))))
                        (collect (list name default supplied?))
                        #`(and #,supplied?
			       (cons #,(if (pair? name)
					   (cadr name)
					   name)
				     #,(nu (grovel state 
                                                   (stx-cdr lambda-list)))))))
                     ((#:key)
                      (let-values (((name default supplied?)
				    (if (pair? param)
					(values (car  param)
						(cadr param)
						(if (pair? (cddr param))
						    (caddr param)
						    (gensm "supplied?-")))
					(values param 
						'()
						(gensm "supplied?-")))))

                        (collect (list name default supplied?))
                        (let-values (((key var)
				      (if (pair? name)
					  (values (car name) (cadr name))
					  (values (symbol->keyword 
                                                   name) name))))
                          #`(append (and #,supplied? (list #',key #,var))
				    #,(grovel state (stx-cdr lambda-list))))))
                     ((#:rest)
                      (collect param)
                      (grovel state (stx-cdr lambda-list))
                      param)))))
		#'()))))
        (let ((reconstructor (nu (grovel #f (stx-cdr lambda-list)))))
          (values (reverse new-lambda-list)
                  segment-name
                  vop-var
                  reconstructor))))))


(define (extract-nths index glue list-of-lists-of-lists)
  (map (lambda (list-of-lists)
	 (cons glue
	       (map (lambda (list)
		      (list-ref list index))
		    list-of-lists)))
       list-of-lists-of-lists))


(define (handle-options name options)
  (define (get-items x)
    (syntax-case x ()
      ((n args ...)
       (values (syntax->datum #'n) #'(args ...)))
      (n 
       (values (syntax->datum #'n) '()))))

  (let*  ((vop-var      #f)
	  (emitter      #f)
	  (decls        '())
	  (attributes   '())
	  (cost         #f)
	  (dependencies '())
	  (ddelay       #f)
	  (pinned       #f)
	  (pdefs        '()))
    (for-each 
     (lambda (x)
       (let-values (((key args) (get-items x)))
	 (case key
	   ((:emitter)
	    (when emitter
		  (error "You can only specify :EMITTER once per instruction."))
	    (set! emitter args))
	   ((:declare)
	    (set! decls (append decls args)))
	   ((:attributes)
	    (set! attributes (append attributes args)))
	   ((:cost)
	    (set! cost (first args)))
	   ((:dependencies)
	    (set! dependencies (append dependencies args)))
	   ((:delay)
	    (when ddelay
		  (error "You can only specify :DELAY once per instruction."))
	    (set! ddelay args))
	   ((:pinned)
	    (set! pinned #t))
	   ((:vop-var)
	    (if vop-var
               (error "You can only specify :VOP-VAR once per instruction.")
               (set! vop-var (car args))))
          ((:printer)
           (push #`((xx
		    #,(gen-printer-def-forms-def-form
		       name			   
		       #'(let ((*print-right-margin* 1000)
			       (format #f "~@:(~A[~A]~)" '#,name #,args)))
		       (stx-cdr x))))
                 pdefs))
          ((:printer-list)
           ;; same as :PRINTER, but is EVALed first, and is a list of
           ;; printers
           (push
	    #'(xx 
	       (map (lambda (printer)
		      #,(sb!disassem:gen-printer-def-forms-def-form
			 #'name
			 #'(let ((*print-right-margin* 1000))
			     (format #f "~@:(~A[~A]~)" 
				     '#,name #,printer))
			 printer
			 #f))
		    #,(cadr x)))
            pdefs))
          (else
           (error "unknown option: ~S" options)))))
     options)
    (values vop-var emitter decls attributes cost dependencies
            ddelay pinned pdefs)))
   

(define (pp x) (pretty-print (syntax->datum x)) x)

(define-syntax define-instruction
  (lambda (x)
    (syntax-case x ()
      ((_ name lambda-list options ...)
       (let ((sym-name (symbol->string (syntax->datum #'name)))
	     (postits  (datum->syntax #'name (gensym "postits"))))
	 (define (gen x) 
	   (datum->syntax #'name (string->symbol x)))
	 
       (let-values  (((vop-var emitter decls attributes cost 
		       dependencies ddelay pinned pdefs)
		      (handle-options #'name #'(options ...))))
       (let-values
		     (((new-lambda-list segment-name vop-name arg-reconstructor)
		       (grovel-lambda-list #'name #'lambda-list vop-var)))
	 (set! pdefs (reverse pdefs))
	 (unless cost (set! cost 1))
	 (push #`(let ((hook (segment-inst-hook #,segment-name)))
		   (when hook
			 (hook #,segment-name #,vop-name #,sym-name
			       #,arg-reconstructor)))
	       emitter)
	 (push #`(dolist (postit #,postits)
			 (emit-back-patch #,segment-name 0 postit))
	       emitter)

      (when *dyncount*
	    (push #`(when (segment-collect-dynamic-statistics #,segment-name)
			  (let* ((info (ir2-component-dyncount-info
					(component-info
					 *component-being-compiled*))))
			    (costs (dyncount-info-costs info))
			    (block-number (block-number
					   (ir2-block-block
					    (vop-block #,vop-name)))))
			  (vector-set costs block-number
				      (+
				       (vector-ref costs block-number
						   #,cost))))
		  emitter))

      (when *assem-scheduler?*
	(if pinned
            (set! emitter
                  #`((when (segment-run-scheduler #,segment-name)
			   (schedule-pending-instructions #,segment-name))
		     #,@emitter))
            (let ((flet-name
                   (gen (string-append "emit-" sym-name "-inst-")))
		  (inst-name (datum->syntax #'name (gensym "inst-"))))
              (set! emitter 
		    #`((letrec ((#,flet-name 
				 (lambda (#,segment-name)
				   ,@emitter)))
			 (if (segment-run-scheduler #,segment-name)
			     (let ((#,inst-name
				    (make-instruction
				     (incf (segment-inst-number
					    ,segment-name))
				     #,flet-name
				     (instruction-attributes
				      ,@attributes)
				     (progn ,@ddelay))))
			       #,@(if dependencies
				      #`((note-dependencies
					  (#,segment-name #,inst-name)
					  #,@dependencies))
				      '())
			       (queue-inst #,segment-name #,inst-name))
			     (#,flet-name #,segment-name))))))))

      (with-syntax ((defun-name  (gen (string-append sym-name
						     "-inst-emitter"))))
       
        #`(begin
         (define defun-name 
           (lambda** #,new-lambda-list
             (let ((#,postits (segment-postits #,segment-name)))
               ;; Must be done so that contribs and user code doing
               ;; low-level stuff don't need to worry about this.
               (segment-postits-set! #,segment-name '())
               (let ()
                 (syntax-parameterize
                  ((%%current-segment%% 
                    (lambda (x)
                      (error "You can't use INST without an ASSEMBLE inside emitters."))))
                  ;; KLUDGE: Some host lisps (CMUCL 18e Sparc at least)
                  ;; can't deal with this declaration, so disable it on host
                  ;; Ditto for earlier ENABLE-PACKAGE-LOCKS %%C-S%% %%C-V%%
                  ;; declaration.
                  (if #f (pp `(instruction defun-name)))
                  #,@emitter)))
             (values)))

         
         (%define-instruction 'name #'defun-name)

	 #|
         #,@(extract-nths 1 #'begin pdefs)
         #,@(if (pair? pdefs)
		#`((install-inst-flavors
		    'name
		    (append #,@(extract-nths 0 #'list pdefs))))
		'())
	 |#)))))))))


(define (%define-instruction name defun)
  (hash-set! *assem-instructions* name defun)
  name)

