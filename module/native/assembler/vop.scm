(define-module (native assembler vop)
  #:use-module (native assembler assem)
  #:use-module (native assembler sbcl)
  #:use-module (native assembler params)
  #:use-module (native assembler early-vm)
  #:use-module (rnrs records syntactic)
  #:export (sb-name sc-sb sc-name make-tn make-random-tn
                    tn-sc tn-offset tn tn? 
		    make-sc make-finite-sb make-sb
                    ir2-component-constants
		    finite-sb-always-live-set!
		    finite-sb-conflicts-set!
		    finite-sb-live-tns-set!
		    finite-sb-always-live-count-set!
		    sc-load-costs sc-sb-set!		    
		    ))

#|PORT
defprinter see code/early-extensions.lisp
mk-cl-protocol
|#

;;;; structures for the second (virtual machine) intermediate
;;;; representation in the compiler, IR2

;;;; This software is part of the SBCL system. See the README file for
;;;; more information.
;;;;
;;;; This software is derived from the CMU CL system, which was
;;;; written at Carnegie Mellon University and released into the
;;;; public domain. The software is in the public domain and is
;;;; provided with absolutely no warranty. See the COPYING and CREDITS
;;;; files for more information.

;;; the largest number of TNs whose liveness changes that we can have
;;; in any block
(define local-tn-limit 64)

;;;; PRIMITIVE-TYPEs

;;; A PRIMITIVE-TYPE is used to represent the aspects of type
;;; interesting to the VM. Selection of IR2 translation templates is
;;; done on the basis of the primitive types of the operands, and the
;;; primitive type of a value is used to constrain the possible
;;; representations of that value.
(define-record-type primitive-type
  (protocol
   (mk-cl-protocol
    (name       #f) 
    (scs       '())
    (specifier  (missing-arg)) 
    (check      #f)))
               
  (fields
   ;; the name of this PRIMITIVE-TYPE
   (mutable name)

   ;; a list of the SC numbers for all the SCs that a TN of this type
   ;; can be allocated in
   (mutable scs)

   ;; the Lisp type equivalent to this type. If this type could never be
   ;; returned by PRIMITIVE-TYPE, then this is the NIL (or empty) type
   (mutable specifier)

   ;; the template used to check that an object is of this type. This is a
   ;; template of one argument and one result, both of primitive-type T. If
   ;; the argument is of the correct type, then it is delivered into the
   ;; result. If the type is incorrect, then an error is signalled.
   (mutable check)))
  
(defprinter (primitive-type)
  name)

;;;; IR1 annotations used for IR2 conversion

;;; BLOCK-INFO
;;;    Holds the IR2-BLOCK structure. If there are overflow blocks,
;;;    then this points to the first IR2-BLOCK. The BLOCK-INFO of the
;;;    dummy component head and tail are dummy IR2 blocks that begin
;;;    and end the emission order thread.
;;;
;;; COMPONENT-INFO
;;;    Holds the IR2-COMPONENT structure.
;;;
;;; LVAR-INFO
;;;    Holds the IR2-LVAR structure. LVARs whose values aren't used
;;;    won't have any. XXX
;;;
;;; CLEANUP-INFO
;;;    If non-null, then a TN in which the affected dynamic
;;;    environment pointer should be saved after the binding is
;;;    instantiated.
;;;
;;; PHYSENV-INFO
;;;    Holds the IR2-PHYSENV structure.
;;;
;;; TAIL-SET-INFO
;;;    Holds the RETURN-INFO structure.
;;;
;;; NLX-INFO-INFO
;;;    Holds the IR2-NLX-INFO structure.
;;;
;;; LEAF-INFO
;;;    If a non-set lexical variable, the TN that holds the value in
;;;    the home environment. If a constant, then the corresponding
;;;    constant TN. If an XEP lambda, then the corresponding
;;;    Entry-Info structure.
;;;
;;; BASIC-COMBINATION-INFO
;;;    The template chosen by LTN, or
;;;     :FULL if this is definitely a full call.
;;;     :FUNNY if this is an oddball thing with IR2-convert.
;;;     :LOCAL if this is a local call.
;;;
;;; NODE-TAIL-P
;;;    After LTN analysis, this is true only in combination nodes that are
;;;    truly tail recursive.

;;; An IR2-BLOCK holds information about a block that is used during
;;; and after IR2 conversion. It is stored in the BLOCK-INFO slot for
;;; the associated block.
(define-record-type ir2-block
  (parent block-annotation)
  (protocol
   (lambda (n)
     (lambda (block)
       (let ((p (n #:block block)))
         (p #f '() '() '() '() #f #f 0
            (make-vector local-tn-limit)
            (make-bitvector local-tn-limit #f)
            (make-bitvector local-tn-limit)
            (make-bitvector local-tn-limit #f)
            #f #f #f #f '())))))

  (fields
   ;; the IR2-BLOCK's number, which differs from BLOCK's BLOCK-NUMBER
   ;; if any blocks are split. This is assigned by lifetime analysis.
   (mutable number)

   ;; information about unknown-values LVARs that is used by stack
   ;; analysis to do stack simulation. An UNKNOWN-VALUES LVAR is PUSHED
   ;; if its DEST is in another block. Similarly, a LVAR is POPPED if
   ;; its DEST is in this block but has its uses elsewhere. The LVARs
   ;; are in the order that are pushed/popped in the block. Note that
   ;; the args to a single MV-COMBINATION appear reversed in POPPED,
   ;; since we must effectively pop the last argument first. All pops
   ;; must come before all pushes (although internal MV uses may be
   ;; interleaved.) POPPED is computed by LTN, and PUSHED is computed
   ;; by stack analysis.
   (mutable pushed)
   (mutable popped)

   ;; the result of stack analysis: lists of all the unknown-values
   ;; LVARs on the stack at the block start and end, topmost LVAR
   ;; first.
   (mutable start-stack)
   (mutable end-stack)

   ;; the first and last VOP in this block. If there are none, both
   ;; slots are null.
   (mutable start-vop)
   (mutable last-vop)

   ;; the number of local TNs actually allocated
   (mutable local-tn-count)

   ;; a vector that maps local TN numbers to TNs. Some entries may be
   ;; NIL, indicating that that number is unused. (This allows us to
   ;; delete local conflict information without compressing the LTN
   ;; numbers.)
   ;;
   ;; If an entry is :MORE, then this block contains only a single VOP.
   ;; This VOP has so many more arguments and/or results that they
   ;; cannot all be assigned distinct LTN numbers. In this case, we
   ;; assign all the more args one LTN number, and all the more results
   ;; another LTN number. We can do this, since more operands are
   ;; referenced simultaneously as far as conflict analysis is
   ;; concerned. Note that all these :MORE TNs will be global TNs.

   (mutable local-tns)

   ;; Bit-vectors used during lifetime analysis to keep track of
   ;; references to local TNs. When indexed by the LTN number, the
   ;; index for a TN is non-zero in WRITTEN if it is ever written in
   ;; the block, and in LIVE-OUT if the first reference is a read.
   (mutable written) 
   (mutable live-out)

   ;; This is similar to the above, but is updated by lifetime flow
   ;; analysis to have a 1 for LTN numbers of TNs live at the end of
   ;; the block. This takes into account all TNs that aren't :LIVE.
   (mutable live-in)

   ;; a thread running through the global-conflicts structures for this
   ;; block, sorted by TN number
   (mutable global-tns)
   
   ;; the assembler label that points to the beginning of the code for
   ;; this block, or NIL when we haven't assigned a label yet
   (mutable %label)

   ;; the assembler label that points to the trampoline for this block,
   ;; or NIL if unassigned yet. Only meaningful for local call targets.
   (mutable %trampoline-label)

   ;; T if the preceding block assumes it can drop thru to %label
   (mutable dropped-thru-to)

   ;; list of LOCATION-INFO structures describing all the interesting
   ;; (to the debugger) locations in this block
   (mutable locations)))

(defprinter (ir2-block)
  (pushed         :test pushed)
  (popped         :test popped)
  (start-vop      :test start-vop)
  (last-vop       :test last-vop)
  (local-tn-count :test (lambda (o)
			  (not (zero? (ir2-block-local-tn-count o)))))
  (%label         :test %label))

;;; An IR2-LVAR structure is used to annotate LVARs that are used as a
;;; function result LVARs or that receive MVs.
(define-record-type ir2-lvar
  (protocol
   (lambda (n)
     (lambda (primitive-type)
       (n #:fixed primitive-type '() #f))))

  (fields
   ;; If this is :DELAYED, then this is a single value LVAR for which
   ;; the evaluation of the use is to be postponed until the evaluation
   ;; of destination. This can be done for ref nodes or predicates
   ;; whose destination is an IF.
   ;;
   ;; If this is :FIXED, then this LVAR has a fixed number of values,
   ;; with the TNs in LOCS.
   ;;
   ;; If this is :UNKNOWN, then this is an unknown-values LVAR, using
   ;; the passing locations in LOCS.
   ;;
   ;; If this is :UNUSED, then this LVAR should never actually be used
   ;; as the destination of a value: it is only used tail-recursively.
   (mutable kind)
  
   ;; The primitive-type of the first value of this LVAR. This is
   ;; primarily for internal use during LTN, but it also records the
   ;; type restriction on delayed references. In multiple-value
   ;; contexts, this is null to indicate that it is meaningless. This
   ;; is always (primitive-type (lvar-type cont)), which may be more
   ;; restrictive than the tn-primitive-type of the value TN. This is
   ;; becase the value TN must hold any possible type that could be
   ;; computed (before type checking.) XXX
   (mutable primitive-type)

   ;; Locations used to hold the values of the LVAR. If the number of
   ;; values if fixed, then there is one TN per value. If the number of
   ;; values is unknown, then this is a two-list of TNs holding the
   ;; start of the values glob and the number of values. Note that
   ;; since type checking is the responsibility of the values receiver,
   ;; these TNs primitive type is only based on the proven type
   ;; information.
   (mutable locs)
   (mutable stack-pointer)))

(defprinter (ir2-lvar)
  kind
  primitive-type
  locs)

;;; An IR2-COMPONENT serves mostly to accumulate non-code information
;;; about the component being compiled.
(define-record-type ir2-component
  (protocol
   (mk-cl-protocol
    (global-tn-counter 0)
    (normal-tns     #f)
    (restricted-tns #f)
    (wired-tns      #f)
    (constant-tns   #f)
    (component-tns '())
    (nfp            #f)
    (specified-save-tns '())
    (values-receivers   '())
    (constants (make-vector 100))
    (format         #f)
    (entries        '())
    (alias-tns      #f)
    (spilled-vops   (make-hash-table))
    (spilled-tns    (make-hash-table))
    (dyncount-info  #f)))
  (fields
   ;; the counter used to allocate global TN numbers
   (mutable global-tn-counter)

   ;; NORMAL-TNS is the head of the list of all the normal TNs that
   ;; need to be packed, linked through the Next slot. We place TNs on
   ;; this list when we allocate them so that Pack can find them.
   ;;
   ;; RESTRICTED-TNS are TNs that must be packed within a finite SC. We
   ;; pack these TNs first to ensure that the restrictions will be
   ;; satisfied (if possible).
   ;;
   ;; WIRED-TNs are TNs that must be packed at a specific location. The
   ;; SC and OFFSET are already filled in.
   ;;
   ;; CONSTANT-TNs are non-packed TNs that represent constants.
   (mutable normal-tns)
   (mutable restricted-tns)
   (mutable wired-tns)
   (mutable constant-tns)
   
   ;; a list of all the :COMPONENT TNs (live throughout the component).
   ;; These TNs will also appear in the {NORMAL,RESTRICTED,WIRED} TNs
   ;; as appropriate to their location.
   (mutable component-tns)
   
   ;; If this component has a NFP, then this is it.
   (mutable nfp)
   
   ;; a list of the explicitly specified save TNs (kind
   ;; :SPECIFIED-SAVE). These TNs will also appear in the
   ;; {NORMAL,RESTRICTED,WIRED} TNs as appropriate to their location.
   (mutable specified-save-tns)
   
   ;; a list of all the blocks whose IR2-BLOCK has a non-null value for
   ;; POPPED. This slot is initialized by LTN-ANALYZE as an input to
   ;; STACK-ANALYZE.
   (mutable values-receivers)

   ;; an adjustable vector that records all the constants in the
   ;; constant pool. A non-immediate :CONSTANT TN with offset 0 refers
   ;; to the constant in element 0, etc. Normal constants are
   ;; represented by the placing the CONSTANT leaf in this vector. A
   ;; load-time constant is distinguished by being a cons (KIND .
   ;; WHAT). KIND is a keyword indicating how the constant is computed,
   ;; and WHAT is some context.
   ;;
   ;; These load-time constants are recognized:
   ;;
   ;; (:entry . <function>)
   ;;    Is replaced by the code pointer for the specified function.
   ;;    This is how compiled code (including DEFUN) gets its hands on
   ;;    a function. <function> is the XEP lambda for the called
   ;;    function; its LEAF-INFO should be an ENTRY-INFO structure.
   ;;
   ;; (:label . <label>)
   ;;    Is replaced with the byte offset of that label from the start
   ;;    of the code vector (including the header length.)
   ;;
   ;; A null entry in this vector is a placeholder for implementation
   ;; overhead that is eventually stuffed in somehow.
   (mutable constants)

   ;; some kind of info about the component's run-time representation.
   ;; This is filled in by the VM supplied SELECT-COMPONENT-FORMAT function.
   (mutable format)

   ;; a list of the ENTRY-INFO structures describing all of the entries
   ;; into this component. Filled in by entry analysis.
   (mutable entries)

   ;; head of the list of :ALIAS TNs in this component, threaded by TN-NEXT
   (mutable alias-tns)

   ;; SPILLED-VOPS is a hashtable translating from "interesting" VOPs
   ;; to a list of the TNs spilled at that VOP. This is used when
   ;; computing debug info so that we don't consider the TN's value to
   ;; be valid when it is in fact somewhere else. SPILLED-TNS has T for
   ;; every "interesting" TN that is ever spilled, providing a
   ;; representation that is more convenient some places.
   (mutable spilled-vops)
   (mutable spilled-tns)

   ;; dynamic vop count info. This is needed by both ir2-convert and
   ;; setup-dynamic-count-info. (But only if we are generating code to
   ;; collect dynamic statistics.)
   (mutable dyncount-info)))


;;; An ENTRY-INFO condenses all the information that the dumper needs
;;; to create each XEP's function entry data structure. ENTRY-INFO
;;; structures are sometimes created before they are initialized,
;;; since IR2 conversion may need to compile a forward reference. In
;;; this case the slots aren't actually initialized until entry
;;; analysis runs.
(define-record-type entry-info
  (protocol
   (mk-cl-protocol
    (closure-tn #f)
    (offset     #f)
    (name       "<not computed>")
    (arguments  '())
    (type       'function)
    (info       #f)))
  
  (fields
   ;; TN, containing closure (if needed) for this function in the home
   ;; environment.
   (mutable closure-tn)

   ;; a label pointing to the entry vector for this function, or NIL
   ;; before ENTRY-ANALYZE runs
   (mutable offset)

   ;; If this function was defined using DEFUN, then this is the name
   ;; of the function, a symbol or (SETF <symbol>). Otherwise, this is
   ;; some string that is intended to be informative.
   (mutable name)

   ;; the argument list that the function was defined with.
   (mutable arguments)

   ;; a function type specifier representing the arguments and results
   ;; of this function
   (mutable type)

   ;; docstring and/or xref information for the XEP
   (mutable info)))

;;; An IR2-PHYSENV is used to annotate non-LET LAMBDAs with their
;;; passing locations. It is stored in the PHYSENV-INFO.

(define-record-type ir2-physenv
  (protocol
   (mk-cl-protocol
    (closure           (missing-arg))
    (old-fp            #f)
    (return-pc         #f)
    (return-pc-pass    (missing-arg))
    (number-stack-p    #f)
    (live-tns         '())
    (debug-live-tns   '())
    (elsewhere-start   #f) 
    (environment-start #f)))

  (fields
   ;; TN info for closed-over things within the function: an alist
   ;; mapping from NLX-INFOs and LAMBDA-VARs to TNs holding the
   ;; corresponding thing within this function
   ;;
   ;; Elements of this list have a one-to-one correspondence with
   ;; elements of the PHYSENV-CLOSURE list of the PHYSENV object that
   ;; links to us.
   (mutable closure)

   ;; the TNs that hold the OLD-FP and RETURN-PC within the function.
   ;; We always save these so that the debugger can do a backtrace,
   ;; even if the function has no return (and thus never uses them).
   ;; Null only temporarily.
   (mutable old-fp)
   (mutable return-pc)

   ;; The passing location for the RETURN-PC. The return PC is treated
   ;; differently from the other arguments, since in some
   ;; implementations we may use a call instruction that requires the
   ;; return PC to be passed in a particular place.
   (mutable return-pc-pass)

   ;; True if this function has a frame on the number stack. This is
   ;; set by representation selection whenever it is possible that some
   ;; function in our tail set will make use of the number stack.
   (mutable number-stack-p)

   ;; a list of all the :ENVIRONMENT TNs live in this environment
   (mutable live-tns)

   ;; a list of all the :DEBUG-ENVIRONMENT TNs live in this environment
   (mutable debug-live-tns)

   ;; a label that marks the start of elsewhere code for this function,
   ;; or null until this label is assigned by codegen. Used for
   ;; maintaining the debug source map.
   (mutable elsewhere-start)

   ;; a label that marks the first location in this function at which
   ;; the environment is properly initialized, i.e. arguments moved
   ;; from their passing locations, etc. This is the start of the
   ;; function as far as the debugger is concerned.
  (mutable environment-start)))

(defprinter (ir2-physenv)
  closure
  old-fp
  return-pc
  return-pc-pass)

;;; A RETURN-INFO is used by GTN to represent the return strategy and
;;; locations for all the functions in a given TAIL-SET. It is stored
;;; in the TAIL-SET-INFO.
(define-record-type return-info
  (protocol
   (mk-cl-protocol
    (kind       (missing-arg))
    (count      (missing-arg))
    (types     '())
    (locations '())))

  (fields
   ;; The return convention used:
   ;; -- If :UNKNOWN, we use the standard return convention.
   ;; -- If :FIXED, we use the known-values convention.
   (mutable kind)

   ;; the number of values returned, or :UNKNOWN if we don't know.
   ;; COUNT may be known when KIND is :UNKNOWN, since we may choose the
   ;; standard return convention for other reasons.
   (mutable count)

   ;; If count isn't :UNKNOWN, then this is a list of the
   ;; primitive-types of each value.
   (mutable types)

   ;; If kind is :FIXED, then this is the list of the TNs that we
   ;; return the values in.
   (mutable locations)))

(defprinter (return-info)
  kind
  count
  types
  locations)

(define-record-type ir2-nlx-info
  (protocol
   (mk-cl-protocol
    (home    #f)
    (save-sp (missing-arg))
    (dynamic-state (list* (make-stack-pointer-tn)
                          (make-dynamic-state-tns)))
    (target  (gen-label))))


  (fields
   ;; If the kind is :ENTRY (a lexical exit), then in the home
   ;; environment, this holds a VALUE-CELL object containing the unwind
   ;; block pointer. In the other cases nobody directly references the
   ;; unwind-block, so we leave this slot null.
   (mutable home)

   ;; the saved control stack pointer
   (mutable save-sp)

   ;; the list of dynamic state save TNs
   (mutable dynamic-state)
   
   ;; the target label for NLX entry
   (mutable target)))

(defprinter (ir2-nlx-info)
  home
  save-sp
  dynamic-state)

(define-record-type loop 
  (protocol
   (mk-cl-protocol
    (kind      (missing-arg))
    (head      #f)
    (tail      '())
    (exits     '())
    (superior  #f)
    (inferiors '())
    (depth      0)
    (blocks    #f)
    (info      #f)))

  (fields
   ;; The kind of loop that this is.  These values are legal:
   ;;
   ;;    :OUTER
   ;;        This is the outermost loop structure, and represents all the
   ;;        code in a component.
   ;;
   ;;    :NATURAL
   ;;        A normal loop with only one entry.
   ;;
   ;;    :STRANGE
   ;;        A segment of a "strange loop" in a non-reducible flow graph.
   (mutable kind)

   ;; The first and last blocks in the loop.  There may be more than one tail,
   ;; since there may be multiple back branches to the same head.
   (mutable head)
   (mutable tail)

   ;; A list of all the blocks in this loop or its inferiors that have a
   ;; successor outside of the loop.
   (mutable exits)

   ;; The loop that this loop is nested within.  This is null in the outermost
   ;; loop structure.
   (mutable superior)

   ;; A list of the loops nested directly within this one.
   (mutable inferiors)
   (mutable depth)

   ;; The head of the list of blocks directly within this loop.  We must recurse
   ;; on INFERIORS to find all the blocks.
   (mutable blocks)

   ;; Backend saves the first emitted block of each loop here.
   (mutable info)))

(defprinter (loop)
  kind
  head
  tail
  exits
  depth)

;;;; VOPs and templates

;;; A VOP is a Virtual Operation. It represents an operation and the
;;; operands to the operation.
(define-record-type vop
  (protocol
   (mk-cl-protocol
    #:subset (block node info args results)
    (info     #f)
    (block    (missing-arg))
    (next     #f)
    (prev     #f)
    (args     #f)
    (results  #f)
    (temps    #f)
    (refs     #f)
    (codegen-info #f)
    (node     #f)
    (save-set #f)))
  
  (fields
   ;; VOP-INFO structure containing static info about the operation
   (mutable info)

   ;; the IR2-BLOCK this VOP is in
   (mutable block)

   ;; VOPs evaluated after and before this one. Null at the
   ;; beginning/end of the block, and temporarily during IR2
   ;; translation.
   (mutable next)
   (mutable prev)

   ;; heads of the TN-REF lists for operand TNs, linked using the
   ;; ACROSS slot
   (mutable args)
   (mutable results)
  
   ;; head of the list of write refs for each explicitly allocated
   ;; temporary, linked together using the ACROSS slot
   (mutable temps)
   
   ;; head of the list of all TN-REFs for references in this VOP,
   ;; linked by the NEXT-REF slot. There will be one entry for each
   ;; operand and two (a read and a write) for each temporary.
   (mutable refs)
   
   ;; stuff that is passed uninterpreted from IR2 conversion to
   ;; codegen. The meaning of this slot is totally dependent on the VOP.
   (mutable codegen-info)
   (mutable node)

   ;; LOCAL-TN-BIT-VECTOR representing the set of TNs live after args
   ;; are read and before results are written. This is only filled in
   ;; when VOP-INFO-SAVE-P is non-null.
   (mutable save-set)))

(defprinter (vop)
  info
  args
  results
  (codegen-info :test codegen-info))

;;; A TN-REF object contains information about a particular reference
;;; to a TN. The information in TN-REFs largely determines how TNs are
;;; packed.
(define-record-type tn-ref 
  (protocol
   (mk-cl-protocol
    #:subset (tn write?)
    (tn       (missing-arg))
    (write?   #f)
    (next     #f)
    (vop      #f)
    (next-ref #f)
    (across   #f)
    (target   #f)
    (load-tn  #f)))

  (fields
   ;; the TN referenced
   (mutable tn)
 
   ;; Is this is a write reference? (as opposed to a read reference)
   (mutable write?)

   ;; the link for a list running through all TN-REFs for this TN of
   ;; the same kind (read or write)
   (mutable next)

   ;; the VOP where the reference happens, or NIL temporarily
   (mutable vop)

   ;; the link for a list of all TN-REFs in VOP, in reverse order of
   ;; reference
   (mutable next-ref)

   ;; the link for a list of the TN-REFs in VOP of the same kind
   ;; (argument, result, temp)
   (mutable across)

   ;; If true, this is a TN-REF also in VOP whose TN we would like
   ;; packed in the same location as our TN. Read and write refs are
   ;; always paired: TARGET in the read points to the write, and
   ;; vice-versa.
   (mutable target)

   ;; the load TN allocated for this operand, if any
   (mutable load-tn)))

(defprinter (tn-ref)
  tn
  write?
  (vop :test vop))

;;; A TEMPLATE object represents a particular IR2 coding strategy for
;;; a known function.
(define-record-type template 
  (protocol
   (mk-cl-protocol
    (name               #f)
    (type               (missing-arg))
    (arg-types         '())
    (result-types      '())
    (more-args-type    '())
    (more-results-type '())
    (guard              #f)
    (ltn-policy        (missing-arg))
    (cost              (missing-arg))
    (note               #f)
    (info-arg-count     0)
    (emit-function     (missing-arg))))
  
  (fields
   ;; the symbol name of this VOP. This is used when printing the VOP
   ;; and is also used to provide a handle for definition and
   ;; translation.
   (mutable name)

   ;; the arg/result type restrictions. We compute this from the
   ;; PRIMITIVE-TYPE restrictions to make life easier for IR1 phases
   ;; that need to anticipate LTN's template selection.
   (mutable type)

   ;; lists of restrictions on the argument and result types. A
   ;; restriction may take several forms:
   ;; -- The restriction * is no restriction at all.
   ;; -- A restriction (:OR <primitive-type>*) means that the operand
   ;;    must have one of the specified primitive types.
   ;; -- A restriction (:CONSTANT <predicate> <type-spec>) means that the
   ;;    argument (not a result) must be a compile-time constant that
   ;;    satisfies the specified predicate function. In this case, the
   ;;    constant value will be passed as an info argument rather than
   ;;    as a normal argument. <type-spec> is a Lisp type specifier for
   ;;    the type tested by the predicate, used when we want to represent
   ;;    the type constraint as a Lisp function type.
   ;;
   ;; If RESULT-TYPES is :CONDITIONAL, then this is an IF-FOO style
   ;; conditional that yields its result as a control transfer. The
   ;; emit function takes two info arguments: the target label and a
   ;; boolean flag indicating whether to negate the sense of the test.
   ;;
   ;; If RESULT-TYPES is a cons whose car is :CONDITIONAL, then this is
   ;; a flag-setting VOP. The rest is a list of condition descriptors to
   ;; be interpreted by the BRANCH-IF VOP (see $ARCH/pred.lisp).
   (mutable arg-types)
   (mutable result-types)

   ;; the primitive type restriction applied to each extra argument or
   ;; result following the fixed operands. If NIL, no extra
   ;; args/results are allowed. Otherwise, either * or a (:OR ...) list
   ;; as described for the {ARG,RESULT}-TYPES.  
   (mutable more-args-type)
   (mutable more-results-type)

   ;; If true, this is a function that is called with no arguments to
   ;; see whether this template can be emitted. This is used to
   ;; conditionally compile for different target hardware
   ;; configuarations (e.g. FP hardware.)
   (mutable guard)

   ;; the policy under which this template is the best translation.
   ;; Note that LTN might use this template under other policies if it
   ;; can't figure out anything better to do.
   (mutable ltn-policy)

   ;; the base cost for this template, given optimistic assumptions
   ;; such as no operand loading, etc.
   (mutable cost)

   ;; If true, then this is a short noun-like phrase describing what
   ;; this VOP "does", i.e. the implementation strategy. This is for
   ;; use in efficiency notes.
   (mutable note)

   ;; the number of trailing arguments to VOP or %PRIMITIVE that we
   ;; bundle into a list and pass into the emit function. This provides
   ;; a way to pass uninterpreted stuff directly to the code generator.
   (mutable info-arg-count)

   ;; a function that emits the VOPs for this template. Arguments:
   ;;  1] Node for source context.
   ;;  2] IR2-BLOCK that we place the VOP in.
   ;;  3] This structure.
   ;;  4] Head of argument TN-REF list.
   ;;  5] Head of result TN-REF list.
   ;;  6] If INFO-ARG-COUNT is non-zero, then a list of the magic
   ;;     arguments.
   ;;
   ;; Two values are returned: the first and last VOP emitted. This vop
   ;; sequence must be linked into the VOP Next/Prev chain for the
   ;; block. At least one VOP is always emitted.
   (mutable emit-function)))

(defprinter (template)
  name
  arg-types
  result-types
  (more-args-type    :test more-args-type   )
  (more-results-type :test more-results-type)
  ltn-policy
  cost
  (note              :test note)
  (info-arg-count    :test (lambda (o) 
			     (not (zero? (template-info-arg-count o))))))

;;; A VOP-INFO object holds the constant information for a given
;;; virtual operation. We include TEMPLATE so that functions with a
;;; direct VOP equivalent can be translated easily.
(define-record-type vop-info
  (parent template)
  (protocol
   (mk-cl-protocol
    #:parent ()
    (effects            (missing-arg))
    (affected           (missing-arg))
    (save-p             #f)
    (move-args          #f)
    (arg-costs         '())
    (result-costs      '())
    (more-arg-costs     #f)
    (more-result-costs  #f)
    (arg-load-scs      '())
    (result-load-scs   '())
    (target-fun         #f)
    (generator-function #f)
    (variant           '())
    (num-args           0)
    (num-results        0)
    (temps              #f)
    (ref-ordering       #f)
    (targets            #f)))

  (fields
   ;; side effects of this VOP and side effects that affect the value
   ;; of this VOP
   (mutable effects)
   (mutable affected)

   ;; If true, causes special casing of TNs live after this VOP that
   ;; aren't results:
   ;; -- If T, all such TNs that are allocated in a SC with a defined
   ;;    save-sc will be saved in a TN in the save SC before the VOP
   ;;    and restored after the VOP. This is used by call VOPs. A bit
   ;;    vector representing the live TNs is stored in the VOP-SAVE-SET.
   ;; -- If :FORCE-TO-STACK, all such TNs will made into :ENVIRONMENT TNs
   ;;    and forced to be allocated in SCs without any save-sc. This is
   ;;    used by NLX entry vops.
   ;; -- If :COMPUTE-ONLY, just compute the save set, don't do any saving.
   ;;    This is used to get the live variables for debug info.
   (mutable save-p)

   ;; info for automatic emission of move-arg VOPs by representation
   ;; selection. If NIL, then do nothing special. If non-null, then
   ;; there must be a more arg. Each more arg is moved to its passing
   ;; location using the appropriate representation-specific MOVE-ARG
   ;; VOP. The first (fixed) argument must be the control-stack frame
   ;; pointer for the frame to move into. The first info arg is the
   ;; list of passing locations.
   ;;
   ;; Additional constraints depend on the value:
   ;;
   ;; :FULL-CALL
   ;;     None.
   ;;
   ;; :LOCAL-CALL
   ;;     The second (fixed) arg is the NFP for the called function (from
   ;;     ALLOCATE-FRAME.)
   ;;
   ;; :KNOWN-RETURN
   ;;     If needed, the old NFP is computed using COMPUTE-OLD-NFP.
   (mutable move-args)

   ;; a list of sc-vectors representing the loading costs of each fixed
   ;; argument and result
   (mutable arg-costs)
   (mutable result-costs)

   ;; if true, SC-VECTORs representing the loading costs for any more
   ;; args and results
   (mutable more-arg-costs)
   (mutable more-result-costs)

   ;; lists of SC-VECTORs mapping each SC to the SCs that we can load
   ;; into. If a SC is directly acceptable to the VOP, then the entry
   ;; is T. Otherwise, it is a list of the SC numbers of all the SCs
   ;; that we can load into. This list will be empty if there is no
   ;; load function which loads from that SC to an SC allowed by the
   ;; operand SC restriction.
   (mutable arg-load-scs)
   (mutable result-load-scs)

   ;; if true, a function that is called with the VOP to do operand
   ;; targeting. This is done by modifying the TN-REF-TARGET slots in
   ;; the TN-REFS so that they point to other TN-REFS in the same VOP.
   (mutable target-fun)

   ;; a function that emits assembly code for a use of this VOP when it
   ;; is called with the VOP structure. This is null if this VOP has no
   ;; specified generator (i.e. if it exists only to be inherited by
   ;; other VOPs).
   (mutable generator-function)

   ;; a list of things that are used to parameterize an inherited
   ;; generator. This allows the same generator function to be used for
   ;; a group of VOPs with similar implementations.
   (mutable variant)

   ;; the number of arguments and results. Each regular arg/result
   ;; counts as one, and all the more args/results together count as 1.
   (mutable num-args)
   (mutable num-results)

   ;; a vector of the temporaries the vop needs. See EMIT-GENERIC-VOP
   ;; in vmdef for information on how the temps are encoded.
   (mutable temps)

   ;; the order all the refs for this vop should be put in. Each
   ;; operand is assigned a number in the following ordering: args,
   ;; more-args, results, more-results, temps. This vector represents
   ;; the order the operands should be put into in the next-ref link.
   (mutable ref-ordering)

   ;; a vector of the various targets that should be done. Each element
   ;; encodes the source ref (shifted 8, it is also encoded in
   ;; MAX-VOP-TN-REFS) and the dest ref index.
   (mutable targets)))


;;;; SBs and SCs

;;; copied from docs/internals/retargeting.tex by WHN 19990707:
;;;
;;; A Storage Base represents a physical storage resource such as a
;;; register set or stack frame. Storage bases for non-global
;;; resources such as the stack are relativized by the environment
;;; that the TN is allocated in. Packing conflict information is kept
;;; in the storage base, but non-packed storage resources such as
;;; closure environments also have storage bases.
;;;
;;; Some storage bases:
;;;     General purpose registers
;;;     Floating point registers
;;;     Boxed (control) stack environment
;;;     Unboxed (number) stack environment
;;;     Closure environment
;;;
;;; A storage class is a potentially arbitrary set of the elements in
;;; a storage base. Although conceptually there may be a hierarchy of
;;; storage classes such as "all registers", "boxed registers", "boxed
;;; scratch registers", this doesn't exist at the implementation
;;; level. Such things can be done by specifying storage classes whose
;;; locations overlap. A TN shouldn't have lots of overlapping SC's as
;;; legal SC's, since time would be wasted repeatedly attempting to
;;; pack in the same locations.
;;;
;;; ...
;;;
;;; Some SCs:
;;;     Reg: any register (immediate objects)
;;;     Save-Reg: a boxed register near r15 (registers easily saved in a call)
;;;     Boxed-Reg: any boxed register (any boxed object)
;;;     Unboxed-Reg: any unboxed register (any unboxed object)
;;;     Float-Reg, Double-Float-Reg: float in FP register.
;;;     Stack: boxed object on the stack (on control stack)
;;;     Word: any 32bit unboxed object on nstack.
;;;     Double: any 64bit unboxed object on nstack.

;;; The SB structure represents the global information associated with
;;; a storage base.
(define-record-type sb 
  (protocol
   (mk-cl-protocol
    (name #f)
    (kind #:non-packed)
    (size 0)))

  (fields
   ;; name, for printing and reference
   (mutable name)

   ;; the kind of storage base (which determines the packing
   ;; algorithm)
   (mutable kind)

   ;; the number of elements in the SB. If finite, this is the total
   ;; size. If unbounded, this is the size that the SB is initially
   ;; allocated at.
   (mutable size)))

(defprinter (sb)
  name)

;;; A FINITE-SB holds information needed by the packing algorithm for
;;; finite SBs.
(define-record-type finite-sb 
  (parent sb)
  (protocol    
   (mk-cl-protocol
    #:parent (name kind size)
    (current-size         0)
    (last-offset          0)
    (conflicts          #())
    (always-live        #())
    (always-live-count  #())
    (live-tns           #())
    (last-block-count     0)))

  (fields   
   ;; the number of locations currently allocated in this SB
   (mutable current-size)

   ;; the last location packed in, used by pack to scatter TNs to
   ;; prevent a few locations from getting all the TNs, and thus
   ;; getting overcrowded, reducing the possibilities for targeting.
   (mutable last-offset)

   ;; a vector containing, for each location in this SB, a vector
   ;; indexed by IR2 block numbers, holding local conflict bit vectors.
   ;; A TN must not be packed in a given location within a particular
   ;; block if the LTN number for that TN in that block corresponds to
   ;; a set bit in the bit-vector.
   (mutable conflicts)

   ;; a vector containing, for each location in this SB, a bit-vector
   ;; indexed by IR2 block numbers. If the bit corresponding to a block
   ;; is set, then the location is in use somewhere in the block, and
   ;; thus has a conflict for always-live TNs.
   (mutable always-live)
   (mutable always-live-count)

   ;; a vector containing the TN currently live in each location in the
   ;; SB, or NIL if the location is unused. This is used during load-tn pack.
   (mutable live-tns)

   ;; the number of blocks for which the ALWAYS-LIVE and CONFLICTS
   ;; might not be virgin, and thus must be reinitialized when PACK
   ;; starts. Less then the length of those vectors when not all of the
   ;; length was used on the previously packed component.
   (mutable last-block-count)))


;;; the SC structure holds the storage base that storage is allocated
;;; in and information used to select locations within the SB
(define-record-type sc
  (protocol    
   (mk-cl-protocol  
    (name          #f)
    (number        0)
    (sb            #f)
    (element-size  0)
    (locations     '())
    (alternate-scs '())
    (constant-scs  '())
    (save?         #f)
    (move-funs     (make-vector sc-number-limit #f))
    (load-costs    (make-vector sc-number-limit #f))
    (move-vops     (make-vector sc-number-limit #f))
    (move-costs    (make-vector sc-number-limit #f))
    (move-arg-vops (make-vector sc-number-limit #f))
    (number-stack? #f)
    (alignment     1)
    (reserve-locations '())))

  (fields
   ;; name, for printing and reference
   (mutable name)

   ;; the number used to index SC cost vectors
   (mutable number)

   ;; the storage base that this SC allocates storage from
   (mutable sb)

   ;; the size of elements in this SC, in units of locations in the SB
   (mutable element-size)

   ;; if our SB is finite, a list of the locations in this SC
   (mutable locations)

   ;; a list of the alternate (save) SCs for this SC
   (mutable alternate-scs)

   ;; a list of the constant SCs that can me moved into this SC
   (mutable constant-scs)

   ;; true if the values in this SC needs to be saved across calls
   (mutable save?)

   ;; vectors mapping from SC numbers to information about how to load
   ;; from the index SC to this one. MOVE-FUNS holds the names of
   ;; the functions used to do loading, and LOAD-COSTS holds the cost
   ;; of the corresponding move functions. If loading is impossible,
   ;; then the entries are NIL. LOAD-COSTS is initialized to have a 0
   ;; for this SC.
   (mutable move-funs)
   (mutable load-costs)

   ;; a vector mapping from SC numbers to possibly
   ;; representation-specific move and coerce VOPs. Each entry is a
   ;; list of VOP-INFOs for VOPs that move/coerce an object in the
   ;; index SC's representation into this SC's representation. This
   ;; vector is filled out with entries for all SCs that can somehow be
   ;; coerced into this SC, not just those VOPs defined to directly
   ;; move into this SC (i.e. it allows for operand loading on the move
   ;; VOP's operands.)
   ;;
   ;; When there are multiple applicable VOPs, the template arg and
   ;; result type restrictions are used to determine which one to use.
   ;; The list is sorted by increasing cost, so the first applicable
   ;; VOP should be used.
   ;;
   ;; Move (or move-arg) VOPs with descriptor results shouldn't have
   ;; TNs wired in the standard argument registers, since there may
   ;; already be live TNs wired in those locations holding the values
   ;; that we are setting up for unknown-values return.
   (mutable move-vops)

   ;; the costs corresponding to the MOVE-VOPS. Separate because this
   ;; info is needed at meta-compile time, while the MOVE-VOPs don't
   ;; exist till load time. If no move is defined, then the entry is
   ;; NIL.
   (mutable move-costs)

   ;; similar to Move-VOPs, except that we only ever use the entries
   ;; for this SC and its alternates, since we never combine complex
   ;; representation conversion with argument passing.
   (mutable move-arg-vops)

   ;; true if this SC or one of its alternates in in the NUMBER-STACK SB.
   (mutable number-stack-p)

   ;; alignment restriction. The offset must be an even multiple of this.
   (mutable alignment)

   ;; a list of locations that we avoid packing in during normal
   ;; register allocation to ensure that these locations will be free
   ;; for operand loading. This prevents load-TN packing from thrashing
   ;; by spilling a lot.
   (mutable reserve-locations)))

(defprinter (sc)
  name)


;;;; TNs

(define-record-type tn 
  (parent sset-element)
  (protocol
   (mk-cl-protocol  
    #:parent ()
    #:subset (#:parent (number) #:this (kind primitive-type sc))
    (kind (missing-arg))   
    (primitive-type   #f)
    (leaf             #f)
    (next             #f)
    (reads            #f)
    (writes           #f)
    (next*            #f)
    (local            #f)
    (local-number     #f)
    (local-conflicts  (make-bitvector local-tn-limit #f))
    (global-conflicts #f)
    (current-conflict #f)
    (save-tn          #f)
    (sc               #f)
    (offset           #f)
    (cost              0)
    (physenv          #f)
    (loop-depth        0)))

  (fields
   ;; The kind of TN this is:
   ;;
   ;;   :NORMAL
   ;;    A normal, non-constant TN, representing a variable or temporary.
   ;;    Lifetime information is computed so that packing can be done.
   ;;
   ;;   :ENVIRONMENT
   ;;    A TN that has hidden references (debugger or NLX), and thus must be
   ;;    allocated for the duration of the environment it is referenced in.
   ;;
   ;;   :DEBUG-ENVIRONMENT
   ;;    Like :ENVIRONMENT, but is used for TNs that we want to be able to
   ;;    target to/from and that don't absolutely have to be live
   ;;    everywhere. These TNs are live in all blocks in the environment
   ;;    that don't reference this TN.
   ;;
   ;;   :COMPONENT
   ;;    A TN that implicitly conflicts with all other TNs. No conflict
   ;;    info is computed.
   ;;
   ;;   :SAVE
   ;;   :SAVE-ONCE
   ;;    A TN used for saving a :NORMAL TN across function calls. The
   ;;    lifetime information slots are unitialized: get the original
   ;;    TN out of the SAVE-TN slot and use it for conflicts. SAVE-ONCE
   ;;    is like :SAVE, except that it is only save once at the single
   ;;    writer of the original TN.
   ;;
   ;;   :SPECIFIED-SAVE
   ;;    A TN that was explicitly specified as the save TN for another TN.
   ;;    When we actually get around to doing the saving, this will be
   ;;    changed to :SAVE or :SAVE-ONCE.
   ;;
   ;;   :LOAD
   ;;    A load-TN used to compute an argument or result that is
   ;;    restricted to some finite SB. Load TNs don't have any conflict
   ;;    information. Load TN pack uses a special local conflict
   ;;    determination method.
   ;;
   ;;   :CONSTANT
   ;;    Represents a constant, with TN-LEAF a CONSTANT leaf. Lifetime
   ;;    information isn't computed, since the value isn't allocated by
   ;;    pack, but is instead generated as a load at each use. Since
   ;;    lifetime analysis isn't done on :CONSTANT TNs, they don't have
   ;;    LOCAL-NUMBERs and similar stuff.
   ;;
   ;;   :ALIAS
   ;;    A special kind of TN used to represent initialization of local
   ;;    call arguments in the caller. It provides another name for the
   ;;    argument TN so that lifetime analysis doesn't get confused by
   ;;    self-recursive calls. Lifetime analysis treats this the same
   ;;    as :NORMAL, but then at the end merges the conflict info into
   ;;    the original TN and replaces all uses of the alias with the
   ;;    original TN. SAVE-TN holds the aliased TN.
   (mutable kind)

   ;; the primitive-type for this TN's value. Null in restricted or
   ;; wired TNs.
   (mutable primitive-type)

   ;; If this TN represents a variable or constant, then this is the
   ;; corresponding LEAF.
   (mutable leaf)

   ;; thread that links TNs together so that we can find them
   (mutable next)

   ;; head of TN-REF lists for reads and writes of this TN
   (mutable reads)
   (mutable writes)

   ;; a link we use when building various temporary TN lists
   (mutable next*)

   ;; some block that contains a reference to this TN, or NIL if we
   ;; haven't seen any reference yet. If the TN is local, then this is
   ;; the block it is local to.
   (mutable local)

   ;; If a local TN, the block relative number for this TN. Global TNs
   ;; whose liveness changes within a block are also assigned a local
   ;; number during the conflicts analysis of that block. If the TN has
   ;; no local number within the block, then this is NIL.
   (mutable local-number)

   ;; If this object is a local TN, this slot is a bit-vector with 1
   ;; for the local-number of every TN that we conflict with.
   (mutable local-conflicts)

   ;; head of the list of GLOBAL-CONFLICTS structures for a global TN.
   ;; This list is sorted by block number (i.e. reverse DFO), allowing
   ;; the intersection between the lifetimes for two global TNs to be
   ;; easily found. If null, then this TN is a local TN.
   (mutable global-conflicts)

   ;; During lifetime analysis, this is used as a pointer into the
   ;; conflicts chain, for scanning through blocks in reverse DFO.
   (mutable current-conflict)

   ;; In a :SAVE TN, this is the TN saved. In a :NORMAL or :ENVIRONMENT
   ;; TN, this is the associated save TN. In TNs with no save TN, this
   ;; is null.
   (mutable save-tn)

   ;; After pack, the SC we packed into. Beforehand, the SC we want to
   ;; pack into, or null if we don't know.
   (mutable sc)

   ;; the offset within the SB that this TN is packed into. This is what
   ;; indicates that the TN is packed
   (mutable offset)

   ;; some kind of info about how important this TN is
   (mutable cost)

   ;; If a :ENVIRONMENT or :DEBUG-ENVIRONMENT TN, this is the
   ;; physical environment that the TN is live throughout.
   (mutable physenv)

   ;; The depth of the deepest loop that this TN is used in.
   (mutable loop-depth)))

(defprinter (tn) kind)
  
#;
(add-printer tn
   (lambda (tn stream)
     (print-tn-guts tn stream)))

(define* (make-random-tn #:key (kind #f) (sc #f) (offset #f))
  (make-tn 0 kind #f sc #:offset offset))

;;; The GLOBAL-CONFLICTS structure represents the conflicts for global
;;; TNs. Each global TN has a list of these structures, one for each
;;; block that it is live in. In addition to representing the result of
;;; lifetime analysis, the global conflicts structure is used during
;;; lifetime analysis to represent the set of TNs live at the start of
;;; the IR2 block.
(define-record-type global-conflicts
  (protocol
   (mk-cl-protocol
    #:subset (kind tn block number)
    (block          (missing-arg))
    (next-blockwise #f)
    (kind           #:read-only)
    (conflicts      (make-bitvector local-tn-limit #f))
    (tn             (missing-arg))
    (next-tnwise    #f)
    (number         #f)))

  (fields  
    ;; the IR2-BLOCK that this structure represents the conflicts for
   (mutable block)

   ;; thread running through all the GLOBAL-CONFLICTSs for BLOCK. This
   ;; thread is sorted by TN number
   (mutable next-blockwise)

   ;; the way that TN is used by BLOCK
   ;;
   ;;   :READ
   ;;     The TN is read before it is written. It starts the block live,
   ;;     but is written within the block.
   ;;
   ;;   :WRITE
   ;;     The TN is written before any read. It starts the block dead,
   ;;     and need not have a read within the block.
   ;;
   ;;   :READ-ONLY
   ;;     The TN is read, but never written. It starts the block live,
   ;;     and is not killed by the block. Lifetime analysis will promote
   ;;     :READ-ONLY TNs to :LIVE if they are live at the block end.
   ;;
   ;;   :LIVE
   ;;     The TN is not referenced. It is live everywhere in the block.
   (mutable kind)

   ;; a local conflicts vector representing conflicts with TNs live in
   ;; BLOCK. The index for the local TN number of each TN we conflict
   ;; with in this block is 1. To find the full conflict set, the :LIVE
   ;; TNs for BLOCK must also be included. This slot is not meaningful
   ;; when KIND is :LIVE.
   (mutable conflicts)

   ;; the TN we are recording conflicts for.
   (mutable tn)

   ;; thread through all the GLOBAL-CONFLICTSs for TN
   (mutable next-tnwise)

   ;; TN's local TN number in BLOCK. :LIVE TNs don't have local numbers.
   (mutable number)))

(defprinter (global-conflicts)
  tn
  block
  kind
  (number :test number))
