#include<libguile.h>
#include<stdio.h>
#include <sys/mman.h>
#include <unistd.h>
#include <errno.h>

scm_t_bits  page_size;

const char *str = "could not set execution flag on memory\n";

void set_flag (unsigned char *bv, size_t n)
{
  int len;    
  char *start = (char *) ((((scm_t_bits ) bv) / page_size) * page_size);
  len         = (scm_t_bits) bv + n - (scm_t_bits) start;
  //printf("head: %d %d %d %d\n",bv[0],bv[1],bv[2],bv[3]);
  if(mprotect (start, len, PROT_READ | PROT_WRITE | PROT_EXEC))
    {
      printf("ERROR ... ");
      if(errno == EINVAL)
        printf("%s addr is not a valid pointer or not a multiple of PAGESOZE\n"
               ,str);
      if(errno == EFAULT)
        printf("%s memory cannot be accessed\n"
               ,str);
      if(errno == EACCES)
        printf("%s the memory cannot be given the spicified access\n"
               ,str);
      if(errno == ENOMEM)
        printf("%s internal memory structurs could not be constructed\n"
               ,str);
    }
}

SCM_DEFINE(mk_rwx,"mk-rwx",1,0,0,(SCM x),"")
#define FUNC_NAME s_mk_rwx
{
  if(scm_is_bytevector (x))
    {
      set_flag((unsigned char *) SCM_BYTEVECTOR_CONTENTS(x),
               SCM_BYTEVECTOR_LENGTH(x));
    }
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME

SCM_DEFINE(run_native,"%run-native",2,0,0,(SCM x, SCM z),"")
#define FUNC_NAME s_run_native
{
  SCM ret = SCM_BOOL_F;
  void **y = &&cont;  
  //printf("start with %p\n",SCM_UNPACK(z));
  if(scm_is_bytevector (x))
    {
      asm("pushq %%rbx\n"
          "movq  %0,%%rax\n"
          "pushq %1\n"
          "jmp   %2"      ::"g" (SCM_UNPACK(z)),"g" (y),
          "g" (SCM_BYTEVECTOR_CONTENTS(x)):);
    cont:
      asm("movq  %%rax,%0":"=g" (ret)::);
      asm("popq  %rbx");
    }
  printf("back with %p, %p\n",SCM_UNPACK(ret)/4, (scm_t_bits) SCM_BYTEVECTOR_CONTENTS(x));
  return ret;
}
#undef FUNC_NAME


void init()
{
  page_size = sysconf(_SC_PAGESIZE);
#include  "run.x"
}
