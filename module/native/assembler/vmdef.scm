(define-module (native assembler vmdef)
  #:use-module (native assembler backend)
  #:use-module (native assembler sbcl)
  #:use-module (native assembler vop)
  #:use-module (srfi srfi-1)
  #:export (sc-or-lose define-storage-class define-storage-base
		       ))



;;;; implementation-independent facilities used for defining the
;;;; compiler's interface to the VM in a given implementation

;;;; This software is part of the SBCL system. See the README file for
;;;; more information.
;;;;
;;;; This software is derived from the CMU CL system, which was
;;;; written at Carnegie Mellon University and released into the
;;;; public domain. The software is in the public domain and is
;;;; provided with absolutely no warranty. See the COPYING and CREDITS
;;;; files for more information.

;;; Return the template having the specified name, or die trying.
#;
(define (template-or-lose x)
  (or (hash-ref *backend-template-names* x )
      (merror "~S is not a defined template." x)))

;;; Return the SC structure, SB structure or SC number corresponding
;;; to a name, or die trying.

(define (sc-or-lose x)
  (or (hashq-ref *backend-sc-names* x)
      (merror "~S is not a defined storage class." x)))


(define (sb-or-lose x)
  (or (hashq-ref *backend-sb-names* x)
      (merror "~S is not a defined storage base." x)))

#;
(define (sc-number-or-lose x)
  (sc-number (sc-or-lose x)))


;;; This is like the non-meta versions, except we go for the
;;; meta-compile-time info. These should not be used after load time,
;;; since compiling the compiler changes the definitions.
(define (meta-sc-or-lose x)
  (or (hashq-ref *backend-meta-sc-names* x)
      (error "~S is not a defined storage class." x)))

(define (meta-sb-or-lose x)
  (or (hashq-ref *backend-meta-sb-names* x)
      (error "~S is not a defined storage base." x)))

#|
(defun meta-sc-number-or-lose (x)
  (the sc-number (sc-number (meta-sc-or-lose x))))

;;;; side effect classes

(!def-boolean-attribute vop
  any)

;;;; move/coerce definition

;;; Compute at compiler load time the costs for moving between all SCs that
;;; can be loaded from FROM-SC and to TO-SC given a base move cost Cost.
(defun compute-move-costs (from-sc to-sc cost)
  (declare (type sc from-sc to-sc) (type index cost))
  (let ((to-scn (sc-number to-sc))
        (from-costs (sc-load-costs from-sc)))
    (dolist (dest-sc (cons to-sc (sc-alternate-scs to-sc)))
      (let ((vec (sc-move-costs dest-sc))
            (dest-costs (sc-load-costs dest-sc)))
        (setf (svref vec (sc-number from-sc)) cost)
        (dolist (sc (append (sc-alternate-scs from-sc)
                            (sc-constant-scs from-sc)))
          (let* ((scn (sc-number sc))
                 (total (+ (svref from-costs scn)
                           (svref dest-costs to-scn)
                           cost))
                 (old (svref vec scn)))
            (unless (and old (< old total))
              (setf (svref vec scn) total))))))))

;;;; primitive type definition

;;; Return the primitive type corresponding to the specified name, or
;;; die trying.
(defun primitive-type-or-lose (name)
  (the primitive-type
       (or (gethash name *backend-primitive-type-names*)
           (error "~S is not a defined primitive type." name))))

;;; Return true if SC is either one of PTYPE's SC's, or one of those
;;; SC's alternate or constant SCs.
(defun sc-allowed-by-primitive-type (sc ptype)
  (declare (type sc sc) (type primitive-type ptype))
  (let ((scn (sc-number sc)))
    (dolist (allowed (primitive-type-scs ptype) nil)
      (when (eql allowed scn)
        (return t))
      (let ((allowed-sc (svref *backend-sc-numbers* allowed)))
        (when (or (member sc (sc-alternate-scs allowed-sc))
                  (member sc (sc-constant-scs allowed-sc)))
          (return t))))))

;;;; generation of emit functions

(eval-when (:compile-toplevel :load-toplevel :execute)
  ;; We need the EVAL-WHEN because %EMIT-GENERIC-VOP (below)
  ;; uses #.MAX-VOP-TN-REFS, not just MAX-VOP-TN-REFS.
  ;; -- AL 20010218
  ;;
  ;; See also the description of VOP-INFO-TARGETS. -- APD, 2002-01-30
  (def!constant max-vop-tn-refs 256))

;;; FIXME: This is a remarkably eccentric way of implementing what
;;; would appear to be by nature a closure.  A closure isn't any more
;;; threadsafe than this special variable implementation, but at least
;;; it's more idiomatic, and one could imagine closing over an
;;; extensible pool to make a thread-safe implementation.
(defvar *vop-tn-refs* (make-array max-vop-tn-refs :initial-element nil))

(def!constant sc-bits (integer-length (1- sc-number-limit)))

(defun emit-generic-vop (node block template args results &optional info)
  (%emit-generic-vop node block template args results info))

(defun %emit-generic-vop (node block template args results info)
  (let* ((vop (make-vop block node template args results))
         (num-args (vop-info-num-args template))
         (last-arg (1- num-args))
         (num-results (vop-info-num-results template))
         (num-operands (+ num-args num-results))
         (last-result (1- num-operands))
         (ref-ordering (vop-info-ref-ordering template)))
    (declare (type vop vop)
             (type (integer 0 #.max-vop-tn-refs)
                   num-args num-results num-operands)
             (type (integer -1 #.(1- max-vop-tn-refs)) last-arg last-result))
    (setf (vop-codegen-info vop) info)
    (unwind-protect
         (let ((refs *vop-tn-refs*))
           (declare (type (simple-vector #.max-vop-tn-refs) refs))
           (do ((index 0 (1+ index))
                (ref args (and ref (tn-ref-across ref))))
               ((= index num-args))
             (setf (svref refs index) ref))
           (do ((index num-args (1+ index))
                (ref results (and ref (tn-ref-across ref))))
               ((= index num-operands))
             (setf (svref refs index) ref))
           (let ((temps (vop-info-temps template)))
             (when temps
               (let ((index num-operands)
                     (prev nil))
                 (dotimes (i (length temps))
                   (let* ((temp (aref temps i))
                          (tn (if (logbitp 0 temp)
                                  (make-wired-tn nil
                                                 (ldb (byte sc-bits 1) temp)
                                                 (ash temp (- (1+ sc-bits))))
                                  (make-restricted-tn nil (ash temp -1))))
                          (write-ref (reference-tn tn t)))
                     ;; KLUDGE: These formulas must be consistent with
                     ;; those in COMPUTE-REF-ORDERING, and this is
                     ;; currently maintained by hand. -- WHN
                     ;; 2002-01-30, paraphrasing APD
                     (setf (aref refs index) (reference-tn tn nil))
                     (setf (aref refs (1+ index)) write-ref)
                     (if prev
                         (setf (tn-ref-across prev) write-ref)
                         (setf (vop-temps vop) write-ref))
                     (setf prev write-ref)
                     (incf index 2))))))
           (let ((prev nil))
             (flet ((add-ref (ref)
                      (setf (tn-ref-vop ref) vop)
                      (setf (tn-ref-next-ref ref) prev)
                      (setf prev ref)))
               (declare (inline add-ref))
               (dotimes (i (length ref-ordering))
                 (let* ((index (aref ref-ordering i))
                        (ref (aref refs index)))
                   (if (or (= index last-arg) (= index last-result))
                       (do ((ref ref (tn-ref-across ref)))
                           ((null ref))
                         (add-ref ref))
                       (add-ref ref)))))
             (setf (vop-refs vop) prev))
           (let ((targets (vop-info-targets template)))
             (when targets
               (dotimes (i (length targets))
                 (let ((target (aref targets i)))
                   (target-if-desirable
                    (aref refs (ldb (byte 8 8) target))
                    (aref refs (ldb (byte 8 0) target)))))))
           (values vop vop))
      (fill *vop-tn-refs* nil))))

;;;; function translation stuff

;;; Add Template into List, removing any old template with the same name.
;;; We also maintain the increasing cost ordering.
(defun adjoin-template (template list)
  (declare (type template template) (list list))
  (sort (cons template
              (remove (template-name template) list
                      :key #'template-name))
        #'<=
        :key #'template-cost))

;;; Return a function type specifier describing TEMPLATE's type computed
;;; from the operand type restrictions.
(defun template-type-specifier (template)
  (declare (type template template))
  (flet ((convert (types more-types)
           (flet ((frob (x)
                    (if (eq x '*)
                        t
                        (ecase (first x)
                          (:or `(or ,@(map #'primitive-type-specifier
					   (rest x))))
                          (:constant `(constant-arg ,(third x)))))))
             `(,@(map #'frob types)
               ,@(when more-types
                   `(&rest ,(frob more-types)))))))
    (let* ((args (convert (template-arg-types template)
                          (template-more-args-type template)))
           (result-restr (template-result-types template))
           (results (if (template-conditional-p template)
                        '(boolean)
                        (convert result-restr
                                 (cond ((template-more-results-type template))
                                       ((/= (length result-restr) 1) '*)
                                       (t nil))))))
      `(function ,args
                 ,(if (= (length results) 1)
                      (first results)
                      `(values ,@results))))))


(defun template-conditional-p (template)
  (declare (type template template))
  (let ((rtypes (template-result-types template)))
    (or (eq rtypes :conditional)
        (eq (car rtypes) :conditional))))
|#

    
(define (find-key k l v)
  (if (pair? l)
      (if (eq? k (syntax->datum (car l)))
	  (cadr l)
	  (find-key k (cdr l) v))
      v))


(define-syntax define-storage-class 
  (lambda (x)	
    (syntax-case x ()
      ((_ name number sb-name-in . r)
       (let ((r (stx->list #'r)))
	 (let ((element-size  (find-key #:element-size r '1))
	       (alignment     (find-key #:alignment    r '1))
	       (locations     (find-key #:locations    r '()))
	       (reserve-locations 
		(find-key #:reserve-locations  r '()))
	       (save-p        (find-key #:save-p  r #f))
	       (alternate-scs (find-key #:alternate-scs r '()))
	       (constant-scs  (find-key #:constant-scs  r '())))
	   
	   #`(let ((nstack-p
		    (or (eq? 'sb-name-in
			     'non-descriptor-stack)
			(let loop ((x
				    (map meta-sc-or-lose 
					 '#,alternate-scs)))
			  (if (pair? x)
			      (if (eq? 'non-descriptor-stack
				       (sb-name (sc-sb (car x))))
				  #t
				  (loop (cdr x)))
			      #f)))))
	       (begin
		   (let ((res (make-sc #:name 'name #:number number
                             #:sb (meta-sb-or-lose 'sb-name-in)
                             #:element-size       #,element-size
                             #:alignment          #,alignment
                             #:locations         '#,locations
                             #:reserve-locations '#,reserve-locations
                             #:save?              #,save-p
                             #:number-stack?      nstack-p
                             #:alternate-scs (map meta-sc-or-lose
						  '#,alternate-scs)
                             #:constant-scs (map meta-sc-or-lose
						 '#,constant-scs))))
		     (hashq-set!  *backend-meta-sc-names*   'name   res)
		     (vector-set! *backend-meta-sc-numbers*  number res)
		     (vector-set! (sc-load-costs res) number 0)))

	       (let ((old (vector-ref *backend-sc-numbers* number)))
		 (when (and old (not (eq? (sc-name old) 'name)))
		       (warn (format #f 
				     "redefining SC number ~W from ~S to ~S" 
				     number
				     (sc-name old) 'name)))

		 (vector-set! *backend-sc-numbers* number
			      (meta-sc-or-lose 'name))
		 (hashq-set! *backend-sc-names* 'name 
			       (meta-sc-or-lose 'name))
		 (sc-sb-set! (sc-or-lose 'name) (sb-or-lose 'sb-name-in))
		 'name))))))))


;;; Define a storage base having the specified NAME. KIND may be :FINITE,
;;; :UNBOUNDED or :NON-PACKED. The following keywords are legal:
;;;    :SIZE specifies the number of locations in a :FINITE SB or
;;;          the initial size of an :UNBOUNDED SB.
;;;
;;; We enter the basic structure at meta-compile time, and then fill
;;; in the missing slots at load time.
(define-syntax define-storage-base 
  (lambda (x)
    (syntax-case x ()
      ((_ name k . l)
       (let ((nm   (syntax->datum #'name))
	     (kind (syntax->datum #'k))
	     (size (syntax->datum
		    (find-key #:size (stx->list #'l) #f))))

	 ;; SIZE is either mandatory or forbidden.
	 (case kind
	   ((#:non-packed)
	    (when size
		  (error "A size specification is meaningless in a ~S SB." 
			 kind)))
	   ((#:finite #:unbounded)
	    (unless size (error "Size is not specified in a ~S SB." kind))))
	 #`(let ((res (if (eq? k #:non-packed)
			  (make-sb #:name 'name #:kind k)
			  (make-finite-sb #:name 'name #:kind k 
					  #:size #,size))))
	     (hashq-set! *backend-meta-sb-names* 'name res)
	     (if (eq? k #:non-packed)
		 (hashq-set! *backend-sb-names* 'name
			     (copy-struct res))
		 (let ((res (copy-struct res)))
		   (finite-sb-always-live-set! 
		    res
		    (make-vector #,size
				 (make-bitvector 0)))
		   (finite-sb-conflicts-set! 
		    res
		    (make-vector #,size #()))
		   (finite-sb-live-tns-set! 
		    res
		    (make-vector #,size #f))
		   (finite-sb-always-live-count-set! 
		    res
		    (make-vector #,size 0))
		   (hashq-set! *backend-sb-names* 'name 
			 res)))

	     (set! *backend-sb-list*
		   (cons (sb-or-lose 'name)
			 (remove (lambda (x)
				   (eq? (sb-name x) 'name))
				 *backend-sb-list*)))
	     'name))))))
