(define-module (native assembler sbcl)
  #:use-module (rnrs bytevectors)
  #:use-module (rnrs records syntactic)
  #:use-module (ice-9 pretty-print)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-11)
  #:export (aver merror dotimes dolist subseq
		 write-sequence add-printer defprinter
		 mk-cl-protocol
		 aif awhile
		 !def-boolean-attribute attribute-set!
		 stx-cdr push
		 fill missing-arg print-tn-guts
		 stx-pair? stx->list stx-car stx-cdr
                 ldb ldb-set! ldb-test dpb
                 byte-spec byte byte? byte-size byte-position
		 deftype typep bug
                 return-frame return-from memb-case
                 lambda** alambda position
		 copy-struct))

(define-syntax-rule (alambda (s) a code ...)
  (letrec ((s (lambda a code ...))) s))
    
(define (missing-arg) #f)

(define (print-tn-guts . l)
  (warn "using unsupported debug routine"))

#;
(define-syntax-rule (when   pred code ...) 
  (if pred (begin code ...)))

#;
(define-syntax-rule (unless pred code ...) 
  (if (not pred) (begin code ...)))

(define (stx-cdr x)
  (syntax-case x () ((x . l) #'l)))

(define-syntax aif
  (syntax-rules ()
    ((aif (it) p x)
     (let ((it p)) (if it x)))
    ((aif (it) p x y)
     (let ((it p)) (if it x y)))))

(define-syntax-rule (aver expr)
  (unless expr
    (%failed-aver expr)))

(define (merror . l)
  (error (apply format l)))

(define (%failed-aver expr)
  (merror "<failed AVER: ~A>" expr))


(define-syntax-rule (dotimes (n e) code ...)
  (let loop ((n 0))
    (if (= n e)
	(values)
	(begin
	  (begin code ...)
	  (loop (+ n 1) )))))

(define-syntax-rule (dolist (l e) code ...)
  (let loop ((q e))
    (if (pair? q)
	(let ((l (car q)))
	  (begin code ...)
	  (loop (cdr q)))
	(values))))

(define (subseq buffer i0 i1)
  (let* ((n (- i1 i0))
	 (v (make-bytevector n)))
    (bytevector-copy! buffer i0 v 0 n)
    v))

;;Write the bytesvector v to the stream.
(define (write-sequence v stream)
  (error "write-sequence - todo"))


(define (reduce x)
  (let loop ((x x))
    (if (pair? x)
	(if (car x)
	    (cons (car x) (loop (cdr x)))
	    (loop (cdr x)))
	'())))
  
(define (add-printer nm f)
  (struct-set! nm 4 f))

(define-syntax defprinter
  (lambda (x)
    (define (mk x y)
      (datum->syntax x (string->symbol (format #f "~a-~a" 
					       (syntax->datum x)
					       (syntax->datum y)))))

    (syntax-case x ()
      ((_ (nm) form ...)
       (let ()
	 (define (fms x)
	   (syntax-case x (:test)
	     ((f :test (x ...))
		 #`(lambda (o) (if ((x ...) o)
				(#,(mk #'nm #'f) o)
				#f)))
	     ((f :test p)
	      #`(lambda (o) (if (#,(mk #'nm #'p) o)
				(#,(mk #'nm #'f) o)
				#f)))
	     (f
	      (mk #'nm #'f))))

	 (with-syntax (((fms ...) (map fms #'(form ...))))		    
	    #'(add-printer nm
		 (lambda (o stream)
		   (format stream "<~a |~{ ~a~}>" 'nm 
			   (reduce (list (fms o) ...)))))))))))
	    
	
(define (find-rest subs rs)
  (let loop ((subs subs) (ret rs))
    (if (stx-pair? subs)
	(let ((s (stx-car subs)))
	  (loop (stx-cdr subs) 
		(remove (lambda (x) (free-identifier=? s (stx-car x)))
			ret)))
	ret)))

(define (pp x)
  (pretty-print (syntax->datum x))
  x)

(define-syntax mk-sb-protocol-parent
  (lambda (x)
    (syntax-case x ()
      ((_ pq #:subset (#:parent (p ...) #:this (subs ...)) (nm init) ...)
       (with-syntax (((rest ...) (find-rest #'(subs ...) #'((nm init) ...)))
		     ((kp   ...) (let ((f (lambda (x) (symbol->keyword 
						       (syntax->datum x)))))
				   (let loop ((l #'(p ...)))
				     (if (pair? l)
					 `(,(f (car l)) ,(car l) 
					   ,@(loop (cdr l)))
					 '())))))
	
	 #'(lambda (n)
	     (lambda* (p ... subs ... #:key rest ...) 
		      (let ((f (n kp ...)))
			(f nm ...))))))

      ((_ (p ...) (nm init) ...)
       (with-syntax (((pk ...) (map (lambda (x) 
				      (symbol->keyword (syntax->datum x)))
				    #'(p ...))))
       #'(lambda (n)	   
	   (lambda* (#:key (p #:not-supplied) ... (nm init) ...)
		    
	      (let* ((q (let loop ((pkk (list pk ...)) (ps (list p ...)))
			  (if (pair? ps)
			      (if (eq? (car ps) #:not-supplied)
				  (loop (cdr pkk) (cdr ps))
				  `(,(car pkk) ,(car ps) 
				    ,@(loop (cdr pkk) (cdr ps))))
			      '())))
		     (f (apply n q)))
		(f nm ...)))))))))

(define-syntax mk-cl-protocol
  (lambda (x)
    (syntax-case x ()
      ((_ #:parent (p ...) . l)
       #'(mk-sb-protocol-parent (p ...) . l))
      ((_ #:subset (subs ...) (nm init) ...)
       (with-syntax (((rest ...) (find-rest #'(subs ...) #'((nm init) ...))))
	 #'(lambda (n)
	     (lambda* (subs ... #:key rest ...) (n nm ...)))))		
      ((_ (nm init) ...)
       #'(lambda (n)
	   (lambda* (#:key (nm init) ...)
		    (n nm ...)))))))

(define (mk-sym n str . l)
  (datum->syntax n 
    (string->symbol
     (apply format #f str (map syntax->datum l)))))

(define-record-type attribute
  (fields (mutable name) (mutable assq)))

(defprinter (attribute) name)

(define-syntax !def-boolean-attribute
  (lambda (x)
    (syntax-case x ()
      ((_ nm attr ...)
       (with-syntax ((check? (mk-sym #'nm "~a-attribute?" #'nm))
		     (mk     (mk-sym #'nm "make-~a"       #'nm)))
		    
        #'(begin
	   (define-syntax-rule (check? a m) 
	     (let ((r (assq 'm (attribute-assq a))))
	       (if r
		   (cdr r)
		   (merror "attribute ~a does not support attr ~a" a 'm))))
	   (define mk (lambda () (make-attribute 'nm '((attr . #f) ...))))))))))

(define-syntax-rule (attribute-set! (ch a k) v) 
  (let ((q (ch a k)))
    (if q
	(set-cdr! q v)
	(merror "attribute ~a does not support ~a" a 'k))))

(define-syntax-rule (awhile (x v) code ...)
  (let loop ((x v) (ret #f))
    (if x 
	(let ((ret (begin code ...)))
	  (loop v ret))
	ret)))

(define (fill ar x)
  (dotimes (i (vector-length ar))
     (vector-set! ar i x)))

(define-syntax-rule (push x l) (set! l (cons x l)))

(define-record-type byte
  (fields
   (mutable size)
   (mutable position)))


(defprinter (byte) size position)
(define byte-spec byte)
(define byte      make-byte)


(define (ldb bs i) 
  (let ((pos   (byte-position bs))
	(size  (byte-size bs)))
    (bit-extract i pos (+ pos size))))
 
(define (dpb newbyte bs i)
  (let* ((pos  (byte-position bs))
         (size (byte-size bs))
         (end  (+ pos size))
         (low  (bit-extract i 0 pos))
         (high (ash i (- end)))
         (item (bit-extract newbyte 0 size)))
    (logior 
     low
     (ash item pos)
     (ash high end))))

(define-syntax-rule (ldb-set! bs i new) (set! i (dpb new bs i)))
  

(define (ldb-test bs i)
  (not (= 0 (ldb bs i))))

(define (stx-pair? x) (syntax-case x () ((x . l) #t) (_ #f)))
(define (stx-car x)   (syntax-case x () ((x . l) #'x)))
(define (stx-cdr x)   (syntax-case x () ((x . l) #'l)))
(define (stx->list x) 
  (syntax-case x () 
    ((x . l) (cons #'x (stx->list #'l))) 
    (_       x)))

(define (bug . l) (error (string-append "BUG: " (apply format #f l))))

(define-record-type type
  (fields
   (mutable spec)))

(define-syntax-rule (deftype name arg spec)
  (define name (make-type spec)))

(define (typep x tp)
  (if (struct? x)
      (eq? (struct-vtable x) tp)
      (error "typep has to work on structs in order for it to work")))

(define-syntax-rule (return-frame tag code ...)
  (call-with-prompt tag
    (lambda () code ...)
    (lambda (s v) v)))
      
(define-syntax-rule (return-from tag v) (abort-to-prompt tag v))
  
(define-syntax-rule (memb-case z (a . l) ...)
  (let ((x z))
    (cond ((<member> x a) . l) ...)))

(define-syntax <member>
  (syntax-rules (else)
    ((_ x else) #t)
    ((_ x a   ) (member x a))))

(define (em x k) (if (null? x) '() `(,k ,@x)))

(define (grok-args args)
  (let ((ks '())
        (os '())
        (le '())
        (vs '()))
    (define (exit) (values (reverse ks)
                           (reverse os)
                           (reverse vs)
                           (reverse le)))
    (define (collect-var x) (push x vs))
    (define (collect-opt x) (push x os))
    (define (collect-let x) (push x le))
    (define (collect-key x) (push x ks))
      
    (let loop ((state #f) (args args))
      (case state
        ((#f)
         (syntax-case args ()
           ((#:optional . l)
            (loop #:optional #'l))
           ((#:key     . l)
            (loop #:key      #'l))
           ((x . l)
            (begin
              (collect-var #'x)
              (loop state #'l)))
           (() 
            (exit))))

        ((#:optional)
         (syntax-case args ()
           ((#:key . l)
            (loop #:key     #'l))
           (((x v s) . l)
            (begin
              (collect-opt #'(x #:initialize-lambda))
              (collect-let #'(s (not (eq? x #:initialize-lambda))))
              (collect-let #'(x (if s x v)))
              (loop state #'l)))
           ((x . l)
            (begin
              (collect-opt #'x)
              (loop state #'l)))
           (() (exit))))
        
        ((#:key)
         (syntax-case args ()
           (((x v s) . l)
            (begin
              (collect-key #'(x v))
              (collect-let #'(s v))
              (loop state #'l)))
           ((x . l)
            (begin
              (collect-key #'x)
              (loop state  #'l)))
           (() (exit))))))))

(define-syntax lambda** 
  (lambda (x)
    (syntax-case x ()
      ((_ args code ...)
       (let-values (((keys optional vars tr-vars) (grok-args #'args)))
         (with-syntax (((key  ...) (em keys     #:key))
                       ((opt  ...) (em optional #:optional))
                       ((var  ...) vars)
                       ((tv   ...) tr-vars))
           #'(lambda* (var ... opt ... key ...)
                      (let* (tv ...) code ...))))))))

(define (position x l)
  (let loop ((l l) (i 0))
    (if (pair? l)
        (if (eq? (car l) x)
            i
            (loop (cdr l) (+ i 1)))
        #f)))

(define (struct->list x)
  (let loop ((n 0) (l '()))
    (catch #t
      (lambda () (loop (+ n 1) (cons (struct-ref x n) l)))
      (lambda x  (reverse l)))))

(define (copy-struct s)
  (let ((vt (struct-vtable s)))
    (apply make-struct vt 0 (struct->list s))))
