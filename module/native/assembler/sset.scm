(define-module (native assembler sset)
  #:use-module (rnrs records syntactic)
  #:export (make-sset sset-empty? sset? sset-adjoin sset-delete
		      sset-element
		      do-sset-elements))

(define-record-type sset-element  
  (fields 
   (mutable number)))

(define s? sset-element?)
(define s  sset-element-number)

(define-record-type (sset mk sset?)
  (fields
   (mutable table)
   (mutable n)))

(define (make-sset) 
  (mk (make-hash-table) 0))

(define (sset-empty? set)
  (= (sset-n set) 0))

(define (sset-adjoin x set)
  (let ((table (sset-table set)))
    (when (not (hashx-ref hash-x assoc-x table x))
	  (hashx-set! hash-x assoc-x table x #t)
	  (sset-n-set! set (+ 1 (sset-n set))))))

(define assoc-x assq)
(define (hash-x x n)
  (if (s? x)
      (hashq (s x) n)
      (error "need a struct with a sset-element parent")))
      

(define (sset-delete x set)
  (let ((table (sset-table set)))
    (when (not (hashx-ref hash-x assoc-x table x))
	  (hashx-remove! hash-x assoc-x table x)
	  (sset-n-set! set (- (sset-n set) 1)))))

(define-syntax-rule (do-sset-elements (x set) code ...)
  (hash-for-each
   (lambda (x val)
     code ...)
   (sset-table set)))