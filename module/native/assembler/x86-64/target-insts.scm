(define-module (native assembler x86-64 target-insts)
  #:use-module (native assembler sbcl)
  #:use-module (native assembler disassem)
  #:use-module (native assembler target-disassem)
  #:export (print-mem-access))


;; Stubs to make it possible to compile an assembler
(define-syntax-rule (mk name) 
  (define (name . l) 
    (merror "~a not-implemented" 'name)))
(mk nth-value)

;;;; target-only stuff from CMU CL's src/compiler/x86/insts.lisp
;;;;
;;;; i.e. stuff which was in CMU CL's insts.lisp file, but which in
;;;; the SBCL build process can't be compiled into code for the
;;;; cross-compilation host

;;;; This software is part of the SBCL system. See the README file for
;;;; more information.
;;;;
;;;; This software is derived from the CMU CL system, which was
;;;; written at Carnegie Mellon University and released into the
;;;; public domain. The software is in the public domain and is
;;;; provided with absolutely no warranty. See the COPYING and CREDITS
;;;; files for more information.

;;; Prints a memory reference to STREAM. VALUE is a list of
;;; (BASE-REG OFFSET INDEX-REG INDEX-SCALE), where any component may be
;;; missing or nil to indicate that it's not used or has the obvious
;;; default value (e.g., 1 for the index-scale). BASE-REG can be the
;;; symbol RIP or a full register, INDEX-REG a full register. If WIDTH
;;; is non-nil it should be one of the symbols :BYTE, :WORD, :DWORD or
;;; :QWORD and a corresponding size indicator is printed first.
(define (print-mem-access value width stream dstate)
  (when width
    (write width stream)
    (format stream "| PTR |"))
  (write-char #\[ stream)
  (let ((firstp #t) (rip-p #f))
    (let-syntax ((pel 
                  (lambda (x)
                    (syntax-rules ()
                      ((_ (var val) body ...)
                       ;; Print an element of the address, maybe with
                       ;; a leading separator.
                       (let ((var val))
                         (when var
                           (unless firstp
                             (write-char #\+ stream))
                           body ...
                           (set! firstp #f))))))))
      (pel (base-reg (car value))
           (cond ((eqv? 'rip base-reg)
                  (set! rip-p #t)
                  (write base-reg stream))
                 (else
                  (print-addr-reg base-reg stream dstate))))

      (pel (index-reg (third value))
        (print-addr-reg index-reg stream dstate)
        (let ((index-scale (list-ref value 4)))
          (when (and index-scale (not (= index-scale 1)))
            (write-char #\* stream)
            (write index-scale stream))))

      (let ((offset (cadr value)))
        (when (and offset (or firstp (not (zero? offset))))
          (unless (or firstp (negative? offset))
            (write-char #\+ stream))
          (cond
           (rip-p
            (write offset stream)
            (let ((addr (+ offset (dstate-next-addr dstate))))
               (when (positive? addr)
                 (or (nth-value 1
                                (note-code-constant-absolute
                                 addr dstate))
                     (maybe-note-assembler-routine addr
                                                   #f
                                                   dstate)))))
            (firstp
             (begin
               (princ16 offset stream)
               (or (negative? offset)
                   (nth-value 1
                              (note-code-constant-absolute offset dstate))
                   (maybe-note-assembler-routine offset
                                                 #f
                                                 dstate))))
            (else
             (write offset stream)))))))
  (write-char #\] stream))
