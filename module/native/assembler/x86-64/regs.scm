(define-module (native assembler x86-64 regs)
  #:use-module (native assembler sbcl)
  #:use-module (native assembler vop)
  #:use-module (native assembler vmdef)
  #:export (
	    byte-register-names*
	    *word-register-names*
	    *dword-register-names*
	    *qword-register-names*
	    *float-register-names*
	    *byte-sc-names*
	    *word-sc-names*
	    *dword-sc-names*
	    *qword-sc-names*
	    *float-sc-names*
	    *double-sc-names*
	    *complex-sc-names*
	    ))

(define t #t)

(eval-when (compile load eval)
  (define *byte-register-names*  (make-vector 32 #f))
  (define *word-register-names*  (make-vector 32 #f))
  (define *dword-register-names* (make-vector 32 #f))
  (define *qword-register-names* (make-vector 32 #f))
  (define *float-register-names* (make-vector 16 #f))
  
  (define *byte-sc-names*  '(byte-reg))
  (define *word-sc-names*  '(word-reg))
  (define *dword-sc-names* '(dword-reg))
  (define *qword-sc-names*
    '(any-reg descriptor-reg sap-reg signed-reg unsigned-reg control-stack
	      signed-stack unsigned-stack sap-stack single-stack
	      constant))
;;; added by jrd. I guess the right thing to do is to treat floats
;;; as a separate size...
;;;
;;; These are used to (at least) determine operand size.
    (define *float-sc-names*   '(single-reg))
    (define *double-sc-names*  '(double-reg double-stack))
    (define *complex-sc-names* '(complex-single-reg 
				 complex-single-stack
				 complex-double-reg complex-double-stack))
    )

(eval-when (compile load eval)
  (define *byte-register-names* (make-vector 32 #f))
  (define *word-register-names* (make-vector 32 #f))
  (define *dword-register-names* (make-vector 32 #f))
  (define *qword-register-names* (make-vector 32 #f))
  (define *float-register-names* (make-vector 16 #f))

  (define (symbolicate stx . l)
    (let ((str (apply string-append
		      (map (lambda (x)
			   (let ((x (syntax->datum x)))
			     (cond
			      ((string? x)
			       x)
			      ((keyword? x)
			       (symbol->string 
				(keyword->symbol x)))
			      (#t
			       (format #f "~a" x)))))							 l))))
      (datum->syntax stx (string->symbol str)))))


;;; There are 16 registers really, but we consider them 32 in order to
;;; describe the overlap of byte registers. The only thing we need to
;;; represent is what registers overlap. Therefore, we consider bytes
;;; to take one unit, and [dq]?words to take two. We don't need to
;;; tell the difference between [dq]?words, because you can't put two
;;; words in a dword register.
(eval-when (load compile eval)
	   (define-storage-base registers #:finite #:size 32)

	   (define-storage-base float-registers #:finite #:size 16)

	   (define-storage-base stack #:unbounded #:size 8)
	   (define-storage-base constant #:non-packed)
	   (define-storage-base immediate-constant #:non-packed)
	   (define-storage-base noise #:unbounded #:size 2))

(define-syntax !define-storage-classes 
  (lambda (x)
    (syntax-case x ()
      ((_ . classes)
       (let loop ((index 0) (forms '()) (cls (stx->list #'classes)))
	 (if (pair? cls)
	     (let ((class (car cls)))
	       (let* ((sc-name       
		       (stx-car class))
		      (constant-name 
		       (symbolicate sc-name sc-name "-sc-number")))

		 (loop (+ index 1)
		       `(,#`(define-storage-class #,sc-name #,index
			     #,@(stx-cdr class))
			 ,#`(define #,constant-name #,index)
			 ,@forms)
		       (cdr cls))))
	     #`(begin
		 #,@(reverse forms))))))))

;;; The DEFINE-STORAGE-CLASS call for CATCH-BLOCK refers to the size
;;; of CATCH-BLOCK. The size of CATCH-BLOCK isn't calculated until
;;; later in the build process, and the calculation is entangled with
;;; code which has lots of predependencies, including dependencies on
;;; the prior call of DEFINE-STORAGE-CLASS. The proper way to
;;; unscramble this would be to untangle the code, so that the code
;;; which calculates the size of CATCH-BLOCK can be separated from the
;;; other lots-of-dependencies code, so that the code which calculates
;;; the size of CATCH-BLOCK can be executed early, so that this value
;;; is known properly at this point in compilation. However, that
;;; would be a lot of editing of code that I (WHN 19990131) can't test
;;; until the project is complete. So instead, I set the correct value
;;; by hand here (a sort of nondeterministic guess of the right
;;; answer:-) and add an assertion later, after the value is
;;; calculated, that the original guess was correct.
;;;
;;; (What a KLUDGE! Anyone who wants to come in and clean up this mess
;;; has my gratitude.) (FIXME: Maybe this should be me..)
(!define-storage-classes

  ;; non-immediate constants in the constant pool
  (constant constant)

  (fp-single-zero immediate-constant)
  (fp-double-zero immediate-constant)
  (fp-complex-single-zero immediate-constant)
  (fp-complex-double-zero immediate-constant)

  (fp-single-immediate immediate-constant)
  (fp-double-immediate immediate-constant)
  (fp-complex-single-immediate immediate-constant)
  (fp-complex-double-immediate immediate-constant)

  (immediate immediate-constant)

  ;;
  ;; the stacks
  ;;

  ;; the control stack
  (control-stack stack)                 ; may be pointers, scanned by GC

  ;; the non-descriptor stacks
  ;; XXX alpha backend has :element-size 2 :alignment 2 in these entries
  (signed-stack stack)                  ; (signed-byte 64)
  (unsigned-stack stack)                ; (unsigned-byte 64)
  (character-stack stack)               ; non-descriptor characters.
  (sap-stack stack)                     ; System area pointers.
  (single-stack stack)                  ; single-floats
  (double-stack stack)
  (complex-single-stack stack)  ; complex-single-floats
  (complex-double-stack stack #:element-size 2)  ; complex-double-floats


  ;;
  ;; magic SCs
  ;;

  (ignore-me noise)

  ;;
  ;; things that can go in the integer registers
  ;;

  ;; On the X86, we don't have to distinguish between descriptor and
  ;; non-descriptor registers, because of the conservative GC.
  ;; Therefore, we use different scs only to distinguish between
  ;; descriptor and non-descriptor values and to specify size.

  ;; immediate descriptor objects. Don't have to be seen by GC, but nothing
  ;; bad will happen if they are. (fixnums, characters, header values, etc).
  (any-reg registers
           #:locations *qword-regs*
           #:element-size 2 ; I think this is for the al/ah overlap thing
           #:constant-scs (immediate)
           #:save-p #t
           #:alternate-scs (control-stack))

  ;; pointer descriptor objects -- must be seen by GC
  (descriptor-reg registers
                  #:locations *qword-regs*
                  #:element-size 2
;                 #:reserve-locations (eax-offset)
                  #:constant-scs (constant immediate)
                  #:save-p #t
                  #:alternate-scs (control-stack))

  ;; non-descriptor characters
  (character-reg registers
                 #:locations *qword-regs*
                 #:element-size 2
                 #:reserve-locations (al-offset)
                 #:constant-scs (immediate)
                 #:save-p #t
                 #:alternate-scs (character-stack))

  ;; non-descriptor SAPs (arbitrary pointers into address space)
  (sap-reg registers
           #:locations *qword-regs*
           #:element-size 2
;          #:reserve-locations (#.eax-offset)
           #:constant-scs (immediate)
           #:save-p #t
           #:alternate-scs (sap-stack))

  ;; non-descriptor (signed or unsigned) numbers
  (signed-reg registers
              #:locations *qword-regs*
              #:element-size 2
              #:constant-scs (immediate)
              #:save-p #t
              #:alternate-scs (signed-stack))
  (unsigned-reg registers
                #:locations *qword-regs*
                #:element-size 2
                #:constant-scs (immediate)
                #:save-p #t
                #:alternate-scs (unsigned-stack))

  ;; miscellaneous objects that must not be seen by GC. Used only as
  ;; temporaries.
  (word-reg registers
            #:locations *word-regs*
            #:element-size 2
            )
  (dword-reg registers
            #:locations *dword-regs*
            #:element-size 2
            )
  (byte-reg registers
            #:locations *byte-regs*
            )

  ;; that can go in the floating point registers

  ;; non-descriptor SINGLE-FLOATs
  (single-reg float-registers
              #:locations *float-regs*
              #:constant-scs (fp-single-zero fp-single-immediate)
              #:save-p #t
              #:alternate-scs (single-stack))

  ;; non-descriptor DOUBLE-FLOATs
  (double-reg float-registers
              #:locations *float-regs*
              #:constant-scs (fp-double-zero fp-double-immediate)
              #:save-p #t
              #:alternate-scs (double-stack))

  (complex-single-reg float-registers
                      #:locations *float-regs*
                      #:constant-scs (fp-complex-single-zero fp-complex-single-immediate)
                      #:save-p #t
                      #:alternate-scs (complex-single-stack))

  (complex-double-reg float-registers
                      #:locations *float-regs*
                      #:constant-scs (fp-complex-double-zero fp-complex-double-immediate)
                      #:save-p t
                      #:alternate-scs (complex-double-stack))

  ;; a catch or unwind block
  (catch-block stack #:element-size 5))

(define-syntax defreg 
  (lambda (x)
    (syntax-case x ()
      ((_ name offset size)
       (with-syntax ((offset-sym 
		      (symbolicate #'name #'name "-offset"))
		     (names-vector 
		      (symbolicate #'name "*" #'size "-register-names*")))
	 #`(begin
	    (eval-when (compile load eval)
	       ;; EVAL-WHEN is necessary because stuff like #.EAX-OFFSET
	       ;; (in the same file) depends on compile-time evaluation
	       ;; of the DEFCONSTANT. -- AL 20010224
	     (define offset-sym offset)
	     (vector-set! names-vector offset-sym
			  'name))))))))

(define-syntax defregset
  (lambda (x)
    (syntax-case x ()
      ((_ name . regs)
       ;; FIXME: It looks to me as though DEFREGSET should also
       ;; define the related *FOO-REGISTER-NAMES* variable.
       #`(eval-when (compile load eval)
                (define name
                  (list #,@(map (lambda (nm)
                                    (symbolicate nm nm "-offset"))
				(stx->list #'regs)))))))))

  ;; byte registers
  ;;
  ;; Note: the encoding here is different than that used by the chip.
  ;; We use this encoding so that the compiler thinks that AX (and
  ;; EAX) overlap AL and AH instead of AL and CL.
  ;;
  ;; High-byte are registers disabled on AMD64, since they can't be
  ;; encoded for an op that has a REX-prefix and we don't want to
  ;; add special cases into the code generation. The overlap doesn't
  ;; therefore exist anymore, but the numbering hasn't been changed
  ;; to reflect this.
  (defreg al    0 #:byte)
  (defreg cl    2 #:byte)
  (defreg dl    4 #:byte)
  (defreg bl    6 #:byte)
  (defreg sil  12 #:byte)
  (defreg dil  14 #:byte)
  (defreg r8b  16 #:byte)
  (defreg r9b  18 #:byte)
  (defreg r10b 20 #:byte)
  (defreg r11b 22 #:byte)
  (defreg r12b 24 #:byte)
  (defreg r13b 26 #:byte)
  (defreg r14b 28 #:byte)
  (defreg r15b 30 #:byte)
  (defregset *byte-regs*
      al cl dl bl sil dil r8b r9b r10b
      r11b r12b r13b r14b r15b)

  ;; word registers
  (defreg ax 0 #:word)
  (defreg cx 2 #:word)
  (defreg dx 4 #:word)
  (defreg bx 6 #:word)
  (defreg sp 8 #:word)
  (defreg bp 10 #:word)
  (defreg si 12 #:word)
  (defreg di 14 #:word)
  (defreg r8w  16 #:word)
  (defreg r9w  18 #:word)
  (defreg r10w 20 #:word)
  (defreg r11w 22 #:word)
  (defreg r12w 24 #:word)
  (defreg r13w 26 #:word)
  (defreg r14w 28 #:word)
  (defreg r15w 30 #:word)
  (defregset *word-regs* ax cx dx bx si di r8w r9w r10w
             r11w r12w r13w r14w r15w)

  ;; double word registers
  (defreg eax 0 #:dword)
  (defreg ecx 2 #:dword)
  (defreg edx 4 #:dword)
  (defreg ebx 6 #:dword)
  (defreg esp 8 #:dword)
  (defreg ebp 10 #:dword)
  (defreg esi 12 #:dword)
  (defreg edi 14 #:dword)
  (defreg r8d  16 #:dword)
  (defreg r9d  18 #:dword)
  (defreg r10d 20 #:dword)
  (defreg r11d 22 #:dword)
  (defreg r12d 24 #:dword)
  (defreg r13d 26 #:dword)
  (defreg r14d 28 #:dword)
  (defreg r15d 30 #:dword)
  (defregset *dword-regs* eax ecx edx ebx esi edi r8d r9d r10d
             r11d r12w r13d r14d r15d)

  ;; quadword registers
  (defreg rax 0 #:qword)
  (defreg rcx 2 #:qword)
  (defreg rdx 4 #:qword)
  (defreg rbx 6 #:qword)
  (defreg rsp 8 #:qword)
  (defreg rbp 10 #:qword)
  (defreg rsi 12 #:qword)
  (defreg rdi 14 #:qword)
  (defreg r8  16 #:qword)
  (defreg r9  18 #:qword)
  (defreg r10 20 #:qword)
  (defreg r11 22 #:qword)
  (defreg r12 24 #:qword)
  (defreg r13 26 #:qword)
  (defreg r14 28 #:qword)
  (defreg r15 30 #:qword)
  ;; for no good reason at the time, r12 and r13 were missed from the
  ;; list of qword registers.  However
  ;; <jsnell> r13 is already used as temporary [#lisp irc 2005/01/30]
  ;; and we're now going to use r12 for the struct thread*
  ;;
  ;; Except that now we use r11 instead of r13 as the temporary,
  ;; since it's got a more compact encoding than r13, and experimentally
  ;; the temporary gets used more than the other registers that are never
  ;; wired. -- JES, 2005-11-02
  (defregset *qword-regs* rax rcx rdx rbx rsi rdi
             r8 r9 r10 r11 r12 r13  r14 r15)

  ;; floating point registers
  (defreg float0 0 #:float)
  (defreg float1 1 #:float)
  (defreg float2 2 #:float)
  (defreg float3 3 #:float)
  (defreg float4 4 #:float)
  (defreg float5 5 #:float)
  (defreg float6 6 #:float)
  (defreg float7 7 #:float)
  (defreg float8 8 #:float)
  (defreg float9 9 #:float)
  (defreg float10 10 #:float)
  (defreg float11 11 #:float)
  (defreg float12 12 #:float)
  (defreg float13 13 #:float)
  (defreg float14 14 #:float)
  (defreg float15 15 #:float)
  (defregset *float-regs* float0 float1 float2 float3 float4 float5 float6 
    float7 float8 float9 float10 float11 float12 float13 float14 float15)



(define-syntax def-misc-reg-tns 
  (lambda (x)
    (syntax-case x ()
      ((_ sc-name . reg-names)
       (let loop ((regs (stx->list #'reg-names)) (forms '()) (exp '()))
	 (if (pair? regs)
	     (let ((reg (car regs)))
	       (let ((tn-name     (symbolicate reg reg "-tn"))
		     (offset-name (symbolicate reg reg "-offset")))
                          ;; FIXME: It'd be good to have the special
                          ;; variables here be named with the *FOO*
                          ;; convention.
		 (loop (cdr regs)
		       (cons
                        #`(define #,tn-name
			    (make-random-tn #:kind   #:normal
					    #:sc     (sc-or-lose 'sc-name)
					    #:offset #,offset-name))
			forms)
		       (cons tn-name exp))))
                                                    
	     #`(begin #,@forms (export #,@exp))))))))

(def-misc-reg-tns unsigned-reg rax rbx rcx rdx rbp rsp rdi rsi
  r8 r9 r10 r11 r12 r13 r14 r15)
(def-misc-reg-tns dword-reg eax ebx ecx edx ebp esp edi esi
  r8d r9d r10d r11d r12d r13d r14d r15d)
(def-misc-reg-tns word-reg ax bx cx dx bp sp di si
  r8w r9w r10w r11w r12w r13w r14w r15w)
(def-misc-reg-tns byte-reg al cl dl bl sil dil r8b r9b r10b
  r11b r12b r13b r14b r15b)
(def-misc-reg-tns single-reg
  float0 float1 float2 float3 float4 float5 float6 float7
  float8 float9 float10 float11 float12 float13 float14 float15)

