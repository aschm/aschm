(define-module (native assembler params)
  #:export(n-word-bits
	   n-machine-word-bits
	   n-byte-bits
	   minimum-immediate-offset
	   maximum-immediate-offset
	   float-sign-shift
	   location-number
           other-pointer-lowtag
	   emit-nop x86 x86-64
	   make-stack-pointer-tn
           make-dynamic-state-tns
           *backend-byte-order*
           frame-byte-offset))

;;;; machine architecture parameters

(define (frame-byte-offset index)
  (error "frame-byte-offset is not supported because it belongs to the 
vm code"))

(define (make-stack-pointer-tn . l)
  (error "make-stack-pointer-tn is not supported"))

(define (make-dynamic-state-tns . l)
  (error "make-dynamic-state-tns is not supported"))

(define *backend-byte-order* #f)

;;; the number of bits per word, where a word holds one lisp descriptor
(define n-word-bits 64)

;;; the natural width of a machine word (as seen in e.g. register width,
;;; address space)
(define n-machine-word-bits 64)

;;; the number of bits per byte, where a byte is the smallest
;;; addressable object
(define n-byte-bits 8)

;;; The minimum immediate offset in a memory-referencing instruction.
(define minimum-immediate-offset (- (expt 2 31)))

;;; The maximum immediate offset in a memory-referencing instruction.
(define maximum-immediate-offset (1- (expt 2 31)))

(define float-sign-shift 31)

(define location-number 
  (lambda x (error "this should be implemented in backend")))

(define emit-nop
  (lambda x (error "this should be implemented in backend")))

(define other-pointer-lowtag 2)

(define x86    #f)
(define x86-64 #f)
