(define-module (native vm constants)
  #:export (
            SCM_BOOL_F
            SCM_BOOL_T
            SCM_EOL
            SCM_UNDEFINED
            SCM_UNSPECIFIED
            SCM_ELISP_NIL
            SCM_INUM0
            SCM_INUM1

            tc7_variable
            tc7_symbol
            tc7_vector
            tc3_struct
            tc7_program

            tc8_char
            ))

(let ((file  (%search-load-path "native/vm/C/libguile-tr.so")))
  (if file
      (load-extension file "init")
      (error "libguile-tr.so is not present, did you forget to make it?")))

(define SCM_BOOL_F      (get-flag 0))
(define SCM_BOOL_T      (get-flag 1))
(define SCM_EOL         (get-flag 2))
(define SCM_UNDEFINED   (get-flag 3))
(define SCM_UNSPECIFIED (get-flag 4))
(define SCM_ELISP_NIL   (get-flag 5))
(define SCM_INUM0       #b010)
(define SCM_INUM1       #b110)

(define tc7_variable (get-tc7 0))
(define tc7_symbol   (get-tc7 1))
(define tc7_vector   (get-tc7 2))
(define tc3_struct   (get-tc7 3))
(define tc7_program  (get-tc7 4))
(define tc8_char     (get-tc8 0))
