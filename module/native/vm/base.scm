(define-module (native vm base)
  #:use-module (native aschm)
  #:use-module (ice-9 match)
  #:export (*vm-instr* *vm-jump* *jmp-hooks* define-vm-inst define-vm-inst-jmp
                       define-jmp-hook 
                       make-byte-stream read-byte))


(define *vm-instr* (make-vector 500 #f))
(define *vm-jump*  (make-vector 500 #f))

(define-syntax-rule (define-vm-inst nm id (arg ...) code ...)
  (begin
    (define (nm label arg ...)
      (assemble () code ...))
    (vector-set! *vm-jump*  id (cons (length (list 'arg ...))
                                     (lambda x #f)))
    (vector-set! *vm-instr* id (cons (length (list 'arg ...)) nm))))

(define-syntax-rule (define-vm-inst-jmp nm id (arg ...) (jn jmp) code ...)
  (begin
    (define (nm jn arg ...)
      (assemble () code ...))
    (vector-set! *vm-jump*  id 
                 (cons (length (list 'arg ...))
                       (lambda (table i arg ...)
                         (let* ((n (pk (+ (pk i) (pk jmp))))
                                (x (hashq-ref table n)))
                           (if x
                               x
                               (let ((x (make-label)))
                                 (hashq-set! table n x)
                                 x))))))
                           
                           
    (vector-set! *vm-instr* id (cons (length (list 'arg ...)) nm))))

(define *jmp-hooks* '())
(define hook-i 0)
(define (add-hook x) (set! *jmp-hooks* (cons x *jmp-hooks*)))
(define-syntax-rule (define-jmp-hook name code ...)
  (begin
    (define name (let ((i     hook-i)
                       (label (make-label)))
                   (add-hook
                    (lambda x
                      (match x
                        (('set-jmp-map jmp reg)
                         (assemble () 
                           (inst mov reg (&& label))
                           (inst mov (Q jmp i) reg)))
                        (('emit)
                         (assemble () (emit-label label) code ...)))))
                   (lambda (reg) (Q reg i))))
    (set! hook-i (+ hook-i 1))))

(define (make-byte-stream bv) (cons 0 bv))
(define (read-byte s) 
  (let* ((i (car s))
         (r (u8vector-ref (cdr s) i)))
    (set-car! s (+ i 1))
    r))
