(define-module (native vm inst)
  #:use-module (native vm base)
  #:use-module (native vm bit-utilities)
  #:use-module (native vm vm)
  #:use-module (native vm program)
  #:use-module (native vm variable)
  #:use-module (native vm c-callers)
  #:use-module (native vm c-call)
  #:use-module (native aschm)
  #:use-module (native vm constants))
;-------------------------------------------------------------------------------
(define-syntax-rule (return-block code ...)
  (assemble ()
    (inst mov reg1 (&& cont:))
    (inst mov (ret) reg1)
    code ...
  cont:))

(define-vm-inst nop  0 ()
  (inst nop))

#;
(define-vm-inst halt 1 ()
  (jump-to-inst 1))

(define-vm-inst drop 2 ()
  (inst pop reg1))

(define-vm-inst dup  3 ()
  (inst push (Q rsp 0)))

(define-vm-inst void 4 ()
  (inst push SCM_UNSPECIFIED))

(define-vm-inst make-true 5 ()
  (inst push SCM_BOOL_T))

(define-vm-inst make-false 6 ()
  (inst push SCM_BOOL_F))

(define-vm-inst make-nil 7 ()
  (inst push SCM_ELISP_NIL))

(define-vm-inst make-eol 8 ()
  (inst push SCM_EOL))

(define-vm-inst make-int8 9 (arg)
  (inst push (make+-inum arg)))

(define-vm-inst make-int8:0 10 ()
  (inst push SCM_INUM0))

(define-vm-inst make-int8:1 11 ()
  (inst push SCM_INUM1))

(define-vm-inst make-int16 12 (a1 a2)
  (inst push (make+-inum a1 a2)))

(define-vm-inst make-int64 13 (a1 a2 a3 a4 a5 a6 a7 a8)
  (inst mov rax (make+-inum a1 a2 a3 a4 a5 a6 a7 a8))
  (inst push rax))

(define-vm-inst make-uint64 14 (a1 a2 a3 a4 a5 a6 a7 a8)
  (inst mov rax (make+inum a1 a2 a3 a4 a5 a6 a7 a8))
  (inst push rax))




(define-vm-inst make-char8 15 (a1)
  (inst push (+ (ash a1 8) tc8_char)))

(define-vm-inst make-char32 16 (a1 a2 a3 a4)
  (inst push (+ (ash (make+num a1 a2 a3 a4) 8) tc8_char)))

(define-vm-inst make-list 17 (a1 a2)
  (return-block
   (inst mov reg2 (make+num a1 a2))
   (inst jmp (make-list-jmp jmp))))


(define-jmp-hook make-list-jmp
  (inst mov call-2 SCM_EOL)
 loop:
  (inst cmp reg2 0)
  (inst jmp #:eq out:)
  (inst pop call-1)  
  (inst push reg2)
  (c-call scm_cons call-1 call-2)
  (inst pop reg2)
  (inst mov call-2 reg1)
  (inst dec reg2)
  (inst jmp loop:)
 out:
  (inst push call-2)
  (inst jmp (ret))
  )

(define-vm-inst make-vector 18 (a1 a2)
  (inst mov call-1 (make+num a1 a2))
  (inst mov call-2 rsp)
  (c-call scm_vector call-1 call-2)  
  (inst lea rsp (Q rsp (make+num a1 a2)))
  (inst push rax))


(define-vm-inst object-ref 19 (a1)
  (get-object a1 reg1)
  (inst push reg1))

(define-vm-inst long-object-ref 20 (a1 a2)
  (get-object (make+num a1 a2) reg1)
  (inst push reg1))

(define-vm-inst local-ref 21 (a1)
  (inst push (get-local a1)))

(define-vm-inst long-local-ref 22 (a1 a2)
  (inst push (get-local (make+num a1 a2))))


(define-vm-inst local-bound 23 (a1)
  (bound a1))

(define-vm-inst long-local-bound 24 (a1 a2)
  (bound (make+num a1 a2)))

(define (bound a1)
  (assemble ()
     (inst mov reg1 (get-local a1))
     (inst mov reg2 SCM_BOOL_F)
     (inst cmp reg1 SCM_UNDEFINED)
     (inst jmp #:eq out:)
     (inst mov reg2 SCM_BOOL_T)
    out:
     (inst push reg2)))


(define-vm-inst variable-ref 25 ()
  (inst mov call-1 (Q rsp))
  (VARIABLE? call-1 reg1 nonvar:)
  (VARIABLE-BOUND? call-1 reg1 not-bound:)
  (inst mov reg1 (VARIABLE-REF call-1))
  (inst mov (Q rsp) reg1)
  (inst jmp out:)
 nonvar:
  (c-call c_not_a_var)
 not-bound:
  (c-call c_var_not_bound)
 out:)

;;TODO make a custom nonvar fail call
(define-vm-inst variable-bound? 26 ()
  (inst mov call-1 (Q rsp))
  (VARIABLE? call-1 reg1 nonvar:)
  (inst mov reg2 SCM_BOOL_F)
  (VARIABLE-BOUND? call-1 reg1 out:)
  (inst mov reg2 SCM_BOOL_T)
  (inst jmp out:)
 nonvar:
  (c-call c_not_a_var_b)
 out:
  (inst mov (Q rsp) reg2))


(define-vm-inst toplevel-ref 27 (a1)
  (get-object a1 reg1)
  (VARIABLE? reg1 reg2 nonvar:)
  (inst jmp out:)
 nonvar:
  (inst mov call-1 reg1)
  (inst mov call-2 (get-program rbp))
  (inst mov call-3 a1)
  (c-call g_toplevel_lookup) 
 out:
  (inst push (VARIABLE-REF reg1)))

(define-vm-inst long-toplevel-ref 28 (a1 a2)
  (get-object (make+num a1 a2) reg1)
  (VARIABLE? reg1 reg2 nonvar:)
  (inst jmp out:)
 nonvar:
  (inst mov call-1 reg1)
  (inst mov call-2 (get-program rbp))
  (inst mov call-1 (make+num a1 a2))
  (c-call g_toplevel_lookup) 
 out:
  (inst push (VARIABLE-REF reg1)))

          
(define-vm-inst local-set 29 (a1)
  (inst pop  reg1)
  (inst mov  (get-local a1) reg1))

(define-vm-inst long-local-set 30 (a1 a2)
  (inst pop  reg1)
  (inst mov  (get-local (make+num a1 a2)) reg1))

(define-vm-inst variable-set 31 ()
  (inst pop call-1)
  (inst pop reg1)
  (VARIABLE? call-1 reg3 nonvar:)
  (inst mov (VARIABLE-REF call-1) reg1)
  (inst jmp out:)
 nonvar:
  (c-call c_not_a_var_s)
 out:)


(define-vm-inst toplevel-set 32 (a1)
  (get-object a1 reg1)
  (VARIABLE? reg1 reg2 nonvar:)
  (inst jmp out:)
 nonvar:
  (inst mov call-1 reg1)
  (inst mov call-2 (get-program rbp))
  (get-object-ref a1 call-3)
  (c-call g_toplevel_lookup) 
 out:
  (inst pop reg2)
  (inst mov (VARIABLE-REF reg1) reg2))
 
(define-vm-inst long-toplevel-set 33 (a1 a2)
  (get-object (make+num a1 a2) reg1)
  (VARIABLE? reg1 reg2 nonvar:)
  (inst jmp out:)
 nonvar:
  (inst mov call-1 reg1)
  (inst mov call-2 (get-program rbp))
  (get-object-ref (make+num a1 a2) call-3)
  (c-call g_toplevel_lookup) 
 out:
  (inst pop reg2)
  (inst mov (VARIABLE-REF reg1) reg2))


;;This is a bit number magic needs to be abstracted in order to be
;;more robust depends on a fixed layout of scm_i_thread
(define (handle-interupts reg4)
  (assemble ()
    (inst mov reg4 (cur_th))
    (inst mov reg4 (Q reg4 30))
    (inst test reg4 reg4)
    (inst jmp #:z out:)
    (c-call scm_async_click) 
   out:))
  
(define-syntax-rule (define-jumper nm id code ... (BR pred))
  (define-vm-inst-jmp nm id (x y z) (label (bit-offset x y z))
    code ...        
    (let ((d     (bit-offset x y z)))
      (if (< d 0)
          (begin
            (handle-interupts reg4)
            (inst jmp pred label))
          (inst jmp pred label)))))

(define-vm-inst-jmp br 34 (a b c) (label (bit-offset a b c))
  (let* ((d (bit-offset a b c)))    
    (if (< d 0)
        (begin
          (handle-interupts reg4)
          (inst jmp label))
        (inst jmp label))))


(define-jumper br-if 35 
  (inst pop reg1)
  (inst cmp reg1 SCM_BOOL_F)
  (BR #:ne))

(define-jumper br-if-not 36
  (inst pop reg1)
  (inst cmp reg1 SCM_BOOL_F)
  (BR #:eq))


(define-jumper br-if-eq 37
  (inst pop reg1)
  (inst pop reg2)
  (inst cmp reg1 reg2)
  (BR #:eq))

(define-jumper br-if-not-eq 38
  (inst pop reg1)
  (inst pop reg2)
  (inst cmp reg1 reg2)
  (BR #:ne))

(define-jumper br-if-null 39
  (inst pop reg1)
  (inst cmp reg1 SCM_EOL)
  (BR #:eq))

(define-jumper br-if-not-null 40
  (inst pop reg1)
  (inst cmp reg1 SCM_EOL)
  (BR #:ne))

#;
(define-vm-instr br_if_nargs_ne 41 (a b x y z)
  (let ((n     (make+num a b))
        (label (get-label (bit-offset x y z))))
    (inst mov reg1 sp)
    (inst sub reg1 bp)
    (inst cmp reg1 (- n 1))
    (inst jmp #:neq  label)))

#;
(define-vm-instr br_if_nargs_lt 42 (a b x y z)
  (let ((n     (make+num a b))
        (label (get-label (bit-offset x y z))))
    (inst mov reg1 sp)
    (inst sub reg1 bp)
    (inst cmp reg1 (- n 1))
    (inst jmp #:l  label)))

#;
(define-vm-instr br_if_nargs_gt 43 (a b x y z)
  (let ((n     (make+num a b))
        (label (get-label (bit-offset x y z))))
    (inst mov reg1 sp)
    (inst sub reg1 bp)
    (inst cmp reg1 (- n 1))
    (inst jmp #:g  label)))
#;
(define-vm-instr br_if_nargs_ee 44 (a b)
  (let ((n     (make+num a b)))
    (inst mov reg1 sp)
    (inst sub reg1 bp)
    (inst cmp reg1 (- n 1))
    (inst jmp #:neq  vm_error_wrong_num_args)))
#;
(define-vm-instr br_if_nargs_ge 45 (a b)
  (let ((n     (make+num a b)))
    (inst mov reg1 sp)
    (inst sub reg1 bp)
    (inst cmp reg1 (- n 1))
    (inst jmp #:l  vm_error_wrong_num_args)))

#;
(define-vm-instr bind-optionals 46 (a b)
  (let ((n     (make+num a b)))
    (inst mov reg1 bp)
    (inst add reg1 (- n 1))
   loop:
    (inst cmp reg1 sp)
    (inst jmp #:ge out)
    (inst push SCM_UNDEFINED)
    (jmp loop:)
    out:))

;;TODO
#;
(define-vm-instr bind-optionals-shuffle 47 (a b i j x y)
  (let ((n     (make+num a b)))
    (inst mov reg1 bp)
    (inst add reg1 (- n 1))
   loop:
    (inst cmp reg1 sp)
    (inst jmp #:ge out)
    (inst push SCM_UNDEFINED)
    (jmp loop:)
    out:))

;;TODO
#;
(define-vm-instr bind-lwargs 48 (a b i j x))

#;
(define-vm-instr push-rest 49 (a b i j)
  (let ((n (make+num a b))
        (m (make+num i j)))
    (assemble ()
      (inst mov call-2 SCM_EOL)
      (inst mov reg1 bp)
      (inst add reg1 (- n 1))
     loop:
      (inst cmp sp reg1)
      (inst jmp #:le out:)
      (inst pop call-1)
      (c-call scm_cons call-1 call-2)
      (inst pop call-2)
      (inst jmp loop:)
     out:
      (inst mov (local-ref m) call-2))))
#;
(define-vm-instr bind-rest 50 (a b i j)
  (let ((n (make+num a b))
        (m (make+num i j)))
    
    (inst mov call-2 SCM_EOL)
    (inst mov reg1 bp)
    (inst add reg1 (- n 1))
   (assemble ()
     loop:
      (inst cmp sp reg1)
      (inst jmp #:le out:)
      (inst pop call-1)
      (c-call scm_cons call-1 call-2)
      (inst pop call-2)
      (inst jmp loop:)
     out:
      (inst push call-2))))


(define-vm-inst reserve-locals 51 (a b)
  (let ((n (make+num a b)))
   (assemble ()
     (inst mov rsp rbp)
     (inst add rsp (- n 1))
     #;(nullstack)
     )))


(define-vm-inst new-frame 52 ()
  (inst push 0)
  (inst push 0)
  (inst push 0))

(define (cache-program reg work)
  (assemble ()
    (inst mov work (OBJTABLE-REF reg))
    (inst lea work (VECTOR-WELTS-REF work))
    (inst mov (vm-obj) work)

    (inst lea work (FREEVAR-REF  reg))
    (inst mov (vm-clo) work)
    
    (inst mov work (OBJCODE-REF reg))
    (inst mov work (NATIVE-REF  work))
    (inst mov work (BYTEVECTOR-REF work))))

;;TODO make sure that the call stub works here
(define-vm-inst vm-call 53 (nargs)
  (inst lea reg4 (Q rsp nargs))  ;points to program
  (inst mov reg1 (Q rsp nargs))  ;the program
  (inst mov call-4 (&& out:))
  (inst jmp (call-hook jmp))
 out:)
  
(define-jmp-hook call-hook
  (handle-interupts  reg3)
  (PROGRAM-JIT? reg1 reg2 reg3 noprogram: no-jit:)

 cont:
  (cache-program reg1 reg3)
  (inst mov reg2 rbp)
  (inst mov rbp  reg4) ;points to program
  
  (inst mov (dynamic-link rbp) reg2)
  (inst mov (return-ip rbp) call-4)
  (inst mov (mvreturn-ip rbp) 0)

  #;(push-continuation-hook)
  #;(apply-hook)

  (inst jmp reg3)

 noprogram:
  (inst push reg4)
  (inst push call-4)
  (inst mov call-1 reg1)
  (c-call scm_get_program)
  (inst pop call-4)
  (inst pop reg4)
  (inst jmp cont:)

 no-jit:
  (inst mov (save-1) reg4)
  (inst mov (save-2) call-4)
  (inst mov call-1 reg4)
  (inst mov call-2 rsp)
  (c-call scm_call_back_to_vm)
  (inst mov reg4   (save-1))
  (inst mov call-4 (save-2))
  (inst mov (dynamic-link reg4) rax)
  (inst lea rsp (dynamic-link reg4))
  (inst jmp call-4))
        

(define (transfer nargs reg2 reg4)
  (case nargs
    ((0)
     (assemble ()
       (inst mov reg2 (Q rsp 0))
       (inst mov (Q rbp 0) reg2)
       (inst mov rsp rbp)))
    ((1)
     (assemble ()
       (inst mov reg2 (Q rsp 1))
       (inst mov (Q rbp 0) reg2)
       (inst mov reg2 (Q rsp 0))
       (inst mov (Q rbp -1) reg2)
       (inst lea rsp (Q rbp -1))))
    ((2)
     (assemble ()
       (inst mov reg2 (Q rsp 2))
       (inst mov (Q rbp  0) reg2)
       (inst mov reg2 (Q rsp 1))
       (inst mov (Q rbp -1) reg2)
       (inst mov reg2 (Q rsp 0))
       (inst mov (Q rbp -2) reg2)
       (inst lea rsp (Q rbp -2))))
    (else       
     (assemble ()
       (inst imul call-4 8)
       (inst mov call-3 rsp)
       (inst lea rsp (Q rsp 0 call-4))
       (inst mov reg2 rbp)
      loop:
       (inst mov reg4 (Q rsp))
       (inst mov (Q reg2) reg4)
       (inst sub rsp  8)
       (inst sub reg2 8)
       (inst cmp rsp call-3)
       (inst jmp #:ne loop:)
       (inst mov rsp reg2)))))

;;TODO make sure that the call stub works here
(define-vm-inst tail-call 54 (nargs)
  (inst lea reg4 (Q rsp nargs))
  (inst mov reg1 (Q reg4))
  (case nargs
    ((0)
     (inst jmp (tcall-hook-0 jmp)))
    ((1)
     (inst jmp (tcall-hook-1 jmp)))
    ((2)
     (inst jmp (tcall-hook-2 jmp)))
    (else
     (inst mov call-4 nargs)
     (inst jmp (tcall-hook-n jmp)))))
        
  
(define (push-return reg2)
  (assemble ()
    (inst mov reg2 0)
   loop:
    (inst cmp rax SCM_EOL)
    (inst jmp #:eq eq:)    
    (inst push (Q rax))
    (inst mov rax (Q rax 1))
    (inst add reg2 1)
    (inst jmp loop:)
   eq:))


(define-syntax-rule (mk-tcall-hook name nargs)
  (define-jmp-hook name
    (handle-interupts reg3)
    (PROGRAM-JIT? reg1 reg2 reg3 noprogram: no-jit:)
   cont:  
    (cache-program reg1 reg3)

    #;(push-continuation-hook)
    #;(apply-hook)
  
    (transfer nargs reg2 reg4)
  
    #;(apply-hook)  
    (inst jmp reg3)

   noprogram:
    (inst mov call-1 reg1)
    (inst push call-4)
    (c-call scm_get_program)
    (inst pop call-4)
    (inst jmp cont:)

   no-jit:
    (inst mov call-1 reg4)
    (inst mov call-2 rsp)
    (inst mov call-3 (callwr))
    (c-call scm_mvcall_back_to_vm)
    (push-return reg2)
    (inst cmp reg2 1)
    (inst jmp #:eq 1:)
    (inst mov reg1 reg2)
    (inst jmp (mv-return-hook jmp))
   1:
    (inst jmp (return-hook jmp))
    ))

(mk-tcall-hook tcall-hook-0 0)
(mk-tcall-hook tcall-hook-1 1)
(mk-tcall-hook tcall-hook-2 2)
(mk-tcall-hook tcall-hook-n 300)

  
;;TODO make sure that the call stub works here
#;  
(define-vm-inst subr-call 55 (nargs)
  (inst mov reg1 (sp (- nargs)))
  (inst mov ret  (&& next:))
  (inst jmp (tail-call-program))
 next:
 )

;;TODO make sure that the call stub works here
#;  
(define-vm-inst smob-call 56 (nargs)
  (inst mov reg1 (sp (- nargs)))
  (inst mov ret  (&& next:))
  (inst jmp (tail-call-program))
 next:
 )

;;TODO make sure that the call stub works here
#;  
(define-vm-inst foreign-call 57 (nargs)
  (inst mov reg1 (sp (- nargs)))
  (inst mov ret  (&& next:))
  (inst jmp (tail-call-program))
 next:
 )

;;TODO make sure that the call stub works here
#;  
(define-vm-inst continuation-call 58 (nargs)
  (inst mov reg1 (sp (- nargs)))
  (inst mov ret  (&& next:))
  (inst jmp (tail-call-program))
 next:
)
  
;;TODO make sure that the call stub works here
#;  
(define-vm-inst partial-continuation-call 59 (nargs)
  (inst mov reg1 (sp (- nargs)))
  (inst mov ret  (&& next:))
  (inst jmp (tail-call-program))
 next:
)

;;TODO make sure that the call stub works here
#;  
(define-vm-inst tail-call-nargs 60(nargs)
  (inst mov reg1 (sp (- nargs)))
  (inst mov ret  (&& next:))
  (inst jmp (tail-call-program))
 next:
)
  

;;TODO make sure that the call stub works here
#;  
(define-vm-inst call/nargs 61(nargs)
  (inst mov reg1 (sp (- nargs)))
  (inst mov ret  (&& next:))
  (inst jmp (tail-call-program))
 next:
)
  

(define-vm-inst-jmp mv-call 62 (nargs x y z) (label (bit-offset x y z))   
  (inst lea reg4 (Q rsp nargs)) ;points to program
  (inst mov reg1 (Q reg4))
  (inst mov call-4 (&& label))
  (inst mov call-3 (&& out:))
  (inst jmp (mvcall-hook jmp))
 out:)
  

(define-jmp-hook mvcall-hook
  (handle-interupts reg2)
  (PROGRAM-JIT? reg1 reg2 reg3 noprogram: no-jit:)

 cont:
  (cache-program reg1 reg3)
  (inst mov reg2 rbp)
  (inst mov rbp  reg4) ;points to program

  (inst mov (dynamic-link rbp) reg2)
  (inst mov (return-ip rbp) call-3)
  (inst mov (mvreturn-ip rbp) call-4)

  #;(push-continuation-hook)
  #;(apply-hook)

  (inst jmp reg3)

 noprogram:
  (inst mov call-1 reg1)
  (inst push reg4)
  (inst push call-4)
  (inst push call-3)
  (c-call scm_get_program)
  (inst pop call-3)
  (inst pop call-4)
  (inst pop reg4)
  (inst jmp cont:)

 no-jit:
  (inst mov (save-1) reg4)
  (inst mov (save-2) call-3)
  (inst mov (save-3) call-4)
  (inst mov call-1 reg4)
  (inst mov call-2 rsp)
  (inst mov call-3 (callwr))
  (c-call scm_mvcall_back_to_vm)
  (inst mov reg4   (save-1))
  (inst mov call-3 (save-2))
  (inst mov call-4 (save-3))

  (inst lea rsp (lower-addres reg4))
  (push-return reg2)
  (inst cmp reg2 1)
  (inst jmp #:eq 1:)
  (inst imul reg2 4)
  (inst add reg2 2)
  (inst push reg2)
  (inst jmp call-4)
 1:
  (inst jmp call-3)
  )


;;TODO make sure that the call stub works here
#;  
(define-vm-inst apply 63(nargs)
  (inst mov reg1 (sp (- nargs)))
  (inst mov ret  (&& next:))
  (inst jmp (tail-call-program))
 next:
)

;;TODO make sure that the call stub works here
#;  
(define-vm-inst tail-apply 64(nargs)
  (inst mov reg1 (sp (- nargs)))
  (inst mov ret  (&& next:))
  (inst jmp (tail-call-program))
 next:
)
  
;;TODO make sure that the call stub works here
#;  
(define-vm-inst call/cc 65 (nargs)
  (inst mov reg1 (sp (- nargs)))
  (inst mov ret  (&& next:))
  (inst jmp (tail-call-program))
 next:
)

;;TODO make sure that the call stub works here
#;  
(define-vm-inst tail-call/cc 66 (nargs)
  (inst mov reg1 (sp (- nargs)))
  (inst mov ret  (&& next:))
  (inst jmp (tail-call-program))
 next:
)



(define (lower-addres reg) (Q reg 4))
(define (return-ip    reg) (Q reg 2))
(define (mvreturn-ip  reg) (Q reg 1))
(define (dynamic-link reg) (Q reg 3))


(define (cache-program-1 reg1 reg2)
  (assemble ()
    (inst mov reg1 (get-program rbp))
    (inst cmp reg1 0)
    (inst jmp #:eq skip:)
  
    ;; cache object table
    (inst mov reg2 (OBJTABLE-REF reg1))
    (inst lea reg2 (VECTOR-WELTS-REF reg2))
    (inst mov (vm-obj) reg2)

    ;; cache free variables
    (inst lea reg2 (FREEVAR-REF reg1))
    (inst mov (vm-clo) reg2)
   skip:))

(define-vm-inst return 67 ()
  (inst jmp (return-hook jmp)))

(define-jmp-hook return-hook
  #;(pop-continuation-hook 1)
  (handle-interupts reg4)
  (inst pop reg1)
  (inst lea rsp  (lower-addres rbp))
  (inst mov reg2 (return-ip    rbp))
  (inst mov rbp  (dynamic-link rbp))
  #;(nullstack)
  #;(inst mov (program) (bp-program bp))
  (cache-program-1 reg3 reg4 )

  (inst push reg1)
  (inst jmp  reg2))




(define-vm-inst return/values 68 (a)
  (inst mov reg2 a)
  (inst jmp (mv-return-hook jmp)))

(define-jmp-hook mv-return-hook
  ;(pop-continuation-hook)
  (handle-interupts reg4)
  (inst cmp reg2 1)
  (inst jmp #:l err:)
  (inst jmp #:eq one:)
  
  (inst mov reg1 (mvreturn-ip rbp))
  (inst test reg1 reg1)
  (inst jmp #:z one:)
  
  (inst lea reg4 (Q rsp -1 reg2 8))
  (inst lea rsp  (lower-addres rbp))
  (inst mov rbp  (dynamic-link rbp))
  
  (inst neg reg2)
  (inst mov reg3 0)
 loop:
  (inst cmp reg3 reg2)
  (inst jmp #:eq out:)
  (inst push (Q reg4 0 reg3 8))
  (inst sub reg3 1)
  (inst jmp loop:)
 out:
  (inst imul reg2 -4)
  (inst add reg2 2)
  (inst push reg2)
  (inst jmp reg1)

 one: 
  (inst lea reg1 (Q rsp -1 reg2 8))
  (inst lea rsp  (lower-addres rbp))
  (inst mov reg3 (return-ip    rbp))
  (inst mov rbp  (dynamic-link rbp))
  (inst push (Q reg1))
  (inst jmp reg3)

 err:
  (c-call g_zero_in_values))

#;
(define-vm-inst return/values* 69 (a)
  (inst mov reg1 a)
  (inst jmp (return-values-hook)))


;; TODO make sure to test for positive values
(define-vm-inst return/nvalues 70 ()
  (inst pop reg2)  
  (inst shr reg2 2)
  (inst jmp (mv-return-hook jmp)))




(define-vm-inst truncate-values 71 (nbinds rest)
  (inst pop reg2)
  (inst shr reg2 2)
  (if (= rest 0)
      (assemble ()
        (inst cmp reg2 nbinds)
        (inst jmp #:l err:)
        (inst sub reg2 nbinds)
        (inst lea rsp (Q rsp 0 reg2)))
      (assemble ()
        (inst sub reg2 1)
        (inst cmp reg2 nbinds)
        (inst jmp #:l err:)
        (inst sub reg2 nbinds)
        (inst mov call-2 SCM_EOL)
       loop:
        (inst test reg2 reg2)
        (inst jmp #:z finish:)
        (inst pop call-1)
        (inst push reg2)
        (c-call scm_cons)
        (inst pop reg2)
        (inst mov call-2 reg1)
        (inst dec reg2)
        (inst jmp loop:)
       finish:
        (inst push call-2)))
   (inst jmp out:)
  err:
   (c-call g_truncate_err)
  out:
  )
#;
(define-vm-inst box 72 (a)
  (inst pop call-2)
  (inst mov ret (&& cont:))
  (inst jmp (box-hook))
 cont:
  (inst pop reg1)
  (inst mov (local-ref a) reg1))

#;
(define-jmp-hook box-hook
  (sync-before-gc)
  (inst mov call-1 tc7_variable)
  (c-call scm-cell call-1 call-2)
  (inst jmp ret))

#;
(define-vm-inst empty-box 73 (a)
  (inst mov call-2 SCM_EOL)
  (inst mov ret (&& cont:))
  (inst jmp (box-hook))
 cont:
  (inst pop reg1)
  (inst mov (local-ref a) reg1))

#;
(define-vm-inst local-boxed-ref 74 (a)
  (local-box-ref* a))

#;
(define (local-box-ref* a)
  (inst mov reg1 (local-ref a))
  (VARIABLE? reg1 abort-hook)
  (inst mov reg1 (VARIABLE-REF reg1))
  (inst cmp reg1 SCM_UNDEFINED)
  (inst jmp #:eq abort-hook)
  (inst push reg1))
 
#;
(define-vm-inst local-boxed-set 75 (a)
  (inst mov reg1 (local-ref a))
  (VARIABLE? reg1 abort-hook)
  (inst pop reg2)
  (inst mov (VARIABLE-REF reg1) reg2))

#;
(define-vm-inst free-ref 76 (a)
  (check-free-variable a)
  (free-variable-ref a reg1)
  (push reg1))

#;
(define-vm-inst free-boxed-ref 77 (a)
  (check-free-variable a)
  (free-variable-ref a reg1)
  (VARIABLE? reg1 abort-hook)
  (inst mov reg1 (VARIABLE-REF reg1))
  (inst cmp reg1 SCM_UNDEFINED)
  (inst jmp #:eq abort-hook)
  (push reg1))

#;
(define-vm-inst free-boxed-set 78 (a)
  (check-free-variable a)
  (free-variable-ref a reg1)
  (VARIABLE? reg1 abort-hook)
  (inst pop reg2)
  (inst mov (VARIABLE-REF reg1) reg2))

#;
(define-vm-inst make-closure 79 (a b)
  (inst mov reg1 (make+num a b))
  (inst mov ret  (&& next:))
  (inst jmp (make-closure-hook))
 next:)

;;TODO make the closure hook

#;
(define-vm-inst make-closure 80 ()
  (inst mov ret  (&& next:))
  (inst jmp (make-closure-hook))
 next:)

#;
(define-vm-inst fix-closure 81 (a b)
  (inst mov reg1 (local-ref (make+num a b)))
  (gosub fix-closure-hook))

#;
(define-hook-inst fix-closure-hook
  (PROGRAM? reg1 (abort-hook))
  (inst mov reg2 (PROGRAM_LEN reg1))
  (inst xor reg3 reg3)
  (inst mov reg4 ref1)
  (inst neg reg4)
  (inst inc reg4)
 loop:
  (inst cmp reg3 reg2)
  (inst jmp #:ge out:)
  (inst mov (PROGRAM-REF reg1 reg3) (sp reg4))
  (inst inc reg3)
  (inst inc reg4)
  (inst jmp loop:)
 out:
  (dropn sp reg2))

#;
(define-vm-inst vm-define 82 ()
  (gosub-hook define-hook))


(define-vm-inst make-keyword 83 ()
  (inst mov call-1 (Q sp))
  (c-call scm_symbol_to_keyword)
  (inst mov (Q sp) rax))


(define-vm-inst make-symbol 84 ()
  (inst mov call-1 (Q sp))
  (c-call scm_string_to_symbol)
  (inst mov (Q sp) rax))

;;TODO Implement hooks and make the code below is portable

(define (setjmp prompt return)
  (assemble ()
    (inst mov call-1 prompt)
    (inst mov call-1 (Q call-1 2))
    (inst add call-1 32)
    (c-call scm_setjmp)
    (inst test rax rax)
    (inst jmp #:z out:)
    (cache-program reg1 reg2)
    #;(abort-continuation-hook)
    (return)
   out:))


(define-vm-inst-jmp vm-prompt 85 (a x y z) (label (bit-offset x y z))  
  (inst pop call-1)
  (inst mov call-2 a)
  (c-call scm_prompt)
  (setjmp reg1
          (lambda ()
            (let ((d     (bit-offset x y z)))
              (if (< d 0)
                  (assemble()
                    (handle-interupts reg4)
                    (inst jmp label))
                  (assemble () (inst jmp label)))))))

#;
(define-vm-inst wind 86 ()
  (gosub-hook wind-hook))

#;
(define-vm-inst abort 87 ()
  (gosub-hook abort-hook))

#;
(define-vm-inst unwind 88 ()
  (gosub-hook unwind-hook))

#;
(define-vm-inst wind-fluids 89 ()
  (gosub-hook wind-fluids-hook))

#;
(define-vm-inst unwind-fluids 90 ()
  (gosub-hook unwind-fluids-hook))

#;
(define-vm-inst fluid-ref 91 ()
  (gosub-hook fluid-ref-hook))

#;
(define-vm-inst fluid-set 92 ()
  (gosub-hook fluid-set-hook))


(define-vm-inst assert-nargs-ee/locals 93 (a)
  (inst mov rax rbp)
  (inst sub rax rsp)
  (inst cmp rax (* (logand a 7) 8))  
  (inst jmp #:eq ok:)
  (inst mov call-1 rax)
  (c-call scm_wrong_argument)
 ok:
  (inst sub rsp (* (ash a -3) 8)))
  


;;.......................................................
;; vm-i-scheme
;;.......................................................
(define-vm-inst vm-not 128 ()
  (inst pop reg1)
  (inst mov reg2 SCM_BOOL_F)
  (inst cmp reg1 SCM_BOOL_F)
  (inst jmp #:neq out:)
  (inst mov reg2 SCM_BOOL_T)
 out:
  (inst push reg2))


(define-vm-inst vm-not-not 129 ()
  (inst pop reg1)
  (inst cmp reg1 SCM_BOOL_F)
  (inst jmp #:eq out:)
  (inst mov reg1 SCM_BOOL_T)
 out:
  (inst push reg1))

(define-vm-inst vm-eq? 130 ()
  (inst pop reg1)
  (inst pop reg2)
  (inst mov reg3 SCM_BOOL_T)
  (inst cmp reg1 reg2)
  (inst jmp #:eq out:)
  (inst mov reg3 SCM_BOOL_F)
 out:
  (inst push reg3))



(define-vm-inst vm-not-eq? 131 ()
  (inst pop reg1)
  (inst pop reg2)
  (inst mov reg3 SCM_BOOL_F)
  (inst cmp reg1 reg2)
  (inst jmp #:eq out:)
  (inst mov reg3 SCM_BOOL_T)
 out:
  (inst push reg3))

(define-vm-inst vm-null? 132 ()
  (inst pop reg1)
  (inst mov reg2 SCM_BOOL_T)
  (inst cmp reg1 SCM_EOL)
  (inst jmp #:eq out:)
  (inst mov reg2 SCM_BOOL_F)
 out:
  (inst push reg2))

(define-vm-inst vm-not-null? 133 ()
  (inst pop reg1)
  (inst mov reg2 SCM_BOOL_F)
  (inst cmp reg1 SCM_EOL)
  (inst jmp #:eq out:)
  (inst mov reg2 SCM_BOOL_T)
 out:
  (inst push reg2))

(define-vm-inst vm-eqv? 134 ()
  (inst pop call-1)
  (inst pop call-2)
  (inst mov reg1 SCM_BOOL_T)
  (inst cmp call-1 call-2)
  (inst jmp #:eq out:)
  (inst mov reg1 SCM_BOOL_F)
  (inst test call-1 7)
  (inst jmp #:ne out:)
  (inst test call-2 7)
  (inst jmp #:ne out:)
  (c-call scm_eqv_p)
 out:
  (inst push reg1))



(define-vm-inst vm-equal? 135 ()
  (inst pop call-1)
  (inst pop call-2)
  (inst mov reg1 SCM_BOOL_T)
  (inst cmp call-1 call-2)
  (inst jmp #:eq out:)
  (inst mov reg1 SCM_BOOL_F)
  (inst test call-1 7)
  (inst jmp #:ne out:)
  (inst test call-2 7)
  (inst jmp #:ne out:)
  (c-call scm_equal_p)
 out:
  (inst push reg1))



(define-vm-inst vm-pair? 136 ()
  (inst pop reg1)
  (inst mov reg2 SCM_BOOL_F)
  (inst test reg1 7)
  (inst jmp #:nz out:)
  (inst mov reg1 (Q reg1))
  (inst test reg1 1)
  (inst jmp #:nz out:)
  (inst mov reg2 SCM_BOOL_T)
 out:
  (inst push reg2))


(define-vm-inst vm-list? 137 ()
  (inst pop reg1)
  (inst mov reg2 SCM_BOOL_T)
 loop:
  (inst cmp reg1 SCM_EOL)
  (inst jmp #:eq out:)
  (inst test reg1 7)
  (inst jmp #:nz false:)
  (inst mov reg3 (Q reg1))
  (inst test reg3 1)
  (inst jmp #:nz false:)
  (inst mov reg1 (Q reg1 1))
  (inst jmp loop:)
 false:
  (inst mov reg2 SCM_BOOL_F)
 out:
  (inst push reg2))

(define (type tc7)
  (assemble ()
    (inst pop reg1)
    (inst mov reg2 SCM_BOOL_F)
    (inst test reg1 7)
    (inst jmp #:nz out:)
    (inst mov reg1 (Q reg1))
    (inst and reg1 #x7f)
    (inst cmp reg1 tc7)
    (inst jmp #:ne out:)
    (inst mov reg2 SCM_BOOL_T)
   out:
    (inst push reg2)))

(define (type3 tc3)
  (assemble ()
    (inst pop reg1)
    (inst mov reg2 SCM_BOOL_F)
    (inst test reg1 7)
    (inst jmp #:nz out:)
    (inst mov reg1 (Q reg1))
    (inst and reg1 7)
    (inst cmp reg1 tc3)
    (inst jmp #:ne out:)
    (inst mov reg2 SCM_BOOL_T)
   out:
    (inst push reg2)))

(define-vm-inst vm-symbol? 138 ()
  (type tc7_symbol))

(define-vm-inst vm-vector? 139 ()
  (type tc7_vector))

(define-vm-inst vm-cons 140 ()
  (inst pop call-2)
  (inst pop call-1)
  (c-call scm_cons call-1 call-2)
  (inst push rax))


(define-vm-inst vm-car 141 ()
  (inst pop reg1)
  (inst test reg1 7)
  (inst jmp #:nz car-error:)
  (inst mov reg1 (Q reg1))
  (inst test reg1 1)
  (inst jmp #:nz car-error:)
  (inst push reg1)
  (inst jmp out:)
 car-error:
  (c-call scm_car_error)
 out:)


(define-vm-inst vm-cdr 142 ()
  (inst pop reg1)
  (inst test reg1 7)
  (inst jmp #:nz cdr-error:)
  (inst test (Q reg1) 1)
  (inst jmp #:nz cdr-error:)
  (inst push (Q reg1 1))
  (inst jmp out:)
 cdr-error:
  (c-call scm_cdr_error)
 out:)

(define-vm-inst vm-set-car! 143 ()
  (inst pop reg2)
  (inst pop reg1)
  (inst test reg1 7)
  (inst jmp #:nz car-error:)
  (inst mov reg3 (Q reg1))
  (inst test reg3 1)
  (inst jmp #:nz car-error:)
  (inst mov (Q reg1) reg2 )
  (inst jmp out:)
 car-error:
  (c-call scm_setcar_error)
 out:)

(define-vm-inst vm-set-cdr! 144 ()
  (inst pop reg2)
  (inst pop reg1)
  (inst test reg1 7)
  (inst jmp #:nz car-error:)
  (inst mov reg3 (Q reg1))
  (inst test reg3 1)
  (inst jmp #:nz car-error:)
  (inst mov (Q reg1 1) reg2)
  (inst jmp out:)
 car-error:
  (c-call scm_setcdr_error)
 out:)

(define (CMP jumper c-code)
  (assemble ()
    (inst pop call-2)
    (inst pop call-1)
    (inst test call-1 2)
    (inst jmp #:z slow:)
    (inst test call-1 2)
    (inst jmp #:z slow:)
    (inst mov reg1 SCM_BOOL_T)
    (inst cmp call-1 call-2)
    (inst jmp jumper out:)
    (inst jmp false:)
   slow:
    (c-call c-code)
    (inst jmp out:)
   false:
    (inst mov reg1 SCM_BOOL_F)
   out:
    (inst push reg1)))

(define-vm-inst ee? 145 ()
  (CMP #:eq scm_num_eq_p))

(define-vm-inst lt? 146 ()
  (CMP #:l scm_less_p))

(define-vm-inst le? 147 ()
  (CMP #:le scm_leq_p))

(define-vm-inst gt? 148 ()
  (CMP #:g scm_gr_p))

(define-vm-inst ge? 149 ()
  (CMP #:ge scm_geq_p))

(define-vm-inst vm-add 150 ()
  (inst pop call-1)
  (inst mov call-2 (Q rsp))
  (inst test call-1 2)
  (inst jmp #:z slow:)
  (inst test call-2 2)
  (inst jmp #:z slow:)
  (inst add call-2 call-1)
  (inst jmp #:o slow:)
  (inst sub call-2 2)
  (inst mov (Q rsp) call-2)
  (inst jmp out:)
 slow:
  (inst mov call-2 (Q rsp))
  (c-call scm_sum call-1 call-2)
  (inst mov (Q rsp) rax)
 out:)

(define-vm-inst vm-inc 151 ()
  (inst mov reg1 (Q rsp))
  (inst test reg1 2)
  (inst jmp #:z slow:)
  (inst add reg1 4)
  (inst jmp #:o slow:)
  (inst jmp out:)
 slow:
  (inst mov call-1 (Q rsp))
  (inst mov call-2 6)
  (c-call scm_sum call-1 call-2)
 out:
  (inst mov (Q rsp) reg1))


(define-vm-inst vm-sub 152 ()
  (inst pop call-1)
  (inst mov call-2 (Q rsp))
  (inst test call-1 2)
  (inst jmp #:z slow:)
  (inst test call-2 2)
  (inst jmp #:z slow:)
  (inst sub call-2 call-1)
  (inst jmp #:o slow:)
  (inst add call-2 2)
  (inst mov (Q rsp) call-2)
  (inst jmp out:)
 slow:
  (inst mov call-2 (Q rsp))
  (c-call scm_difference call-2 call-1)
  (inst mov (Q rsp) rax)
 out:)

(define-vm-inst vm-dec 153 ()
  (inst mov reg1 (Q rsp))
  (inst test reg1 2)
  (inst jmp #:z slow:)
  (inst sub reg1 4)
  (inst jmp #:o slow:)
  (inst jmp out:)
 slow:
  (inst mov call-1 (Q rsp))
  (inst mov call-2 6)
  (c-call scm_difference call-1 call-2)
 out:
  (inst mov (Q rsp) reg1))

(define-vm-inst vm-mul 154 ()
  (inst pop call-2)
  (inst pop call-1)
  (c-call scm_product)
  (inst push rax))

(define-vm-inst vm-div 155 ()
  (inst pop call-2)
  (inst pop call-1)
  (c-call scm_divide)
  (inst push rax))

(define-vm-inst vm-quo 156 ()
  (inst pop call-2)
  (inst pop call-1)
  (c-call scm_quotient)
  (inst push rax))

(define-vm-inst vm-rem 157 ()
  (inst pop call-2)
  (inst pop call-1)
  (c-call scm_remainder)
  (inst push rax))

(define-vm-inst vm-mod 158 ()
  (inst pop call-2)
  (inst pop call-1)
  (c-call scm_modulo)
  (inst push rax))

;;TODO this should be an inline asembly
(define-vm-inst vm-ash 159 ()
  (inst pop  rsi)
  (inst mov  rdi (Q rsp))
  (inst test dil 2)
  (inst jmp  #:z slow:)
  (inst test sil 2)
  (inst jmp  #:z slow:)
  (inst sar  rsi 2)
  (inst test rsi rsi)
  (inst jmp  #:s neg:)
  (inst cmp  rsi 60)
  (inst jmp  #:a slow:)
  (inst sar  rdi 2)
  (inst mov  ecx 61)
  (inst sub  ecx esi)
  (inst mov  rdx rdi)
  (inst sar  rdx #:cl)
  (inst add  rdx 1)
  (inst cmp  rdx 1)
  (inst jmp  #:a slow:)
  (inst mov  ecx esi)
  (inst sal  rdi #:cl)
  (inst jmp last:)
 slow:
  (inst mov call-2 (Q rsp -1))
  (inst mov call-1 (Q rsp  0))
  (c-call scm_ash)
  (inst jmp  final:)
 neg:
  (inst mov  ecx esi)
  (inst sar  rdi 2)
  (inst neg  ecx)
  (inst sar  rdi #:cl)
 last:
  (inst lea  rax (make-ea #:qword #:disp 2 #:index rdi #:scale 4)) ;;rax <- 4*rdi + 2
 final:
  (inst mov  (Q rsp) rax))

(define-vm-inst vm-logand 160 ()
  (inst pop call-2)
  (inst mov call-1 (Q rsp))
  (inst test call-1 2)
  (inst jmp #:z slow:)
  (inst test call-2 2)
  (inst jmp #:z slow:)
  (inst and call-2 call-1)
  (inst mov (Q rsp) call-2)
  (inst jmp out:)
 slow:
  (c-call scm_logand)
  (inst mov (Q rsp) rax)
 out:)
 
(define-vm-inst vm-logior 161 ()
  (inst pop call-2)
  (inst mov call-1 (Q rsp))
  (inst test call-1 2)
  (inst jmp #:z slow:)
  (inst test call-2 2)
  (inst jmp #:z slow:)
  (inst or call-2 call-1)
  (inst mov (Q rsp) call-2)
  (inst jmp out:)
 slow:
  (c-call scm_logior)
  (inst mov (Q rsp) rax)
 out:)

(define-vm-inst vm-logxor 162 ()
  (inst pop call-2)
  (inst mov call-1 (Q rsp))
  (inst test call-1 2)
  (inst jmp #:z slow:)
  (inst test call-2 2)
  (inst jmp #:z slow:)
  (inst xor call-2 call-1)
  (inst add call-2 2)
  (inst mov (Q rsp) call-2)
  (inst jmp out:)
 slow:
  (c-call scm_logxor)
  (inst mov (Q rsp) rax)
 out:)
 
(define-vm-inst vm-struct? 166 ()
  (type3 tc3_struct))
