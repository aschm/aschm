(define-module (native vm c-call)
  #:use-module (native aschm)
  #:use-module (native vm vm)
  #:export (c-call call-1 call-2 call-3 call-4 call-5 call-6))

;; c-call registers
(define call-1 rdi)
(define call-2 rsi)
(define call-3 rdx)
(define call-4 rcx)
(define call-5 r8)
(define call-6 r9)

(define (c-call f . regs)
  (assemble ()
    (f rax)
    (inst call reg1)))

