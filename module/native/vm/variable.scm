(define-module (native vm variable)
  #:use-module (native vm constants)
  #:use-module (native vm vm)
  #:use-module (native aschm)
  #:export (VARIABLE? VARIABLE-REF VARIABLE-BOUND?))


(define (VARIABLE? reg reg2 fail:)
  (assemble ()
    (inst test reg 7)
    (inst jmp #:nz fail:)
    (inst mov reg2 (Q reg))
    (inst and reg2 #x7f)
    (inst cmp reg2 tc7_variable)
    (inst jmp #:ne fail:)))

(define (VARIABLE-BOUND? reg reg2 fail:)
  (assemble ()
    (inst mov reg2 (VARIABLE-REF reg))
    (inst cmp reg2 SCM_UNDEFINED)
    (inst jmp #:eq fail:)))

(define (VARIABLE-REF reg) (Q reg 1))

