(define-module (native vm init)
  #:use-module (native vm vm)
  #:use-module (native vm inst))

(init-native-vm)
  
