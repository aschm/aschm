(define-module (native vm bit-utilities)
  #:export (make+num make+-num make+inum make+-inum bit-offset))

(define (make+num . l)
  (define (make+num0 x . l)
    (let loop ((x x) (l l))
      (if (pair? l)
          (loop (+ (ash x 8) (car l)) (cdr l))
          x)))
  (apply make+num0 l))

(define tr (make-u8vector 1))
(define (+/- x)
  (u8vector-set! tr 0 x)
  (s8vector-ref tr 0))

(define (make+-num . l)
  (let* ((x (apply make+num l))
         (r (ash (logand x (ash 1 (- (* (length l) 8) 1))) 1)))
    (- x r)))
    
(define (inum f . l)
  (let ((r (apply f l)))
    (+ 2 (ash r 2))))

(define (make+inum  . l) (apply inum make+num  l))
(define (make+-inum . l) (apply inum make+-num l))

(define (bit-offset a b c)
  (let* ((o (+ (ash a 16) (ash b 8) c))
         (r (ash (logand o (ash 1 23)) 1)))
    (- o r)))
