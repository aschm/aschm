;;TODO make sure that the call stub works here

(define (grovel-program reg1 reg2 reg3 call:)
  (assemble ()
   (-STRUCT? reg2 struct:)
   (-SMOB?   reg2 smob:)
   (inst jmp (vm_error_wrong_type_apply))

 struct:
   (-STRUCT-APPLICABLE? reg2)
   (inst mov reg1 (STRUCT-PROCEDURE-REF reg1))
   (inst mov (Q reg3) reg1)
   (inst jmp call:)

 smob:
   (-TC7? tc7_smob reg2     (vm_error_wrong_type_apply))
   (-SMOB-APPLICABLE? reg2  (vm_error_wrong_type_apply))
   (prepare-smob-call reg1 reg3)
   (inst jmp call:)))




(define-vm-inst call 53 (nargs)
  (inst lea reg3 (Q sp (- nargs)))
  (inst mov reg1 (Q reg3))
  (jmp-hook call-hook))


(define-jmp-hook call-hook
  (vm-handle-interupts)

 call:
  (PROGRAM? reg1 reg2 vm-error-program no-program:)
  (cache-proram reg1)
  
  (inst mov reg2 rbp)
  (lea  mov rbp (QSP reg3 1))
  (inst mov (DYNAMIC-LINK-REF     rbp) reg2)
  (inst mov (RETURN-ADRESS-REF    rbp) ret)
  
  (inst mov reg1 (PROGRAM-OBJ-REF reg1))
  (inst mov reg1 (OBJECT-CODE-REF reg1))

  (push-continuation-hook)
  (apply-hook)
  (inst jmp reg1)

 no-program:
  (grovel-program reg1 reg2 reg3 call:))


(define-vm-inst tail-call 54 (nargs)
  (inst lea rsp (QS rsp (- 1 nargs)))
  (inst mov reg3 nargs)
  (inst mov reg1 (QS rsp 1))
  (jmp-hook tail-call-hook))


(define-jmp-hook call-hook
  (vm-handle-interupts)

 call:
  (PROGRAM? reg1 reg2 vm-error-program no-program:)

  (cache-program reg1)
  
  (inst mov reg2 -1)
 loop:
  (inst cmp reg2 reg3)
  (inst jmp #:ge out:)
  (inst mov (stack-adress rbp reg2) (QS sp 0 reg2))
  (inst incr reg2)
  (inst jmp loop:)

 out:

  (inst mov reg1 (PROGRAM-OBJ-REF reg1))
  (inst mov reg1 (OBJECT-CODE-REF reg1))

  (push-continuation-hook)
  (apply-hook)
  (inst jmp reg1)

 no-program:
  (grovel-program reg1 reg2 reg3 call:))
