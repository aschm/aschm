(define-module (native vm jit)
  #:use-module (system vm program)
  #:use-module (system vm objcode)
  #:use-module (native aschm)
  #:use-module (native vm base)
  #:use-module (native vm bit-utilities)
  #:export (jit))

(define (get-program x) x)
(define (code-length-objcode code)
  (let ((a (u8vector-ref code 0))
        (b (u8vector-ref code 1))
        (c (u8vector-ref code 2))
        (d (u8vector-ref code 3)))
    (make+num d c b a)))

(define (jit applicative)
  (define labelpos (make-hash-table))
  (let ((program (get-program applicative)))
    (if program
        (let* ((obj  (program-objcode program))
               (code (objcode->bytecode  obj))
               (clen (pk (code-length-objcode code)))
               (lbl  
                (let loop ((i 8) (r '()))
                  (if (< i (+ clen 8))
                      (let* ((tag    (u8vector-ref code i))
                             (n+inst (vector-ref *vm-jump* tag))
                             (n      (car n+inst))
                             (inst   (cdr n+inst))
                             (args   (let loop ((j 0))
                                       (if (< j n)
                                           (cons
                                            (u8vector-ref code (+ i j 1))
                                            (loop (+ j 1)))
                                           '()))))
                        (loop (+ i n 1) (cons (apply inst labelpos (+ i n 1) 
                                                     args)
                                              r)))
                      (reverse (cons 'end  r)))))

               (new      
                (asm-s (seg)
                 (let loop ((i 8) (lbl lbl))
                   (pk `(,(car lbl) ,i))
                   (if (< i (+ clen 8))
                       (let* ((tag    (u8vector-ref code i))
                              (n+inst (vector-ref *vm-instr* tag))
                              (n      (car n+inst))
                              (label  (hashq-ref labelpos i))
                              (inst   (cdr n+inst))
                              (args   (let loop ((j 0))
                                        (if (< j n)
                                            (cons
                                             (u8vector-ref code (+ i j 1))
                                             (loop (+ j 1)))
                                            '()))))

                         (if label 
                             (begin
                               (pk `(label ,inst ,@args))
                               (emit-label label)
                               (apply inst (car lbl) args))
                             (begin
                               (pk `(,inst ,@args))
                               (apply inst (car lbl) args)))
                         (loop (+ i n 1) (cdr lbl)))))))
               (len  (u8vector-length new)))

          (mk-rwx (pk new))
          (program-add-jit program new)))))
                            
                             
          
