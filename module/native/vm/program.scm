(define-module (native vm program)
  #:use-module (native aschm)
  #:use-module (native vm constants)
  #:export (PROGRAM? PROGRAM-JIT? OBJCODE-REF OBJTABLE-REF FREEVAR-REF 
                     NATIVE-REF BYTEVECTOR-REF
                     VECTOR-WELTS-REF))

(define (reference? reg out:)
  (assemble ()
    (inst test reg 7)
    (inst jmp #:nz out:)))

(define (reg->reg8 x)
  (cond
   ((eq? x rax) al)
   ((eq? x rbx) bl)
   ((eq? x rcx) cl)
   ((eq? x rdx) dl)
   ((eq? x rdi) dil)
   ((eq? x rsi) sil)
   ((eq? x r8)  r8b)
   ((eq? x r9)  r9b)
   ((eq? x r10) r10b)
   ((eq? x r11) r11b)
   ((eq? x r12) r12b)
   ((eq? x r13) r13b)
   ((eq? x r14) r14b)
   ((eq? x r15) r15b)
   (else #f)))


(define (PROGRAM-JIT? reg reg2 reg3 out: nojit:) 
  (assemble ()
    (reference? reg out:)
    (inst mov reg2 (Q reg))
    (inst mov reg3 reg2)
    (inst and reg2 #x7f)
    (inst cmp reg2 tc7_program)
    (inst jmp #:ne out:)
    (inst test reg3 #x2000)
    (inst jmp #:z nojit:)))

(define (PROGRAM? reg reg2 out:) 
  (assemble ()
    (reference? reg out:)
    (inst mov reg2 (Q reg))
    (inst and reg2 #x7f)
    (inst cmp reg2 tc7_program)
    (inst jmp #:nz out:)))


(define (OBJCODE-REF      reg) (Q reg 1))
(define (OBJTABLE-REF     reg) (Q reg 2))
(define (FREEVAR-REF      reg) (Q reg 3))
(define (NATIVE-REF       reg) (Q reg 3))
(define (VECTOR-WELTS-REF reg) (Q reg 2))
(define (BYTEVECTOR-REF   reg) (Q reg 2))
