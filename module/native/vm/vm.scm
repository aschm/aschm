(define-module (native vm vm)
  #:use-module (system vm program)
  #:use-module (native aschm)
  #:use-module (native vm base)
  #:use-module (native vm bit-utilities)
  #:export (get-local get-object get-object-ref get-free get-program 
                      get-C reg1 reg2 reg3 reg4 reg5
                      make-c-caller *c-stubs* ret init-native-vm jmp vm
                      vm-rbx vm-r12 vm-r14 vm-clo vm-obj cur_th
                      save-1 save-2 save-3 save-4 callwr))

;; x86-64 rbp rbx r12 and r14 are callee saved registers.
;; rbx is used for shared library lookup tables
(define vm      r12)
(define jmp     r14)


(define n 0)        
(define-syntax-rule (mk-v name reg)
  (begin
    (define name
      (let ((n (- (+ n 1))))
        (case-lambda
          (()    (Q reg n))
          ((reg) (Q reg n))
          ((source reg) 
           (assemble () (inst mov (Q reg n) source))))))
    (set! n (+ n 1))))

;;vm variables defined here
;;we are defensive and don't use many registers yet
(mk-v vm-rbx vm)
(mk-v vm-r12 vm)
(mk-v vm-r14 vm)
(mk-v vm-clo vm)
(mk-v vm-obj vm)
(mk-v vm-c   vm)
(mk-v ret    vm)
(mk-v cur_th vm)
(mk-v callwr vm)
(mk-v save-1 vm)
(mk-v save-2 vm)
(mk-v save-3 vm)
(mk-v save-4 vm)
(define (get-local x) (Q rbp (- (+ 1 x))))
(define (get-program reg) (Q reg 0))
(define (get-object x reg)
  (assemble ()
    (inst mov reg (vm-obj))
    (inst mov reg (Q reg x))))

(define (get-object-ref x reg)
  (assemble ()
    (inst mov reg (vm-obj))
    (inst lea reg (Q reg x))))

(define (get-free x reg)
  (assemble ()
    (inst mov reg (vm-clo))
    (inst mov reg (Q reg x))))

(define (get-C x reg)
  (assemble ()
    (inst mov reg (vm-c))
    (inst mov reg (Q reg x))))


(define reg1 rax)
(define reg2 r10)
(define reg3 r13)
(define reg4 r15)
(define reg5 r11)

;;call this stub first at init as
;; vm2 =  vm(&jmpmap)
;;Then afterwords us 
;; ret = vm2(&jmpmap,&freevars,&objecttable,&c-codemap,&code)
(define (vm-stub)
  (asm
   ;;First call arg1 contains the addres of the goto map fill it in and return
   ;;The adress to the actual function stub
   (for-each (lambda (f) (f 'set-jmp-map rdi rax)) *jmp-hooks*)
   (inst mov rax (&& second:))
   (inst ret)

  second:   


   ;;C dance  
   (inst push rbp)
   (inst push 0)
   (inst mov rbp rsp)
   (inst add rsp (* (- n) 8))

   ;; rax = nargs and reg3 = program
   (inst mov rax  (Q rbp 3))
   (inst mov reg3 (Q rbp 4))

   ;;Save rbx,r12,r14 for as described by the calling convention   
   (vm-rbx rbx rbp)
   (vm-r12 r12 rbp)
   (vm-r14 r14 rbp)

   ;;Initialize the vm register
   (inst mov vm rbp)

   ;;current_thread needs to be imported
   (inst mov reg4 (Q rbp 5))
   (inst mov (cur_th) reg4)

   ;;the call-wrapper needs to be saved
   (inst mov reg4 (Q rbp 6))
   (inst mov (callwr) reg4)

   ;;Move the arguments of the function to it's positions
   (inst mov jmp  rdi)
   (vm-clo rsi vm)
   (vm-obj rdx vm)
   (vm-c   rcx vm)

   #;(inst mov rax 6)
   #;(inst imul rax 4)
   #;(inst add rax 2)
   #;(inst jmp out:)

   ;;Jump to the startup function
   (inst push rbp)           ; dynamic link
   (inst mov reg2 (&& ret:))
   (inst push reg2)          ; return addres
   (inst push 0)             ; mv-return addres
   (inst push reg3)          ; program 
   (inst mov rbp rsp)

   ;;Arguments need to be pushed onto the frame
   (inst neg rax)
  loop:
   (inst cmp rax 0)
   (inst jmp #:eq all:)
   (inst inc rax)
   (inst mov reg2 (Q r9 0 rax))
   (inst push reg2)
   (inst jmp loop:)
  all:

   ;; Goto the supplied function
   (inst jmp r8)

  ret:
   ;; Move return value to rax (return register)
   (inst pop rax)
  out:
   ;; Restore the saved registers
   (inst mov r12 (vm-r12 rbp))
   (inst mov r14 (vm-r14 rbp))
   (inst mov rbx (vm-rbx rbp))

   ;; the rsp rbp dance and return
   (inst mov rsp rbp)   
   (inst pop reg2) ;; We have a dummy program slot of 0 here
   (inst pop rbp)
   (inst ret)

   ;;emit the goto hooks
   (for-each (lambda (f) (f 'emit)) *jmp-hooks*)
   ))

(define (init-native-vm)
  (let ((vm (pk (vm-stub))))
    (mk-rwx vm)
    (native-vm-set! vm))) 
