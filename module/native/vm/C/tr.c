#include<libguile.h>

#define E(x) SCM_PACK((((long) x) << 2) + 2)
#define D(x) SCM_PACK((((long) SCM_UNPACK(x)) << 2) + 2)

SCM_DEFINE(get_flag,"get-flag",1,0,0,(SCM in),"")
{
  long i = (long) (SCM_UNPACK(in) >> 2);
  switch (i)
    {
    case 0:
      return D(SCM_BOOL_F);
    case 1:
      return D(SCM_BOOL_T);
    case 2:
      return D(SCM_EOL);
    case 3:
      return D(SCM_UNDEFINED);
    case 4:
      return D(SCM_UNSPECIFIED);
    case 5:
      return D(SCM_ELISP_NIL);      
    }
  scm_misc_error("get-flag","Not a correct input number",SCM_EOL);
  return SCM_UNSPECIFIED;
}

SCM_DEFINE(get_tc7,"get-tc7",1,0,0,(SCM in),"")
{
  long i = (long) SCM_UNPACK(in) >> 2;
  switch (i)
    {
    case 0:
      return E(scm_tc7_variable);
    case 1:
      return E(scm_tc7_symbol);
    case 2:
      return E(scm_tc7_vector);
    case 3:
      return E(scm_tc3_struct);
    case 4:
      return E(scm_tc7_program);
    }
  scm_misc_error("get-tc7","Not a correct input number",SCM_EOL);
  return SCM_UNSPECIFIED;
}

SCM_DEFINE(get_tc8,"get-tc8",1,0,0,(SCM in),"")
{
  long i = (long) SCM_UNPACK(in) >> 2;
  switch (i)
    {
    case 0:
      return E(scm_tc8_char);
    }
  scm_misc_error("get-tc8","Not a correct input number",SCM_EOL);
  return SCM_UNSPECIFIED;
}

void init()
{
#include  "tr.x"
}
