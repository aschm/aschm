(use-modules (native aschm))
(use-modules (native vm constants))

(define-syntax-rule (mk-and-test name val code ...)
  (let ((b (pk (asm code ...))))
    (mk-rwx b)
    (format #t "test ~a -->  ~a~%" 'name (let ((q (run-native b)))
                                           (if (equal? q val)
                                               #t
                                               q)))))

(define-syntax-rule (end-sub rbx)
  (begin
    (inst pop rbx)
    (inst jmp rbx)))



(mk-and-test eq 0
 (inst mov rax 2)
 (inst cmp rax 2)
 (inst jmp #:eq out:)
 (inst mov rax 6)
out:
 (end-sub rbx))

(mk-and-test neq 1
 (inst mov rax 2)
 (inst cmp rax 2)
 (inst jmp #:ne out:)
 (inst mov rax 6)
out:
 (inst pop rbx)
 (inst jmp rbx))

(mk-and-test less/a 0
 (inst mov rax 2)
 (inst cmp rax 3)
 (inst jmp #:l out:)
 (inst mov rax 6)
out:
 (inst pop rbx)
 (inst jmp rbx))

(mk-and-test less/b 1
 (inst mov rax 2)
 (inst cmp rax 2)
 (inst jmp #:l out:)
 (inst mov rax 6)
out:
 (inst pop rbx)
 (inst jmp rbx))

(mk-and-test index-sp 0
  (inst mov (Q rsp -1) 2)
  (inst mov rax (Q rsp -1))
  (end-sub rbx))

(mk-and-test push-pop 8
  (inst mov rax rsp)
  (inst push 2)
  (inst sub rax rsp)
  (inst imul rax 4)
  (inst add  rax 2)  
  (inst pop rbx)
  (end-sub rbx))

(mk-and-test label-ref 0
  (inst mov rax (&& out:))
  (inst add rax 0)
  (inst jmp rax)
  (inst mov rax 2)
 out:
  (inst imul rax 4)
  (inst add  rax 2)
  (end-sub rbx))

(mk-and-test sp 3
  (inst push 6)
  (inst mov rax (Q rsp 0))
  (inst pop rdi)
  (end-sub rbx))

(mk-and-test ind 1
  (inst push 6)
  (inst push 2)
  (inst mov rax 1)
  (inst mov rax (Q rsp 0 rax))
  (inst pop rdi)
  (inst pop rdi)
  (inst imul rax 4)
  (inst add rax 2)
  (end-sub rbx))

(mk-and-test shl 10
  (inst mov rcx 4)
  (inst mov rax 16)
  (inst shl rax 58)
  (inst jmp #:o o:)
  (inst mov rcx 6)
 o:
  (inst mov rax rcx)
  (end-sub rbx))

(let ((c (asm (inst mov rax (Q rax))
	      (end-sub rbx))))
  (pk c)
  (mk-rwx c)
  (pk `((car (list 1)) = ,(run-native c '(1)))))

(define (loop)
  (mk-and-test loop 0
    (inst mov rax 1000000000)
    (inst imul rax 4)
    (inst add  rax 2)
    (inst push rax)
   loop:
    (inst pop rax)
    (inst test rax 2)
    (inst jmp #:eq out:)
    (inst cmp rax 2)
    (inst jmp #:eq out:)
    (inst sub  rax 4)
    (inst push rax)
    (inst jmp loop:)
   out:
    (end-sub rbx)
    ))

(define &car
  (asm 
   (inst mov rbx SCM_BOOL_F)
   (inst test rax 7)
   (inst jmp #:nz out:)
   (inst mov rcx (Q rax))
   (inst test rcx 1)
   (inst jmp #:nz out:)
   (inst mov rbx rcx)
   out:
   (inst mov rax rbx)
   (end-sub rbx)))

(mk-rwx &car)
(define (my-car x) (run-native &car x))
